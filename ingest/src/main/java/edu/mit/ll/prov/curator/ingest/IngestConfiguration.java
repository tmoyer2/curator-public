package edu.mit.ll.prov.curator.ingest;

public class IngestConfiguration {

    private ReceiveConfiguration receive;
    private DeserializeConfiguration deserialize;
    private StoreConfiguration store;

    public void setReceive(ReceiveConfiguration receive) {
        this.receive = receive;
    }

    public ReceiveConfiguration getReceive() {
        return receive;
    }

    public void setDeserialize(DeserializeConfiguration deserialize) {
        this.deserialize = deserialize;
    }

    public DeserializeConfiguration getDeserialize() {
        return deserialize;
    }

    public void setStore(StoreConfiguration store) {
        this.store = store;
    }

    public StoreConfiguration getStore() {
        return store;
    }

}
