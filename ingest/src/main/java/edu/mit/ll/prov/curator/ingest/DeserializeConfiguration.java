package edu.mit.ll.prov.curator.ingest;

public class DeserializeConfiguration {

    private Boolean provJson;

    public void setProvJson(Boolean provJson) {
        this.provJson = provJson;
    }

    public Boolean getProvJson() {
        return provJson;
    }

}
