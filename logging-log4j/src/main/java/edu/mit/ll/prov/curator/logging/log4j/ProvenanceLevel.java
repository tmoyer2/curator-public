package edu.mit.ll.prov.curator.logging.log4j;

import org.apache.log4j.Level;

public class ProvenanceLevel extends Level {

    // between WARN and ERROR
    //public static int PROVENANCE_INT = 35000;
    //public static int PROVENANCE_SYSLOG = 2;

    // between ERROR and FATAL
    public static final int PROVENANCE_INT = 45000;
    public static final int PROVENANCE_SYSLOG = 4;

    public ProvenanceLevel() {
        super(PROVENANCE_INT, "PROVENANCE", PROVENANCE_SYSLOG);
    }

    public static Level toLevel(int val) {
        if (val == PROVENANCE_INT) {
            return new ProvenanceLevel();
        } else {
            return Level.toLevel(val);
        }
    }
}
