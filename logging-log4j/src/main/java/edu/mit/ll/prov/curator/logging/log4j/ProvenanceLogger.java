package edu.mit.ll.prov.curator.logging.log4j;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import edu.mit.ll.prov.curator.model.*;
import edu.mit.ll.prov.curator.serialize.Serializer;

/**

Use this class by something like:

private final static Logger logger = LogManager.getLogger(MyClass.class);
private final static ProvenanceLogger provLogger = new ProvenanceLogger(logger, new ProvJsonSerializer());

provLogger.log(vertex);

**/
public class ProvenanceLogger {
    
    public static final Level PROVENANCE = new ProvenanceLevel();

    protected Logger logger = null;
    protected Serializer ser = null;
    
    public ProvenanceLogger(Logger logger, Serializer ser) {
        this.logger = logger;
        this.ser = ser;
    }
    
    public void log(Graph graph) {
        logger.log(PROVENANCE, ser.serialize(graph));
    }
    
    public void log(Activity activity) {
        logger.log(PROVENANCE, ser.serialize(activity));
    }
    
    public void log(Agent agent) {
        logger.log(PROVENANCE, ser.serialize(agent));
    }
    
    public void log(Entity entity) {
        logger.log(PROVENANCE, ser.serialize(entity));
    }
    
    public void log(Used used) {
        logger.log(PROVENANCE, ser.serialize(used));
    }
    
    public void log(WasAssociatedWith waw) {
        logger.log(PROVENANCE, ser.serialize(waw));
    }
    
    public void log(WasDerivedFrom wdf) {
        logger.log(PROVENANCE, ser.serialize(wdf));
    }
    
    public void log(WasGeneratedBy wgb) {
        logger.log(PROVENANCE, ser.serialize(wgb));
    }

}
