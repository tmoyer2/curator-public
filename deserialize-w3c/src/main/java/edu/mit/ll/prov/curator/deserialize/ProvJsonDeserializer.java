package edu.mit.ll.prov.curator.deserialize;

import java.io.StringReader;

import java.util.HashMap;
import java.util.Map;

import javax.json.*;
import javax.xml.namespace.QName;

import edu.mit.ll.prov.curator.model.*;

public class ProvJsonDeserializer extends ProvDeserializer {

    public ProvJsonDeserializer() {
    }
    
    public Graph deserialize(String content) throws DeserializeException {
        Graph graph = new Graph();

	JsonObject doc = null;
	try {
	    JsonReader reader = Json.createReader(new StringReader(content));
	    doc = reader.readObject();
	    reader.close();
	} catch (JsonException e) {
	    throw new DeserializeException(e);
	}

        Map<String, String> prefixes = new HashMap<>();  // prefix -> namespace
        prefixes.put("prov", "http://www.w3.org/ns/prov#");
        prefixes.put("xsd", "http://www.w3.org/2000/10/XMLSchema#");
        if (doc.containsKey("prefix")) {
            for (String prefix : doc.getJsonObject("prefix").keySet()) {
                prefixes.put(prefix, doc.getJsonObject("prefix").getString(prefix));
            }
        }

        if (doc.containsKey("activity")) {
            for (String idStr : doc.getJsonObject("activity").keySet()) {
                JsonObject obj = doc.getJsonObject("activity").getJsonObject(idStr);
                graph.add(new Activity(getId(idStr, prefixes),
                                       getAttributes(obj, prefixes)));
            }
        }
        if (doc.containsKey("agent")) {
            for (String idStr : doc.getJsonObject("agent").keySet()) {
                JsonObject obj = doc.getJsonObject("agent").getJsonObject(idStr);
                graph.add(new Agent(getId(idStr, prefixes),
                                    getAttributes(obj, prefixes)));
            }
        }
        if (doc.containsKey("entity")) {
            for (String idStr : doc.getJsonObject("entity").keySet()) {
                JsonObject obj = doc.getJsonObject("entity").getJsonObject(idStr);
                graph.add(new Entity(getId(idStr, prefixes),
                                     getAttributes(obj, prefixes)));
            }
        }

        if (doc.containsKey("actedOnBehalfOf")) {
            for (String idStr : doc.getJsonObject("actedOnBehalfOf").keySet()) {
                JsonObject obj = doc.getJsonObject("actedOnBehalfOf").getJsonObject(idStr);
                graph.add(new WasAttributedTo(getId(idStr, prefixes),
                                              getId(obj.getString("prov:delegate"), prefixes),
                                              getId(obj.getString("prov:responsible"), prefixes),
                                              getAttributes(obj, prefixes)));
            }
        }
        if (doc.containsKey("used")) {
            for (String idStr : doc.getJsonObject("used").keySet()) {
                JsonObject obj = doc.getJsonObject("used").getJsonObject(idStr);
                graph.add(new Used(getId(idStr, prefixes),
                                   getId(obj.getString("prov:activity"), prefixes),
                                   getId(obj.getString("prov:entity"), prefixes),
                                   getAttributes(obj, prefixes)));
            }
        }
        if (doc.containsKey("wasAssociatedWith")) {
            for (String idStr : doc.getJsonObject("wasAssociatedWith").keySet()) {
                JsonObject obj = doc.getJsonObject("wasAssociatedWith").getJsonObject(idStr);
                graph.add(new WasAssociatedWith(getId(idStr, prefixes),
                                                getId(obj.getString("prov:activity"), prefixes),
                                                getId(obj.getString("prov:agent"), prefixes),
                                                getAttributes(obj, prefixes)));
            }
        }
        if (doc.containsKey("wasAttributedTo")) {
            for (String idStr : doc.getJsonObject("wasAttributedTo").keySet()) {
                JsonObject obj = doc.getJsonObject("wasAttributedTo").getJsonObject(idStr);
                graph.add(new WasAttributedTo(getId(idStr, prefixes),
                                              getId(obj.getString("prov:entity"), prefixes),
                                              getId(obj.getString("prov:agent"), prefixes),
                                              getAttributes(obj, prefixes)));
            }
        }
        if (doc.containsKey("wasDerivedFrom")) {
            for (String idStr : doc.getJsonObject("wasDerivedFrom").keySet()) {
                JsonObject obj = doc.getJsonObject("wasDerivedFrom").getJsonObject(idStr);
                graph.add(new WasDerivedFrom(getId(idStr, prefixes),
                                             getId(obj.getString("prov:generatedEntity"), prefixes),
                                             getId(obj.getString("prov:usedEntity"), prefixes),
                                             getAttributes(obj, prefixes)));
            }
        }
        if (doc.containsKey("wasInformedBy")) {
            for (String idStr : doc.getJsonObject("wasInformedBy").keySet()) {
                JsonObject obj = doc.getJsonObject("wasInformedBy").getJsonObject(idStr);
                graph.add(new WasGeneratedBy(getId(idStr, prefixes),
                                             getId(obj.getString("prov:informant"), prefixes),
                                             getId(obj.getString("prov:informed"), prefixes),
                                             getAttributes(obj, prefixes)));
            }
        }
        if (doc.containsKey("wasGeneratedBy")) {
            for (String idStr : doc.getJsonObject("wasGeneratedBy").keySet()) {
                JsonObject obj = doc.getJsonObject("wasGeneratedBy").getJsonObject(idStr);
                graph.add(new WasGeneratedBy(getId(idStr, prefixes),
                                             getId(obj.getString("prov:entity"), prefixes),
                                             getId(obj.getString("prov:activity"), prefixes),
                                             getAttributes(obj, prefixes)));
            }
        }

        return graph;
    }

    protected Map<QName, Object> getAttributes(JsonObject obj, Map<String, String> prefixes) {
        Map<QName, Object> attrs = new HashMap<>();
        for (String nameStr : obj.keySet()) {
            if (nameStr.startsWith("prov:")) {
                continue;  // ignore: it's a source or destination vertex
            }
            attrs.put(getId(nameStr, prefixes),
                      getValue(obj.getJsonObject(nameStr).getString("$"),
                               obj.getJsonObject(nameStr).getString("type")));
        }
        return attrs;
    }

    protected QName getId(String id, Map<String, String> prefixes) {
        String[] prefixName = id.split(":");
        return new QName(prefixes.get(prefixName[0]), prefixName[1], prefixName[0]);
    }

    protected Object getValue(String value, String type) {
        if ("xsd:string".equals(type)) {
            return value;
        } else if ("xsd:boolean".equals(type)) {
            return Boolean.valueOf(value);
        } else if ("xsd:integer".equals(type)) {
            return Integer.valueOf(value);
        } else if ("xsd:long".equals(type)) {
            return Long.valueOf(value);
        } else if ("xsd:float".equals(type)) {
            return Float.valueOf(value);
        } else if ("xsd:double".equals(type)) {
            return Double.valueOf(value);
        } else if ("xsd:decimal".equals(type)) {
            return Double.valueOf(value);
        } else {
            // log a warning
            return value;
        }
    }

}
