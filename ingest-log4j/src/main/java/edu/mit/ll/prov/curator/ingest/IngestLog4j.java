package edu.mit.ll.prov.curator.ingest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import java.util.Map;

import org.yaml.snakeyaml.Yaml;

import edu.mit.ll.prov.curator.CuratorException;
import edu.mit.ll.prov.curator.db.Store;
import edu.mit.ll.prov.curator.deserialize.Deserializer;

public class IngestLog4j extends Ingest {

    public IngestLog4j(String configFile) throws CuratorException {
        super(configFile);
        configureReceiver(config.getReceive());
    }

    final protected void configureReceiver(ReceiveConfiguration config) throws CuratorException {
        if (config == null) {
            throw new CuratorException("config file doesn't contain a 'receive' section");
        }
        if (config.getLog4jSocket() != null) {
            receiver = new ReceiveLog4jSocket(deser, store, config.getLog4jSocket());
        } else if (config.getLog4jFileJson() != null) {
            receiver = new ReceiveLog4jFileJsonLayout(deser, store, config.getLog4jFileJson());
        } else {
            throw new CuratorException("no receiver specified");
        }
    }

    public static void main(String[] args) throws CuratorException {
        if (args.length != 1) {
            System.err.println("usage: Ingest <config file>");
            System.exit(1);
        }

        IngestLog4j ingest = new IngestLog4j(args[0]);
        ingest.run();
    }

}
