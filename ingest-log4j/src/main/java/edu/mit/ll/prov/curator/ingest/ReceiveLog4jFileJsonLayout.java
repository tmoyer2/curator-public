package edu.mit.ll.prov.curator.ingest;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets; 

import java.util.Map;

import javax.json.*;

import org.apache.commons.io.input.Tailer;
import org.apache.commons.io.input.TailerListener;
import org.apache.commons.io.input.TailerListenerAdapter;

import edu.mit.ll.prov.curator.db.Store;
import edu.mit.ll.prov.curator.db.StoreException;
import edu.mit.ll.prov.curator.deserialize.DeserializeException;
import edu.mit.ll.prov.curator.deserialize.Deserializer;
import edu.mit.ll.prov.curator.model.Graph;

public class ReceiveLog4jFileJsonLayout extends Receiver {
    protected Log4jFileJsonLayoutConfiguration config;

    protected Tailer tailer;
    protected String buffer = "";
    
    public ReceiveLog4jFileJsonLayout(Deserializer deser, Store store, Log4jFileJsonLayoutConfiguration config) {
        super(deser, store);
        this.config = config;
    }
    
    public void run() {
        File file = new File(config.getPath());
        tailer = Tailer.create(file, new Listener());
        tailer.run();
    }

    class Listener extends TailerListenerAdapter {
        public void handle(String line) {
            if (line.endsWith("},")) {  // JSON object is done
                buffer = buffer + line;
                try {
                    try (InputStream is = new ByteArrayInputStream(buffer.getBytes(StandardCharsets.UTF_8));
                            JsonReader reader = Json.createReader(is)) {
                            JsonObject obj = reader.readObject();
                            if ("PROVENANCE".equals(obj.getString("level"))) {
                                Graph g = deser.deserialize(obj.getString("message"));
                                store.write(g);
                            }
                        }
                } catch (IOException e) {
                    handle(e);
                } catch (DeserializeException e) {
                    handle(e);
                } catch (StoreException e) {
                    handle(e);
                }
                buffer = "";
            } else {
                buffer = buffer + line;
            }
        }

        public void handle(Exception e) {
            StringWriter trace = new StringWriter();
            e.printStackTrace(new PrintWriter(trace));
            System.out.println("Stacktrace:\n" + trace);
            tailer.stop();
        }
    }
}
