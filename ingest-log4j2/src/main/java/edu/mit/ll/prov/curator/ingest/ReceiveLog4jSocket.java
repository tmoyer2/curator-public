package edu.mit.ll.prov.curator.ingest;

import java.io.BufferedInputStream;
import java.io.ObjectInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.PrintWriter;

import java.net.ServerSocket;
import java.net.Socket;

import java.util.Map;

//import org.apache.log4j.spi.LoggingEvent;

import edu.mit.ll.prov.curator.db.Store;
import edu.mit.ll.prov.curator.db.StoreException;
import edu.mit.ll.prov.curator.deserialize.DeserializeException;
import edu.mit.ll.prov.curator.deserialize.Deserializer;
import edu.mit.ll.prov.curator.model.Graph;

public class ReceiveLog4jSocket extends Receiver {
    protected Log4jSocketConfiguration config;
    
    public ReceiveLog4jSocket(Deserializer deser, Store store, Log4jSocketConfiguration config) {
        super(deser, store);
        this.config = config;
    }
    
    public void run() {
        try (ServerSocket serverSocket = new ServerSocket(config.getPort())) {
            while (true) {
                if (serverSocket.isClosed()) {
                    break;
                }
                Socket socket = serverSocket.accept();
                new Thread(new ReceiveThread(socket)).start();
            }
        } catch (Exception e) {
            handle(e);
        }
    }

    public void handle(Exception e) {
        StringWriter trace = new StringWriter();
        e.printStackTrace(new PrintWriter(trace));
        System.out.println("Stacktrace:\n" + trace);
    }

    class ReceiveThread implements Runnable {
        protected Socket socket;
        protected ObjectInputStream in = null;

        public ReceiveThread(Socket socket) {
            this.socket = socket;
            try {
                in = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
            } catch (IOException e) {
                handle(e);
            }
        }

        public void run() {
            try {
                while (true) {
                    if (in.available() < 0) {
                        break;
                    }
                    System.out.printf("received log object of type %s%n", in.readObject().getClass().getName());
                    /*LoggingEvent event = (LoggingEvent)in.readObject();
                    if (event.getLevel().toString().equals("PROVENANCE")) {  // avoid importing ProvenanceLevel
                        store.write(deser.deserialize((String)event.getMessage()));
                        }*/
                }
            } catch (Exception e) {
                handle(e);
            } finally {
                try {
                    in.close();
                    socket.close();
                } catch (Exception e) {handle(e);}
            }
        }
    }
}
