package edu.mit.ll.prov.curator.db;

import java.util.Random;

public class AccumuloDbTest extends DbTest {

    // return non-void to work around a bug in the maven surefire support for pojo tests
    public int setUp() {
        AccumuloConfiguration config = new AccumuloConfiguration();
        config.setMock(true);
        config.setUsername("testUser");
        config.setPassword("testPass");
        try {
            db = new Accumulo(config);
        } catch (DatabaseException e) {
            e.printStackTrace();
        }
        random = new Random(1390408);
        return 1;
    }

    // return non-void to work around a bug in the maven surefire support for pojo tests
    public int tearDown() {
        db = null;
        return 1;
    }

}
