package edu.mit.ll.prov.curator.db;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.accumulo.core.client.*;
import org.apache.accumulo.core.client.admin.TableOperations;
import org.apache.accumulo.core.client.mock.MockInstance;
import org.apache.accumulo.core.client.security.tokens.PasswordToken;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.user.RegExFilter;
import org.apache.accumulo.core.security.Authorizations;

import edu.mit.ll.prov.curator.model.*;

/*

Key: row id, column family, column qualifier
Timestamp
Value

GRAPH_TABLE:

    row id     | column family      | column qualifier | value
----------------------------------------------------------------------
vertex id      | "vertex_type"      | vertex type      | <empty>           (1)
edge id        | "edge_type"        | edge type        | <empty>
edge id        | "edge_source_dest" | source id        | destination id    (2)
source id      | "edge_forward"     | destination id   | edge id           (3)
destination id | "edge_backward"    | source id        | edge id           (3)
vertex/edge id | attr id            | attr type id     | attr value        (4)

ATTR_TABLE:

attr value     | attr id            | vertex/edge id   | type id           (5)

1) id format is <name>|<namespace>
2) needed for getEdge(id)
3) for fast forward and backward traversals
4) no edge can have the same id as a vertex
5) for fast attribute-based search. vertex/edge id is in the column qualifier to make the key unique.

 */

public class Accumulo implements Database {
    private static final String DEFAULT_AUTHORIZATIONS = "default";

    private static final String GRAPH_TABLE = "provenance_graph";
    private static final String ATTR_TABLE = "provenance_attr";
    private static final String AUTHORIZATION = "public";

    protected AccumuloConfiguration config;

    protected Connector conn;

    private BatchWriter graphWriter;
    private BatchWriter attrWriter;

    private final static Logger logger = LoggerFactory.getLogger(Accumulo.class);
    
    
    public Accumulo(AccumuloConfiguration config) throws DatabaseException {
        this.config = config;
        try {
            connect();
        } catch (AccumuloException | AccumuloSecurityException | TableNotFoundException e) {
            throw new DatabaseException("Error establishing connection", e);
        }
    }

    public Accumulo(String instanceName, String zooserver, String username, String password) throws DatabaseException {
        this.config = new AccumuloConfiguration();
        this.config.setInstanceName(instanceName);
        this.config.setZooserver(zooserver);
        this.config.setUsername(username);
        this.config.setPassword(password);

        try {
            connect();
        } catch (AccumuloException | AccumuloSecurityException | TableNotFoundException e) {
            throw new DatabaseException("Error establishing connection", e);
        }
    }

    final protected void connect() throws AccumuloException, AccumuloSecurityException, TableNotFoundException {
        Instance instance;
        if (config.getMock()) {
            instance = new MockInstance(config.getInstanceName());
        } else {
            instance = new ZooKeeperInstance(config.getInstanceName(), config.getZooserver());
        }
        conn = instance.getConnector(config.getUsername(), new PasswordToken(config.getPassword()));

        BatchWriterConfig conf = new BatchWriterConfig();
        if (config.getBatchBytes() != null) {
            conf.setMaxMemory(config.getBatchBytes());
        }
        if (config.getNumThreads() != null) {
            conf.setMaxWriteThreads(config.getNumThreads());
        }

        createTables();

        graphWriter = conn.createBatchWriter(GRAPH_TABLE, conf);
        attrWriter = conn.createBatchWriter(ATTR_TABLE, conf);
    }

    protected void createTables() throws AccumuloException, AccumuloSecurityException {
        TableOperations tableOps = conn.tableOperations();
        try {
            tableOps.create(GRAPH_TABLE);
        } catch (TableExistsException e) {
            logger.debug("Table exists", e);
        }
        try {
            tableOps.create(ATTR_TABLE);
        } catch (TableExistsException e) {
            logger.debug("Table exists", e);
        }
    }

    public Map<String,List<String>> getCategories() throws QueryException {
        return new HashMap<String,List<String>>();
    }
    
    public void clearDatabase() throws DatabaseException {
        //TODO: implement 
    }

    public void write(Graph graph) throws StoreException {
        // any way to optimize this?
        for(Vertex vertex : graph.vertices.values()) {
            write(vertex);
        }
        for(Edge edge : graph.edges.values()) {
            write(edge);
        }
    }

    public void write(Vertex vertex) throws StoreException {
        Mutation m = new Mutation(toAccumulo(vertex.getId()));
        m.put("vertex_type", vertex.getType(), new Value());
        for (QName attrName : vertex.getAttributeNames()) {
            m.put(toAccumulo(attrName),
                  toAccumulo(vertex.getAttributeType(attrName)),
                  vertex.getAttributeValue(attrName).toString());
        }
        try {
            graphWriter.addMutation(m);
        } catch (MutationsRejectedException ex) {
            logger.error("Mutation rejected: ", ex);
        }

        // insert attributes by value
        for (QName attrName : vertex.getAttributeNames()) {
            m = new Mutation(vertex.getAttributeValue(attrName).toString());
            m.put(toAccumulo(attrName), toAccumulo(vertex.getId()), toAccumulo(vertex.getAttributeType(attrName)));
            try {
                attrWriter.addMutation(m);
            } catch (MutationsRejectedException ex) {
                logger.error("Mutation rejected: ", ex);
            }
        }
    }

    public void write(Edge edge) throws StoreException {
        // add description to edge table
        Mutation m = new Mutation(toAccumulo(edge.getId()));
        m.put("edge_type", edge.getType(), new Value());
        for (QName attrName : edge.getAttributeNames()) {
            m.put(toAccumulo(attrName),
                  toAccumulo(edge.getAttributeType(attrName)),
                  edge.getAttributeValue(attrName).toString());
        }
        // add source and destination ids
        m.put("edge_source_dest", toAccumulo(edge.getSource()), toAccumulo(edge.getDestination()));
        try {
            graphWriter.addMutation(m); // assumes that edge ids don't conflict with vertex ids
        } catch (MutationsRejectedException ex) {
            logger.error("Mutation rejected: ", ex);
        }

        // add forward and backward edges to graph table
        m = new Mutation(toAccumulo(edge.getSource()));
        m.put("edge_forward", toAccumulo(edge.getDestination()), toAccumulo(edge.getId()));
        try {
            graphWriter.addMutation(m);
        } catch (MutationsRejectedException ex) {
            logger.error("Mutation rejected: ", ex);
        }
        m = new Mutation(toAccumulo(edge.getDestination()));
        m.put("edge_backward", toAccumulo(edge.getSource()), toAccumulo(edge.getId()));
        try {
            graphWriter.addMutation(m);
        } catch (MutationsRejectedException ex) {
            logger.error("Mutation rejected: ", ex);
        }

        // insert attributes by value
        for (QName attrName : edge.getAttributeNames()) {
            m = new Mutation(edge.getAttributeValue(attrName).toString());
            m.put(toAccumulo(attrName), toAccumulo(edge.getId()), toAccumulo(edge.getAttributeType(attrName)));
            try {
                attrWriter.addMutation(m);
            } catch (MutationsRejectedException ex) {
                logger.error("Mutation rejected: ", ex);
            }
        }
    }

    public List<Vertex> findVertices(QName attrName, Object attrValue) throws QueryException {
        return findVertices(Collections.singletonMap(attrName, attrValue));
    }
    public List<Vertex> findVertices(QName attrName, Object attrValue, String vertexType, boolean either, Integer paginate) throws QueryException {
        return findVertices(Collections.singletonMap(attrName, attrValue), vertexType, either, paginate);
    }

    public List<Vertex> findVertices(Map<QName, Object> attrs) throws QueryException {
        return findVertices(attrs, null, false, null);
    }
    public List<Vertex> findVertices(Map<QName, Object> attrs, String vertexType, boolean either, Integer paginate) throws QueryException {
        try {
            List<String> vertexIds = accumuloFindVertices(attrs);
            List<Vertex> vertices = new ArrayList<>();
            for(String vertexId : vertexIds) {
                vertices.add(getVertex(vertexId));
            }
            return vertices;
        } catch (Exception e) {
            throw new QueryException(e);
        }
    }

    protected List<String> accumuloFindVertices(Map<QName, Object> attrs)
        throws TableNotFoundException, QueryException {
        
        Set<Map.Entry<QName, Object>> attrSet = attrs.entrySet();
        Set<String> vertexIds = null;
        for (Map.Entry<QName, Object> attr: attrSet) {
            Set<String> vertIds = accumuloFindVertices(attr.getKey(), attr.getValue());
            if (vertexIds == null) {
                vertexIds = vertIds;
            } else {
                vertexIds.retainAll(vertIds);
            }
        }
        return new ArrayList<String>(vertexIds);
    }

    protected Set<String> accumuloFindVertices(QName attrName, Object attrValue)
        throws TableNotFoundException, QueryException {

        return accumuloFindVerticesKey(attrName, attrValue);
        //return accumuloFindVerticesColumn(attr);
    }

    protected Set<String> accumuloFindVerticesKey(QName attrName, Object attrValue)
        throws TableNotFoundException, QueryException {

        Authorizations auths = new Authorizations(AUTHORIZATION);
        Scanner scan = conn.createScanner(ATTR_TABLE, auths);
        scan.setRange(new Range(attrValue.toString()));
        scan.fetchColumnFamily(new org.apache.hadoop.io.Text(toAccumulo(attrName)));

        Set<String> vertexIds = new HashSet<>();
        for (java.util.Map.Entry<Key, Value> entry : scan) {
            vertexIds.add(entry.getKey().getColumnQualifier().toString());
        }
        scan.close();
        return vertexIds;
    }

    protected List<Vertex> accumuloFindVerticesColumn(QName attrName, Object attrValue)
        throws TableNotFoundException, QueryException {

        Authorizations auths = new Authorizations(AUTHORIZATION);
        Scanner scan = conn.createScanner(GRAPH_TABLE, auths);
        IteratorSetting iter = new IteratorSetting(15, "myFilter", RegExFilter.class);
        String rowRegex = null;
        // column family is the attr name|namespace
        String colfRegex = attrName.getLocalPart() + "\\|" + attrName.getNamespaceURI();
        // column qualifier is the type name|namespace
        String colqRegex = null;
        // value is the value
        String valueRegex = attrValue.toString();
        boolean orFields = false;
        RegExFilter.setRegexs(iter, rowRegex, colfRegex, colqRegex, valueRegex, orFields);
        scan.addScanIterator(iter);

        Set<String> vertexSet = new HashSet<>();                   // could be dups if a vertex is added more than once
        for (java.util.Map.Entry<Key,Value> entry : scan) {
            String rowId = entry.getKey().getRow().toString();
            vertexSet.add(rowId);
        }
        scan.close();

        List<Vertex> vertices = new ArrayList<>();
        for(String id : vertexSet) {
            try {
                vertices.add(getVertex(id));
            } catch (QueryException e) {
                // tried to get vertex information for an edge id (I expect)
                // so just ignore
                logger.debug("Vertex not found", e);
            }
        }
        return vertices;
    }

    protected Vertex getVertex(String id) throws QueryException {
        return getVertex(toQName(id));
    }

    public Vertex getVertex(QName id) throws QueryException {
        try {
            return accumuloGetVertex(id);
        } catch (TableNotFoundException e) {
            throw new QueryException(e);
        }
    }

    protected Vertex accumuloGetVertex(QName id) throws TableNotFoundException, QueryException {
        Authorizations auths = new Authorizations(AUTHORIZATION);
        Scanner scan = conn.createScanner(GRAPH_TABLE, auths);
        scan.setRange(new Range(toAccumulo(id)));

        String vertexType = null;
        Map<QName, Object> attrs = new HashMap<>();
        for (java.util.Map.Entry<Key, Value> entry : scan) {
            String colFamily = entry.getKey().getColumnFamily().toString();
            switch (colFamily) {
            case "vertex_type":
                vertexType = entry.getKey().getColumnQualifier().toString();
                break;
            case "edge_type": break;
            case "edge_forward": break;
            case "edge_backward": break;
            default:
                attrs.put(toQName(entry.getKey().getColumnFamily().toString()),
                          getAttributeValue(entry.getValue().toString(),
                                            toQName(entry.getKey().getColumnQualifier().toString())));
            }
        }
        scan.close();

        if (vertexType == null) {
            throw new QueryException(String.format("unknown vertex '%s'", id));
        } else {
            return buildVertex(id, vertexType, attrs);
        }
    }

    public Edge getEdge(QName id) throws QueryException {
        try {
            return accumuloGetEdge(id);
        } catch (TableNotFoundException e) {
            throw new QueryException(e);
        }
    }

    protected Edge accumuloGetEdge(QName id) throws TableNotFoundException, QueryException {
        Authorizations auths = new Authorizations(AUTHORIZATION);
        Scanner scan = conn.createScanner(GRAPH_TABLE, auths);
        scan.setRange(new Range(toAccumulo(id)));

        String edgeType = null;
        QName sourceId = null;
        QName destId = null;
        Map<QName, Object> attrs = new HashMap<>();
        for (java.util.Map.Entry<Key, Value> entry : scan) {
            String colFamily = entry.getKey().getColumnFamily().toString();
            switch (colFamily) {
            case "vertex_type":
                logger.warn("unexpected vertex_type");
                break;
            case "edge_type":
                edgeType = entry.getKey().getColumnQualifier().toString();
                break;
            case "edge_forward":
                logger.warn("unexpected edge_forward");
                break;
            case "edge_backward":
                logger.warn("unexpected edge_backward");
                break;
            case "edge_source_dest":
                sourceId = toQName(entry.getKey().getColumnQualifier().toString());
                destId = toQName(entry.getValue().toString());
                break;
            default:
                attrs.put(toQName(entry.getKey().getColumnFamily().toString()),
                          getAttributeValue(entry.getValue().toString(),
                                            toQName(entry.getKey().getColumnQualifier().toString())));
            }
        }
        scan.close();

        if (edgeType == null) {
            throw new QueryException(String.format("unknown edge '%s'", id));
        } else {
            return buildEdge(id, edgeType, sourceId, destId, attrs);
        }
    }

    protected static Vertex buildVertex(QName id, String type) throws QueryException {
        switch(type) {
        case "activity":
            return new Activity(id);
        case "agent":
            return new Agent(id);
        case "entity":
            return new Entity(id);
        default:
            throw new QueryException("unknown vertex type "+type);
        }
    }

    protected static Vertex buildVertex(QName id, String type, Map<QName, Object> attrs) throws QueryException {
        switch(type) {
        case "activity":
            return new Activity(id, attrs);
        case "agent":
            return new Agent(id, attrs);
        case "entity":
            return new Entity(id, attrs);
        default:
            throw new QueryException("unknown vertex type "+type);
        }
    }

    protected static Edge buildEdge(QName id, String type, QName source, QName destination) throws QueryException {
        switch(type) {
        case "actedOnBehalfOf":
            return new ActedOnBehalfOf(id, source, destination);
        case "used":
            return new Used(id, source, destination);
        case "wasAssociatedWith":
            return new WasAssociatedWith(id, source, destination);
        case "wasAttributedTo":
            return new WasAttributedTo(id, source, destination);
        case "wasDerivedFrom":
            return new WasDerivedFrom(id, source, destination);
        case "wasGeneratedBy":
            return new WasGeneratedBy(id, source, destination);
        case "wasInformedBy":
            return new WasInformedBy(id, source, destination);
        default:
            throw new QueryException("unknown edge type "+type);
        }
    }

    protected static Edge buildEdge(QName id, String type, QName source, QName destination,
                                    Map<QName, Object> attrs) throws QueryException {
        switch(type) {
        case "actedOnBehalfOf":
            return new ActedOnBehalfOf(id, source, destination, attrs);
        case "used":
            return new Used(id, source, destination, attrs);
        case "wasAssociatedWith":
            return new WasAssociatedWith(id, source, destination, attrs);
        case "wasAttributedTo":
            return new WasAttributedTo(id, source, destination, attrs);
        case "wasDerivedFrom":
            return new WasDerivedFrom(id, source, destination, attrs);
        case "wasGeneratedBy":
            return new WasGeneratedBy(id, source, destination, attrs);
        case "wasInformedBy":
            return new WasInformedBy(id, source, destination, attrs);
        default:
            throw new QueryException("unknown edge type "+type);
        }
    }

    protected static Object getAttributeValue(String value, QName type) {
        if ("boolean".equals(type.getLocalPart())) {
            return Boolean.valueOf(value);
        } else if ("string".equals(type.getLocalPart())) {
            return value;
        } else if ("int".equals(type.getLocalPart())) {
            return Integer.valueOf(value);
        } else if ("long".equals(type.getLocalPart())) {
            return Long.valueOf(value);
        } else if ("float".equals(type.getLocalPart())) {
            return Float.valueOf(value);
        } else if ("double".equals(type.getLocalPart())) {
            return Double.valueOf(value);
        } else {
            // just add it as a string
            return value;
        }
    }

    public Graph ancestors(QName vertexId) throws QueryException {
        try {
            return accumuloAncestors(Collections.singletonList(getVertex(vertexId)));
        } catch (TableNotFoundException e) {
            throw new QueryException(e);
        }
    }

    protected Graph accumuloAncestors(List<Vertex> vertices) throws TableNotFoundException, QueryException {
        return breadthFirstSearch(vertices, true, false);
    }

    public Graph descendants(QName vertexId) throws QueryException {
        return descendants(getVertex(vertexId));
    }

    protected Graph descendants(Vertex vertex) throws QueryException {
        try {
            return accumuloDescendants(Collections.singletonList(vertex));
        } catch (TableNotFoundException e) {
            throw new QueryException(e);
        }
    }

    protected Graph accumuloDescendants(List<Vertex> vertices) throws TableNotFoundException, QueryException {
        return breadthFirstSearch(vertices, false, true);
    }

    protected Graph breadthFirstSearch(List<Vertex> vertices, boolean forward, boolean backward)
        throws QueryException {

        Graph graph = new Graph();
        Set<String> queue = new HashSet<>();
        for (Vertex vert : vertices) {
            queue.add(toAccumulo(vert.getId()));
        }

        Map<QName,QName> sources = new HashMap<>();
        Map<QName,QName> destinations = new HashMap<>();
        Map<QName, Map<QName, Object>> attrs = new HashMap<>();
        while (queue.size() > 0) {
            Authorizations auths = new Authorizations(AUTHORIZATION);
            BatchScanner scan;
            try {
                scan = conn.createBatchScanner(GRAPH_TABLE, auths, 1); // 1 thread
            } catch (TableNotFoundException e) {
                throw new QueryException(e);
            }
            List<Range> ranges = new ArrayList<>();
            for (String id : queue) {
                ranges.add(new Range(id));
            }
            scan.setRanges(ranges);
            queue.clear();

            attrs.clear();
            for (java.util.Map.Entry<Key,Value> entry : scan) {
                QName rowId = toQName(entry.getKey().getRow().toString());
                String colFamily = entry.getKey().getColumnFamily().toString();
                switch (colFamily) {
                case "vertex_type":
                    String type = entry.getKey().getColumnQualifier().toString();
                    if (graph.vertices.containsKey(rowId)) {
                        // it's fine - some of the starting vertices may be ancestors or descendants of others
                        continue;
                    }
                    graph.vertices.put(rowId, buildVertex(rowId, type));
                    break;
                case "edge_type":
                    type = entry.getKey().getColumnQualifier().toString();
                    graph.edges.put(rowId, buildEdge(rowId, type, sources.get(rowId), destinations.get(rowId)));
                    sources.remove(rowId);
                    destinations.remove(rowId);
                    break;
                case "edge_forward":
                    if (!forward) {
                        break;
                    }
                    QName edgeId = toQName(entry.getValue().toString());
                    if (graph.edges.containsKey(edgeId)) {
                        continue;
                    }
                    QName destId = toQName(entry.getKey().getColumnQualifier().toString());
                    queue.add(toAccumulo(destId));
                    queue.add(toAccumulo(edgeId));
                    sources.put(edgeId, rowId);
                    destinations.put(edgeId, destId);
                    break;
                case "edge_backward":
                    if (!backward) {
                        break;
                    }
                    edgeId = toQName(entry.getValue().toString());
                    if (graph.edges.containsKey(edgeId)) {
                        continue;
                    }
                    QName sourceId = toQName(entry.getKey().getColumnQualifier().toString());
                    queue.add(toAccumulo(sourceId));
                    queue.add(toAccumulo(edgeId));
                    sources.put(edgeId, sourceId);
                    destinations.put(edgeId, rowId);
                    break;
                case "edge_source_dest":
                    break;
                default:  // an attribute
                    if (!attrs.containsKey(rowId)) {
                        attrs.put(rowId, new HashMap<QName, Object>());
                    }
                    attrs.get(rowId).put(toQName(entry.getKey().getColumnFamily().toString()),
                                         getAttributeValue(entry.getValue().toString(),
                                                           toQName(entry.getKey().getColumnQualifier().toString())));
                    break;
                }
            }
            scan.close();

            // fill in attributes
            Set<Map.Entry<QName, Map<QName, Object>>> attrSet = attrs.entrySet();
            for (Map.Entry<QName, Map<QName, Object>> attr: attrSet) {
                if (graph.vertices.containsKey(attr.getKey())) {
                    graph.vertices.get(attr.getKey()).set(attr.getValue());
                } else if (graph.edges.containsKey(attr.getKey())) {
                    graph.edges.get(attr.getKey()).set(attr.getValue());
                } else {
                    logger.error(String.format("error: didn't find vertex or edge %s for attribute%n", attr.getKey()));
                }
            }
        }

        return graph;
    }
    
    public Graph connected(QName vertexId) throws QueryException {
        return ancestors(vertexId).union(descendants(vertexId));
    }
    
    public Graph connected(List<QName> vertexIds) throws QueryException {
        Graph graph = new Graph();
        for (QName vertexId : vertexIds) {
            graph = graph.union(connected(vertexId));
        }
        return graph;
    }

    public Graph graph() throws QueryException {
        //TODO: implement
        return null;
    }
    
    public Graph subgraph(QName vertexId) throws QueryException {
        return subgraph(Collections.singletonList(vertexId), DEFAULT_HOPS);
    }
    public Graph subgraph(QName vertexId, int hops) throws QueryException {
        return subgraph(Collections.singletonList(vertexId), hops);
    }

    public Graph subgraph(List<QName> vertexIds) throws QueryException {
        return subgraph(vertexIds, DEFAULT_HOPS);
    }
    public Graph subgraph(List<QName> vertexIds, int hops) throws QueryException {
        Graph graph = new Graph();
        for (QName vertexId : vertexIds) {
            //TODO: use 'hops' 
            graph = graph.union(breadthFirstSearch(Collections.singletonList(getVertex(vertexId)), true, true));
        }
        return graph;
    }

    protected QName toQName(String id) throws QueryException {
        String[] localNamespace = id.split("\\|");
        if (localNamespace.length != 2) {
            throw new QueryException("failed to parse id '" + id + "'");
        }
        return new QName(localNamespace[1], localNamespace[0]);
    }

    protected String toAccumulo(QName id) {
        return id.getLocalPart() + "|" + id.getNamespaceURI();
    }
}
