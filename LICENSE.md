Copyright (c) (2016) Massachusetts Institute of Technology. This material may be reproduced by 
or for the US Government pursuant to the copyright license under DFARS 252.227-7014 (Jun 1995). 
Reproduction/Use of all or any part of this material shall acknowledge the MIT Lincoln Laboratory as
the source under the sponsorship of the US Air Force Contract No. FA8721-05-C-0002.

The software and technical data is being provided to you pursuant to DFARS 252.227-7014, Rights in 
NonCommercial Computer Software and Noncommercial Computer Software Documentation (Jun 1995), 
ALTERNATE 1 (Jun 1995) and DFARS 252.227-7013, Rights in Technical Data - Noncommercial Items 
(Nov.1995), ALTERNATE I (June 1995).  It is copyrighted to the Massachusetts Institute of Technology
and protected as a published work under U.S. copyright law. It has not been approved for public 
release and is subject to applicable U.S. export control statutes and regulations. Therefore, it is
not to be distributed or disseminated in any manner to any party, except the U.S. Government,
without the prior permission of MIT Lincoln Laboratory.  The Program is provided to you on an "AS 
IS" basis.

Commercial use of the subject software will require a license from the MIT Technology Licensing 
Office (TLO). Questions regarding commercial licenses should be directed to the TLO at 617-253-6966.