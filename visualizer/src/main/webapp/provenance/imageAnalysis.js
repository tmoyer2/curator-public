
function createAnalysis(imageName, imageCategory, elementId) {
    document.getElementById(elementId).innerHTML = "loading ...";
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
            document.getElementById(elementId).innerHTML = analysis(imageName, imageCategory, xmlHttp.responseText);
        }
    }
    xmlHttp.open("GET", "../../../api/analysis/" + imageCategory + "/" + imageName + "/analysis.json", true);
    xmlHttp.send(null);
}

function analysis(imageName, imageCategory, responseText) {
    var obj = JSON.parse(responseText);

    var out = "<h1>Provenance Analysis</h1>";
    out += "<p>Status: " + obj.status + "<p>";
    out += "<p>Explanation: " + obj.explain + "<p>";
    return out;
}

