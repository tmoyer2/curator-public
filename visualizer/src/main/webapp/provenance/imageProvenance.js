
'use strict';


/* Function to wipe out the database! (truncate all tables) */
function wipeDatabase(filter1, filter2, elementId) {
    let xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState === 4 && xmlHttp.status !== 200) {
            document.getElementById(elementId).innerHTML = "ERROR: Problem wiping database!";
        }
        else {
            document.getElementById(elementId).innerHTML = "Database successfully wiped!";
        }
    }
    
    let ajaxURI = "../../../api/provenance/clearDatabase.json";
    if (filter2 === null) {
        ajaxURI = "../../api/provenance/clearDatabase.json";
    }
    xmlHttp.open("POST", ajaxURI, true);
    
    xmlHttp.send(null);
    document.getElementById(elementId).innerHTML = "wiping database ...";
}
/**/


/* Landing page search functionality */
let LP_PAGE_SIZE = 10;
function landingPageSearch(elementId, offset) {
    // Make filtering variable names more manageable 
    let filterIDVal = document.graphFilter.idFilter.value;
    let nodeTypeIndex = document.graphFilter.nodeType.selectedIndex;
    let nodeType = document.graphFilter.nodeType.options[nodeTypeIndex].value;
    let attrType = document.graphFilter.attr.value;
    let filterAttrVal = document.graphFilter.attrFilter.value;
    
    // If they gave us a vertex ID, go straight to it 
    if (filterIDVal !== "") {
        document.location = "provenance/"+filterIDVal+"/index.html";
        return;
    }
    
    // Offset defaults to 0 if none given 
    if (typeof(offset) === 'undefined') {
        offset = 0;
    }
    
    // Jetty RESTful hack 
    if (attrType === "" || typeof(attrType) === 'undefined') {attrType = " ";}
    if (filterAttrVal === "" || typeof(filterAttrVal) === 'undefined') {filterAttrVal = " ";}
    
    let xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
            lpSearchCallback(xmlHttp.responseText, elementId, offset);
        }
    }
    
    if (nodeType !== "" && (attrType !== " " || filterAttrVal !== " ")) {
        xmlHttp.open("GET", "api/provenance/type/"+nodeType+"/name/"+attrType+"/value/"+filterAttrVal+"/off/"+offset+"/provenance_search.json", true);
    }
    else if (attrType !== " " || filterAttrVal !== " ") {
        xmlHttp.open("GET", "api/provenance/name/"+attrType+"/value/"+filterAttrVal+"/off/"+offset+"/provenance_search.json", true);
    }
    else if (nodeType !== "") {
        xmlHttp.open("GET", "api/provenance/type/"+nodeType+"/off/"+offset+"/provenance_search.json", true);
    }
    else {
        return;
    }
    xmlHttp.send(null);
    document.getElementById(elementId).innerHTML = "searching ...";
}
function lpSearchCallback(response, elementId, offset) {
    let vertexTypes = ["activity", "entity", "agent"];
    let json = JSON.parse(response);
    jsonGraph = new ProvJsonGraph("", ""); // dummy object 
    
    let resultCnt = 0;
    let responseHTML = "";
    if (Object.keys(json).length === 0) {
        responseHTML = "<i>No results!</i>";
    }
    else {
        for (let i = 0; i < vertexTypes.length; i++) {
            let jsonVertex = json[vertexTypes[i]];
            
            for (let prop in jsonVertex) {
                if (jsonVertex.hasOwnProperty(prop)) {
                    ++resultCnt;
                    responseHTML += "<li onclick=\"document.location='provenance/"+prop+"/index.html'\">" +
                                    "<a href='provenance/"+prop+"/index.html'>"+jsonGraph.buildPropString(prop, jsonVertex[prop])+"</a></li>";
                }
            }
        }
    }
    
    // Display results
    document.getElementById(elementId).innerHTML = "<ul class='resultSet'>" + responseHTML + "</ul>";
    
    // Prev/Next pages of results 
    if (offset > 0) {
        document.getElementById(elementId).innerHTML += "<a href='#' onclick=\"landingPageSearch('"+elementId+"',"+(offset-LP_PAGE_SIZE)+")\">&lt;&nbsp;Prev</a>&nbsp;-&nbsp;";
    }
    if (resultCnt === LP_PAGE_SIZE) {
        document.getElementById(elementId).innerHTML += "<a href='#' onclick=\"landingPageSearch('"+elementId+"',"+(offset+LP_PAGE_SIZE)+")\">Next&nbsp;&gt;</a><br><br>";
    }
}
/**/


/* Left menu bar handler */ 
function toggleMenuStyle(menuItem) {
    let menu = document.getElementById('menu');
    for (let ci = 0; ci < menu.children.length; ci++) {
        menu.children[ci].className = "submenu";
    }
    menuItem.className = "submenu_sel";
}
/**/


/* ProvImage request fulfillment */
function createProvImage(imageName, imageCategory, elementId) {
    document.getElementById(elementId).innerHTML = "<h2>Provenance Graph</h2>\n"
        + "<img src='../../../api/provenance/" + imageCategory + "/" + imageName + "/provenance.png'/>\n"
        + "<p>Download an <a href='../../../api/provenance/" + imageCategory + "/" + imageName + "/provenance.svg'>SVG</a> version</p>\n";
}
/**/


/* ProvN request fulfillment */
function createProvn(imageName, imageCategory, elementId) {
    document.getElementById(elementId).innerHTML = "loading ...";
    let xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
            document.getElementById(elementId).innerHTML = provn(imageName, imageCategory, xmlHttp.responseText);
        }
    }
    xmlHttp.open("GET", "../../../api/provenance/" + imageCategory + "/" + imageName + "/provenance.provn", true);
    xmlHttp.send(null);
}

// Callback ProvN
function provn(imageName, imageCategory, responseText) {
    let out = "<h2>PROV-N</h2>";
    out += "<pre>";
    out += responseText;
    out += "</pre>";
    return out;
}
/**/


/* ProvJSON request fulfillment */
function createProvJson(filter1, filter2, elementId, type) {
    document.getElementById(elementId).innerHTML = "loading ...";
    
    // Instantiate a new JSON Graph object 
    jsonGraph = new ProvJsonGraph(filter1, filter2);
    
    let typeStrReq = "";
    if (type === jsonGraph.nodeStyles.AGENT) {
        typeStrReq = "_behalf";
    }
    else if (type === jsonGraph.nodeStyles.ENTITY) {
        typeStrReq = "_derived";
    }
    else if (type === jsonGraph.nodeStyles.ACTIVITY) {
        typeStrReq = "_informed";
    }
    
    let xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
            document.getElementById(elementId).innerHTML = provJson(filter1, filter2, type, xmlHttp.responseText);
        }
    }
    if (filter2 === null) {
        xmlHttp.open("GET", "../../api/provenance/" + filter1 + "/provenance" + typeStrReq + ".json", true);
    }
    else {
        xmlHttp.open("GET", "../../../api/provenance/" + filter2 + "/" + filter1 + "/provenance" + typeStrReq + ".json", true);
    }
    xmlHttp.send(null);
}

// Callback ProvJSON 
function provJson(filter1, filter2, type, responseText) {
    let typeDisplay = "Provenance";
    if (type === jsonGraph.nodeStyles.AGENT) {
        typeDisplay = "Agents";
    }
    else if (type === jsonGraph.nodeStyles.ENTITY) {
        typeDisplay = "Derived";
    }
    else if (type === jsonGraph.nodeStyles.ACTIVITY) {
        typeDisplay = "Action";
    }
    
    // Build output string 
    let filter2Str = (filter2 === null) ? filter2 : "'"+filter2+"'";
    let out = "<h2>PROV-JSON ("+typeDisplay+")</h2>";
    
    if (filter1 !== "*" && filter2 !== "*") {
        out += "<div><a href=\"javascript:createProvJson('"+filter1+"',"+filter2Str+",'contents')\">Provenance</a> &mdash; "
            + "<a href=\"javascript:createProvJson('"+filter1+"',"+filter2Str+",'contents','"+jsonGraph.nodeStyles.AGENT+"')\">Agents</a> &mdash; "
            + "<a href=\"javascript:createProvJson('"+filter1+"',"+filter2Str+",'contents','"+jsonGraph.nodeStyles.ACTIVITY+"')\">Action</a> &mdash; "
            + "<a href=\"javascript:createProvJson('"+filter1+"',"+filter2Str+",'contents','"+jsonGraph.nodeStyles.ENTITY+"')\">Derived</a></div>";
    }
    
    out += "<pre>" + responseText + "</pre>";
    
    return out;
}
/**/


/* HTTP Request for JSON-based Provenance Graph */
// Initial - positioning data
var jsonGraph = null;
function createProvJsonGraph(filter1, filter2, elementId, type) {
    document.getElementById(elementId).innerHTML = "loading ...";
    
    // Instantiate a new JSON Graph object 
    jsonGraph = new ProvJsonGraph(filter1, filter2);
    
    let typeStrReq = "";
    let typeDisplay = "Provenance";
    if (type === jsonGraph.nodeStyles.AGENT) {
        typeStrReq = "_behalf";
        typeDisplay = "Agents";
    }
    else if (type === jsonGraph.nodeStyles.ENTITY) {
        typeStrReq = "_derived";
        typeDisplay = "Derived";
    }
    else if (type === jsonGraph.nodeStyles.ACTIVITY) {
        typeStrReq = "_informed";
        typeDisplay = "Action";
    }
    
    let xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
            let jsonText = xmlHttp.responseText;
            
            // Load positional 
            jsonGraph.loadPositional(jsonText);
            
            // Load data 
            jsonGraph.buildJsonGraph(jsonText);
            let filter2Str = (filter2 === null) ? filter2 : "'"+filter2+"'";
            
            let deluxeLinks = "";
            if (filter1 !== "*" && filter2 !== "*") {
                deluxeLinks = "<div><a href=\"javascript:createProvJsonGraph('"+filter1+"',"+filter2Str+",'contents')\">Provenance</a> &mdash; "
                    + "<a href=\"javascript:createProvJsonGraph('"+filter1+"',"+filter2Str+",'contents','"+jsonGraph.nodeStyles.AGENT+"')\">Agents</a> &mdash; "
                    + "<a href=\"javascript:createProvJsonGraph('"+filter1+"',"+filter2Str+",'contents','"+jsonGraph.nodeStyles.ACTIVITY+"')\">Action</a> &mdash; "
                    + "<a href=\"javascript:createProvJsonGraph('"+filter1+"',"+filter2Str+",'contents','"+jsonGraph.nodeStyles.ENTITY+"')\">Derived</a></div>";
            }
            document.getElementById(elementId).innerHTML = "<h2>"+typeDisplay+" Graph (JS)</h2>\n"
                + deluxeLinks
                + "<div id='id01-graph' style='width:75%;float:left;height:90%;'></div>\n"
                + "<div style='width:20%;overflow:auto;float:left;height:90%;'>"
                + jsonGraph.buildFilterBox(type)
                + "<div id='id01-table' style='height:75%;overflow:auto;'></div></div>\n";
            jsonGraph.displayJsonGraph("id01-graph", "id01-table");
        }
    }
    if (filter2 === null) {
        xmlHttp.open("GET", "../../api/provenance/" + filter1 + "/provenance" + typeStrReq + ".json", true);
    }
    else {
        xmlHttp.open("GET", "../../../api/provenance/" + filter2 + "/" + filter1 + "/provenance" + typeStrReq + ".json", true);
    }
    xmlHttp.send(null);
}
/**/


/* Class for the Prov JSON Graph generation */
function ProvJsonGraph(filter1, filter2) {
    'use strict';
    
    this.DEBUG = true;      // output useful debug info 
    
    // For RESTful URI creation 
    this.filter1 = filter1;
    this.filter2 = filter2;
    
    // Radius of graph hops allowed (should be n-1 of the back-end hops)
    this.vertexHops = 3;
    
    // x-y coordinates for the graph (suggested by GraphViz) 
    this.positional = null;
    
    // vis.js containers 
    this.network = null;    // vis.js graph object 
    this.container = null;  // container graph is canvased onto 
    this.infoTable = null;  // container table is loaded into 
    
    // graph components 
    this.agentList = null;  // agents guide the timeline 
    this.nodes_vs = null;   // vis.js data set wrapper 
    this.edges_vs = null;   // vis.js data set wrapper 
    this.attrBank = {};     // all node attributes possible in graph 
    
    this.BIG_GRAPH = 100;   // define what "big" means (enables fog of war)  
    
    this.prefixes = {};     // prov prefix mappings 
    this.LL_NS = "http://www.ll.mit.edu/provenance/application";    // LL prefix 
    
    
    // Load positional data 
    this.loadPositional = function(positional) {
        this.positional = JSON.parse(positional).prov_pos;
    };
    
    
    // Is this a big graph? (can be as complex as necessary) 
    this.isBigGraph = function() {
        return (this.nodes_vs !== null && this.nodes_vs.length > this.BIG_GRAPH);
    };
    
    
    // Style scheme for each type of node / enum bank of possible styles 
    this.nodeStyles = {
        ACTIVITY: "activity", 
        ENTITY: "entity", 
        AGENT: "agent",
        properties: {
            "activity" : {shape:"box", color:"#9fb1fc"},
            "entity" : {shape:"ellipse", color:"#fffc87"},
            "agent" : {shape:"box", color:"#fdb266"},
        }
    };
    
    
    // All possible edge mappings between nodes 
    this.edgeMappings = {
        wasGeneratedBy : {
            srcTitle:"prov:entity", 
            srcType:"entity", 
            destTitle:"prov:activity", 
            destType:"activity", 
            label:"gen"
        },
        used : {
            srcTitle:"prov:activity", 
            srcType:"activity", 
            destTitle:"prov:entity", 
            destType:"entity", 
            label:"used"
        },
        wasDerivedFrom : {
            srcTitle:"prov:generatedEntity", 
            srcType:"entity", 
            destTitle:"prov:usedEntity", 
            destType:"entity", 
            label:"der"
        },
        wasAssociatedWith : {
            srcTitle:"prov:activity", 
            srcType:"activity", 
            destTitle:"prov:agent", 
            destType:"agent", 
            label:"assoc"
        },
        actedOnBehalfOf : {
            srcTitle:"prov:responsible", 
            srcType:"agent", 
            destTitle:"prov:delegate", 
            destType:"agent", 
            label:"behalf"
        },
        wasAttributedTo : {
            srcTitle:"prov:entity", 
            srcType:"entity", 
            destTitle:"prov:agent", 
            destType:"agent", 
            label:"attr"
        },
        wasInformedBy : {
            srcTitle:"prov:informed", 
            srcType:"activity", 
            destTitle:"prov:informant", 
            destType:"activity", 
            label:"inform"
        },
    };
    
    
    /* Allows user to filter visible graph */
    this.doFilterGraph = function(event) {
        var self = this;
        
        // Make filtering variable names more manageable 
        let filterIDVal = document.graphFilter.idFilter.value;
        let nodeTypeIndex = document.graphFilter.nodeType.selectedIndex;
        let nodeType = document.graphFilter.nodeType.options[nodeTypeIndex].value;
        let attrType = document.graphFilter.attr.value;
        let filterAttrVal = document.graphFilter.attrFilter.value;
        
        // For ID match, there can be only one 
        if (typeof(filterIDVal) !== "undefined" && filterIDVal !== "") {
            if (this.nodes_vs.get(filterIDVal) === null) {
                // Invalid ID specified 
                this.buildPropTable([]);
                return false;
            }
            
            let filterIDs = [];
            filterIDs.push(document.graphFilter.idFilter.value);
            this.selectNode({nodes:filterIDs});
        }
        else if (typeof(filterAttrVal) !== "undefined" && filterAttrVal !== "") {
            let xmlHttp = new XMLHttpRequest();
            xmlHttp.onreadystatechange = function() {
                if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
                    let results = JSON.parse(xmlHttp.responseText);
                    
                    // Convert ID array into node object array
                    let resultSet = [];
                    for (let i = 0; i < results.length; i++) {
                        let node = self.nodes_vs.get(results[i]);
                        if (node === null) continue;
                        
                        // If node type filtering specified, ensure results match 
                        if (typeof(nodeType) !== "undefined" && nodeType !== "") {
                            if (node.nodeType !== nodeType) continue;
                        }
                        
                        resultSet.push(node);
                    }
                    self.buildPropTable(resultSet);
                }
            }
            if (this.filter2 === null) {
                xmlHttp.open("GET", "../../api/provenance/" + this.filter1 + "/" + attrType + "/" + filterAttrVal + "/provenance_search_s.json", true);
            }
            else {
                xmlHttp.open("GET", "../../../api/provenance/" + this.filter2 + "/" + this.filter1 + "/" + attrType + "/" + filterAttrVal + "/provenance_search_s.json", true);
            }
            xmlHttp.send(null);
        }
        else if (typeof(nodeType) !== "undefined" && nodeType !== "") {
            let allIDs = this.nodes_vs.getIds();
            let resultSet = [];
            for (let ni = 0; ni < allIDs.length; ni++) {
                let node = this.nodes_vs.get(allIDs[ni]);
                if (node.nodeType !== nodeType) continue;
                
                resultSet.push(node);
            }
            this.buildPropTable(resultSet);
        }
        else {
            this.network.unselectAll();
            this.buildPropTable();
            this.network.fit();
        }
        
        return false;
    };
    
    
    /* Build input box interface for filtering */ 
    this.buildFilterBox = function(defaultType) {
        let str = "";
        
        // Start form
        str += "<form name='graphFilter' onsubmit='return false;'>";
        
        // Node types
        str += "<label><b>Type: </b><select name='nodeType'>\n";
        str += "<option value=''>*</option>\n";
        for (let type in this.nodeStyles.properties) {
            if (this.nodeStyles.properties.hasOwnProperty(type)) {
                let typeSelected = "";
                if (type === defaultType) {
                    typeSelected = " selected='selected'";
                }
                str += "<option value='" + type + "'" + typeSelected + ">" + type + "</option>\n";
            }
        }
        str += "</select></label><br>\n";
        
        // ID 
        str += "<label><b>ID: </b><input type='text' name='idFilter' value='' style='width:70%;'></label><br>\n";
        
        // Date range
        //str += "<label><b>After: </b><input type='datetime' name='startDT' /></label>&nbsp;"
        //        + "<label><b>Before: </b><input type='datetime' name='endDT' /></label><br>\n";
        
        // Attributes 
        str += "<label><b>Attr: </b><input type='text' name='attr' value=''>\n";
        str += "</label>&nbsp;<input type='text' name='attrFilter' value=''><br>\n";
        
        // Close form
        str += "<button>Filter</button></form><br>";
            
        return str;
    };
    
    
    /* Builds a property string out of an object's properties */
    this.buildPropString = function(id, obj) {
        let propstr = "<b>ID</b> : " + id + "<br>\n";
        for (let prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                let propVal = obj[prop]["$"];
                
                // Add to attribute bank when we see it 
                if (!(prop in this.attrBank)) {
                    this.attrBank[prop] = {};
                }
                ++this.attrBank[prop][propVal];
                
                propstr += "<b>" +prop + "</b> : " + propVal + "<br>\n";
            }
        }
        return propstr;
    };
    
    
    /* Builds a property table out of an object's properties */
    this.buildPropTable = function(obj) {
        if (Array.isArray(obj)) {
            this.buildPropTableSet(obj);
        }
        else {
            this.buildPropTableSingle(obj);
        }
    };
    this.buildPropTableSet = function(array) {
        // If empty array provided, assume nothing selected 
        if (typeof array === "undefined" || array.length === 0) {
            this.infoTable.innerHTML = "No results";
            return;
        }
        
        let propstr = "<h3>Results:</h3>\n";
        for (let i = 0; i < array.length; i++) {
            let id = array[i].id;
            let props = array[i].props;
            propstr += "<table border='1' cellpadding='5' style='cursor:pointer;' "
                        + "onclick=\"jsonGraph.selectNode({nodes:['" + id + "']});jsonGraph.highlightNode({nodes:[],traceopt:false})\" "
                        + "onmouseover=\"this.style.backgroundColor='#fee';jsonGraph.highlightNode({nodes:['" + id + "'],traceopt:false})\" "
                        + "onmouseout=\"this.style.backgroundColor='#fff';jsonGraph.highlightNode({nodes:[],traceopt:false})\">\n"
                        + "<tr><td><b>ID</b></td><td>" 
                        + id + "</td></tr>\n";
            for (let prop in props) {
                if (props.hasOwnProperty(prop)) {
                    propstr += "<tr><td><b>" + prop + "</b></td><td>" 
                            + props[prop]["$"] + "</td></tr>\n";
                }
            }
            propstr += "</table><br>";
        }
        
        this.infoTable.innerHTML = propstr;
    };
    this.buildPropTableSingle = function(node) {
        // If nothing provided, assume nothing selected 
        if (node === null || typeof(node) === 'undefined') {
            this.infoTable.innerHTML = "";
            return;
        }
        
        let id = node.id;
        let props = node.props;
        let URI = "../"+ id + "/index.html";
        if (this.filter2 !== null) {
            URI = "../../"+ id + "/index.html";
        } 
        let propstr = "<h3>Selected:</h3><table border='1' cellpadding='5' bgcolor='#ebb'>\n"
                    + "<tr><td><b>ID</b></td><td><a href='" + URI + "'>" 
                    + id + "</a></td></tr>\n";
        for (let prop in props) {
            if (props.hasOwnProperty(prop)) {
                propstr += "<tr><td><b>" + prop + "</b></td><td>" 
                        + props[prop]["$"] + "</td></tr>\n";
            }
        }
        propstr += "</table><br>";
        
        
        // Crawl edges to get descendants and antecedents 
        let ancestors = "";
        let descendants = "";
        let connEdges = this.network.getConnectedEdges(id);
        for (let ei = 0; ei < connEdges.length; ei++) {
            let edge = this.edges_vs.get(connEdges[ei]);
            
            // Pick our target node 
            let nextNodeID = edge.from;
            if (edge.from === id) {
                nextNodeID = edge.to;
            }
            
            // Build table for this node 
            let nextNode = this.nodes_vs.get(nextNodeID);
            let str = "<table border='1' cellpadding='5' style='cursor:pointer;' "
                    + "onclick=\"jsonGraph.selectNode({nodes:['" + nextNodeID + "']});jsonGraph.highlightNode({nodes:[],traceopt:false})\" "
                    + "onmouseover=\"this.style.backgroundColor='#fee';jsonGraph.highlightNode({nodes:['" + nextNodeID + "'],traceopt:false})\" "
                    + "onmouseout=\"this.style.backgroundColor='#fff';jsonGraph.highlightNode({nodes:['" + id + "'],traceopt:false})\">\n"
                    + "<tr><td><b>ID</b></td><td>" 
                    + nextNodeID + "</td></tr>\n";
            for (let prop in nextNode.props) {
                if (nextNode.props.hasOwnProperty(prop)) {
                    str += "<tr><td><b>" + prop + "</b></td><td>" 
                        + nextNode.props[prop]["$"] + "</td></tr>\n";
                }
            }
            str += "</table><br>";
            
            // Append or prepend (parent or child) 
            if (
                edge.from === id && edge.arrows.from === true 
                || edge.to === id && edge.arrows.to === true
            ) {
                descendants += str;
            }
            else {
                ancestors += str;
            }
        }
        
        if (ancestors != "") {
            ancestors = "<h3>Ancestors:</h3>" + ancestors;
        }
        if (descendants != "") {
            descendants = "<h3>Descendants:</h3>" + descendants;
        }
        
        document.graphFilter.idFilter.value = id;
        this.infoTable.innerHTML = ancestors + propstr + descendants;
    };
    
    
    /* Return whether node at given position is within view for user */
    this.isNodeWithinView = function(nodeID) {
        let poss = this.network.getPositions(nodeID);
        let domPoss = this.network.canvasToDOM(poss[nodeID]);
        let visible = true;
        let bufZone = 1000;
        if (
            domPoss.x < -bufZone 
            || domPoss.y < -bufZone 
            || domPoss.x > this.network.body.container.clientWidth+bufZone 
            || domPoss.y > this.network.body.container.clientHeight+bufZone
        ) {
            visible = false;
        }
        return visible;
    };
    
    
    // Which map reduction function should we use? 
    this.fogOfWar = function() {this.edgeFogOfWar()};
    
    
    /* Hides nodes not within viewport of canvas (speed up) */
    //TODO: This code causes canvas to be resized, resulting in 
    //  the focus point jumping around! (Not used until we can fix) 
    this.fullFogOfWar = function() {
        let newnodes = [];
        let allIDs = this.nodes_vs.getIds();
        for (let ni = 0; ni < allIDs.length; ni++) {
            let hideNode = !this.isNodeWithinView(allIDs[ni]);
            
            // Only mark node if there's been a change in hidden state 
            if (this.nodes_vs.get(allIDs[ni]).hidden !== hideNode) {
                newnodes.push({'id':allIDs[ni],'hidden':hideNode});
            }
        }
        
        this.nodes_vs.update(newnodes);
        
        // Now take care of "orphaned" edges with a hidden node 
        let newedges = [];
        for (let ni = 0; ni < newnodes.length; ni++) {
            // Get node ID
            let nodeid = newnodes[ni].id;
            
            // Set visibility for all edges of this node 
            let connEdges = this.network.getConnectedEdges(nodeid);
            for (let ei = 0; ei < connEdges.length; ei++) {
                let hidden = newnodes[ni].hidden;
                
                // This node isn't hidden -- check connected nodes 
                if (!hidden) {
                    let connNodes = this.network.getConnectedNodes(connEdges[ei]);
                    for (let cni = 0; cni < connNodes.length; cni++) {
                        if (this.nodes_vs.get(connNodes[cni]).hidden) {
                            hidden = true;
                            break;
                        }
                    }
                }
                
                newedges.push({'id':connEdges[ei],'hidden':hidden});
            }
        }
        this.edges_vs.update(newedges);
    };
    
    
    /* Hides edges not within viewport of canvas (speed up) */
    this.edgeFogOfWar = function() {
        let newedges = [];
        let allIDs = this.edges_vs.getIds();
        for (let ei = 0; ei < allIDs.length; ei++) {
            let hideEdge = true;
            let toNode = this.edges_vs.get(allIDs[ei]).to;
            let fromNode = this.edges_vs.get(allIDs[ei]).from;
            
            // Only hide edge if all conn. nodes are out of view 
            if (this.isNodeWithinView(toNode) || this.isNodeWithinView(fromNode)) {
                hideEdge = false;
            }
            
            // Only mark edge if there's been a change in hidden state 
            if (this.edges_vs.get(allIDs[ei]).hidden !== hideEdge) {
                newedges.push({'id':allIDs[ei],'hidden':hideEdge});
            }
        }
        
        this.edges_vs.update(newedges);
    };
    
    
    /* Bulk unhide everything (e.g., zoom out) -- undoes fog of war */
    this.unhideAllElements = function() {
        let types = {'edges':this.edges_vs, 'nodes':this.nodes_vs};
        for (let type in types) {
            if (types.hasOwnProperty(type)) {
                let newlist = [];
                let allIDs = types[type].getIds();
                for (let ei = 0; ei < allIDs.length; ei++) {
                    // Only mark if there's been a change in hidden state 
                    if (types[type].get(allIDs[ei]).hidden !== false) {
                        newlist.push({'id':allIDs[ei],'hidden':false});
                    }
                }
                types[type].update(newlist);
            }
        }
    };
    
    
    /* Prune graph by hop count */
    this.pruneGraphByHops = function(startNode, hops, nodeprops, edgeprops, seenlist) {
        // Disabled for dual-filter mode 
        if (this.filter2 !== null) {return;}
        
        if (typeof(startNode) === 'undefined') {
            startNode = "ns1:"+this.filter1;
        }
        if (typeof(hops) === 'undefined') {
            hops = this.vertexHops;
        }
        if (typeof(nodeprops) === 'undefined') {
            nodeprops = [];
        }
        if (typeof(edgeprops) === 'undefined') {
            edgeprops = [];
        }
        if (typeof(seenlist) === 'undefined') {
            seenlist = {};
        }
        
        seenlist[startNode] = true;
        let children = this.network.getConnectedNodes(startNode);
        for (let ni = 0; ni < children.length; ni++) {
            if (children[ni] in seenlist) {
                continue;
            }
            seenlist[children[ni]] = true;
            
            if (hops <= 0) {
                // Add style to hidden node 
                let nodeStyle = {
                    'id':children[ni],
                    'color':{'background':'#ffffff','border':'#efefef'},
                    'oldColor':{'background':'#ffffff','border':'#efefef'},
                    'font':{'color':'#efefef'}
                };
                nodeprops.push(nodeStyle);
                
                // Add style to hidden edge 
                let edges = this.network.getConnectedEdges(startNode);
                for (let ei = 0; ei < edges.length; ei++) {
                    let edge = this.edges_vs.get(edges[ei]);
                    if (
                        (edge.from === startNode || edge.to === startNode) && 
                        (edge.from === children[ni] || edge.to === children[ni])
                    ) {
                        let edgeStyle = {
                            'id':edges[ei],
                            'color':'#efefef',
                            'font':{'color':'#efefef'}
                        };
                        edgeprops.push(edgeStyle);
                        break;
                    }
                }
            }
            
            this.pruneGraphByHops(children[ni], hops-1, nodeprops, edgeprops, seenlist);
        }
    };
    
    
    /* Get node's position as suggested by GraphViz */
    this.getNodePosition = function(nodeID) {
        if (!nodeID in this.positional) {
            return null;
        }
        
        let pos = this.positional[nodeID];
        if (pos === null || typeof(pos) === 'undefined') {
            return null;
        }
        let x = pos["x"]*50;
        let y = pos["y"]*750;
        
        return {x:x,y:y};
    };
    
    
    /* Loads and builds graph from raw JSON data */
    this.buildJsonGraph = function(responseText) {
        let rawJsonGraph = JSON.parse(responseText).prov;
        
        this.nodes_vs = new vis.DataSet();
        this.edges_vs = new vis.DataSet();
        
        // set up prefixes 
        let llPrefix = "";
        for (let prefix in rawJsonGraph["prefix"]) {
            if (rawJsonGraph["prefix"][prefix] === this.LL_NS) {
                llPrefix = prefix;
            }
            this.prefixes[prefix] = rawJsonGraph["prefix"][prefix];
        }
        
        // add nodes
        for (let nodeType in this.nodeStyles.properties) {
            if (!this.nodeStyles.properties.hasOwnProperty(nodeType)) {
                continue;
            }
            
            // loop nodes of this type 
            for (let key in rawJsonGraph[nodeType]) {
                if (rawJsonGraph[nodeType].hasOwnProperty(key)) {
                    // skip if already added 
                    if (this.nodes_vs.get(key) !== null) {
                        continue;
                    }
                    
                    let pos = this.getNodePosition(key);
                    if (pos === null) {
                        // If no GraphViz-based position, default to library std. 
                        pos = {x:undefined,y:undefined};
                    }
                    let propstr = this.buildPropString(key, rawJsonGraph[nodeType][key]);
                    let obj = {
                            id: key, 
                            label: key.substr(0,17), 
                            shape: this.nodeStyles.properties[nodeType]["shape"], 
                            nodeType: nodeType,
                            x: pos["x"], 
                            y: pos["y"], 
                            color: this.nodeStyles.properties[nodeType]["color"], 
                            oldColor: this.nodeStyles.properties[nodeType]["color"], 
                            title: propstr,
                            props: rawJsonGraph[nodeType][key]
                    };
                    this.nodes_vs.add(obj);
                    
                    // if we want to later include properties as node graph 
                    //this.nodes_vs.add({id: key+"-prop", label: propstr, 'shape': 'box', color: 'white'});
                    //this.edges_vs.add({from: key+"-prop", to: key});
                    
                    // remove processed node 
                    delete rawJsonGraph[nodeType][key];
                }
            }
        }
        
        // add edges
        let weightTitle = ""+llPrefix+":weight";
        for (let edgeType in this.edgeMappings) {
            if (!this.edgeMappings.hasOwnProperty(edgeType)) {
                continue;
            }
            
            for (let edgeID in rawJsonGraph[edgeType]) {
                if (!rawJsonGraph[edgeType].hasOwnProperty(edgeID)) {
                    continue;
                }
                
                let startTitle = this.edgeMappings[edgeType]["srcTitle"];
                let startID = rawJsonGraph[edgeType][edgeID][startTitle];
                let endTitle = this.edgeMappings[edgeType]["destTitle"];
                let endID = rawJsonGraph[edgeType][edgeID][endTitle];
                
                // Figure out weight of edge (if present) 
                let edgeWeight = 1;
                if (weightTitle in rawJsonGraph[edgeType][edgeID]) {
                    edgeWeight = rawJsonGraph[edgeType][edgeID][weightTitle]["$"];
                }
                
                // add new edge
                let obj = {
                    from: startID, 
                    to: endID, 
                    label: this.edgeMappings[edgeType]["label"] + " (" + edgeWeight + ")", 
                    title: "<b>Type:</b> " + edgeType 
                            + "<br>\n<b>Start:</b> " + startID 
                            + "<br>\n<b>End:</b> " + endID 
                            + "<br>\n<b>Connections:</b> " + edgeWeight, 
                    arrows: {to:true},
                    scaling: {min:1,max:4,label:{enabled:false}},
                    value: edgeWeight
                };
                this.edges_vs.add(obj);
                
                // remove processed edge 
                delete rawJsonGraph[edgeType][edgeID];
            }
        }
        
        // dump remaining nodes/edges for debugging purposes 
        for (let nodeType in rawJsonGraph) {
            if (!rawJsonGraph.hasOwnProperty(nodeType)) {
                continue;
            }
            
            if (this.DEBUG) {
                console.log(nodeType + ": ");
                console.log(rawJsonGraph[nodeType]);
            }
        }
    };
    
    
    /* Entrance function to displaying graph */
    this.displayJsonGraph = function(graphElementId, tableElementID) {
        var self = this; // closures make us forget who 'this' is (evt handlers) 
        
        // create a network
        this.container = document.getElementById(graphElementId);
        this.infoTable = document.getElementById(tableElementID);
        document.graphFilter.onsubmit = function(event) {return self.doFilterGraph(event);};
        let data = {
            nodes: this.nodes_vs,
            edges: this.edges_vs
        };
        
        // Hide edges on drag for big graphs 
        let hideEdges = false;
        if (this.isBigGraph()) {
            hideEdges = true;
        }
        
        // Set up general graph options 
        let physicsOpts = false;
        // if full-agent view requested, disable graphviz-based positions
        if (this.filter1 === "*" || this.filter2 === "*") {
            physicsOpts = {
                stabilization: false,
                barnesHut: {
                    gravitationalConstant: -80000,
                    springConstant: 0.001,
                    springLength: 200
                }
            };
        }
        let options = {
            height: '80%',
            width: '90%',
            edges: {
                color: {color: 'black', hover: 'grey', highlight: 'red',}, 
                font: {background:'white',},
                smooth: false,
            },
            interaction:{
                hover: true,
                hideEdgesOnDrag: hideEdges,
                hoverConnectedEdges: true,
            },
            physics: physicsOpts,
        };
        this.network = new vis.Network(this.container, data, options);
        
        // Make sure all nodes fit on screen (pretty) 
        this.network.fit();
        
        // Prune graph to given number of hops (if applicable) 
        if (this.filter2 === null) {
            let nodeprops = [];
            let edgeprops = [];
            this.pruneGraphByHops("ns1:"+this.filter1, this.vertexHops, nodeprops, edgeprops, {});
            this.nodes_vs.update(nodeprops);
            this.edges_vs.update(edgeprops);
        }
        
        
        // Attach auto-complete filter field functionality 
        let autocomplete_attrkeys = new autoComplete({
            selector: document.graphFilter.attr,
            minChars: 2,
            cache: false,
            source: function(term, suggest){
                term = term.toLowerCase();
                let choices = Object.keys(self.attrBank);
                let matches = [];
                for (let i = 0; i < choices.length; i++)
                    if (choices[i].toLowerCase().indexOf(term) >= 0) matches.push(choices[i]);
                suggest(matches);
            }
        });
        let autocomplete_attrfilter = new autoComplete({
            selector: document.graphFilter.attrFilter,
            minChars: 2,
            cache: false,
            source: function(term, suggest){
                if (document.graphFilter.attr.value === "") return;
                term = term.toLowerCase();
                let choices = Object.keys(self.attrBank[document.graphFilter.attr.value]);
                let matches = [];
                for (let i = 0; i < choices.length; i++)
                    if (choices[i].toLowerCase().indexOf(term) >= 0) matches.push(choices[i]);
                suggest(matches);
            }
        });
        let autocomplete_ids = new autoComplete({
            selector: document.graphFilter.idFilter,
            minChars: 2,
            cache: true,
            source: function(term, suggest){
                term = term.toLowerCase();
                let choices = self.nodes_vs.getIds();
                let matches = [];
                for (let i = 0; i < choices.length; i++)
                    if (choices[i].toLowerCase().indexOf(term) >= 0) matches.push(choices[i]);
                suggest(matches);
            }
        });
        
        // Event listeners
        this.network.on('dragStart', function(params) {
            let cursorStyle = "grab"
            if (params.nodes.length > 0) {
                cursorStyle = 'move';
            }
            self.container.getElementsByTagName("canvas")[0].style.cursor = cursorStyle;
        });
        this.network.on('dragging', function(params) {
            let cursorStyle = "grabbing"
            if (params.nodes.length > 0) {
                cursorStyle = 'move';
            }
            self.container.getElementsByTagName("canvas")[0].style.cursor = cursorStyle;
        });
        this.network.on('dragEnd', function(params) {
            self.container.getElementsByTagName("canvas")[0].style.cursor = 'default';
            if (params.nodes.length === 0 && self.isBigGraph()) {
                self.fogOfWar();
            }
        });
        this.network.on('hoverNode', function(params) {
            self.container.getElementsByTagName("canvas")[0].style.cursor = 'pointer';
        });
        this.network.on('blurNode', function(params) {
            self.container.getElementsByTagName("canvas")[0].style.cursor = 'default';
        });
        this.network.on('doubleClick', function(params) {
            if (params.nodes.length === 0) {
                self.network.fit({animation:{duration: 250}});
                
                // un-hide all nodes 
                if (self.isBigGraph()) {
                    self.unhideAllElements();
                }
            }
        });
        this.highlightNode = function(params) {
            if (!params.nodes) {
                params.nodes = [];
            }
            
            let props = {'node': [], 'edge': []};
            let allNodeIDs = this.nodes_vs.getIds();
            let allEdgeIDs = this.edges_vs.getIds();
            
            // Highlight selected nodes (un-highlight others) 
            let relatedEdges = [];
            for (let ei = 0; ei < allNodeIDs.length; ei++) {
                
                // Don't trace path (only highlight selected node) 
                if (params.traceopt === false) {
                    if (params.nodes.indexOf(allNodeIDs[ei]) >= 0) {
                        // highlight node 
                        props['node'].push({'id':allNodeIDs[ei],'borderWidth':3,'color':{'border':'#B2C'}});
                    }
                    else {
                        // recover original color properties 
                        let oldColor = this.nodes_vs.get(allNodeIDs[ei]).oldColor;
                        props['node'].push({'id':allNodeIDs[ei],'borderWidth':1,'color':{'border':oldColor}});
                    }
                }
                else {
                    // Trace path (highlight node and edges) 
                    
                    if (params.nodes.length === 0) {
                        // recover original color properties 
                        let oldColor = this.nodes_vs.get(allNodeIDs[ei]).oldColor;
                        props['node'].push({'id':allNodeIDs[ei],'borderWidth':1,'color':oldColor});
                    }
                    else if (params.nodes.indexOf(allNodeIDs[ei]) >= 0) {
                        // highlight node 
                        let oldColor = this.nodes_vs.get(allNodeIDs[ei]).oldColor;
                        props['node'].push({'id':allNodeIDs[ei],'borderWidth':3,'color':{'background':oldColor}});
                        
                        let edges = self.network.getConnectedEdges(allNodeIDs[ei]);
                        relatedEdges = relatedEdges.concat(edges);
                    }
                    else {
                        // fade unrelated nodes? 
                        props['node'].push({'id':allNodeIDs[ei],'borderWidth':1,'color':{'background':'#EEE'}});
                    }
                }
            }
            
            // Highlight related edges 
            if (params.traceopt !== false) {
                for (let ei = 0; ei < allEdgeIDs.length; ei++) {
                    if (params.nodes.length === 0) {
                        props['edge'].push({'id':allEdgeIDs[ei],'color':'#000'});
                    }
                    else if (relatedEdges.indexOf(allEdgeIDs[ei]) >= 0) {
                        // highlight edge  
                        props['edge'].push({'id':allEdgeIDs[ei],'color':'#F00'});
                    }
                    else {
                        // fade unrelated edges 
                        props['edge'].push({'id':allEdgeIDs[ei],'color':'#EEE'});
                    }
                }
            }
            
            this.nodes_vs.update(props['node']);
            this.edges_vs.update(props['edge']);
        };
        this.selectNode = function(params) {
            if (!params.nodes || params.nodes.length < 1) return;
            
            // tell graph to select node (explicit) 
            self.network.selectNodes(params.nodes);
            let connNodes = self.network.getConnectedNodes(params.nodes);
            
            let options = {
                nodes: params.nodes.concat(connNodes), // fit connected nodes 
                animation: {
                    duration: 250,
                    easingFunction: 'easeInOutCubic'
                }
            };
            self.network.fit(options);
            self.highlightNode(params);
            
            self.buildPropTable(self.nodes_vs.get(params.nodes[0]));
        };
        this.network.on('selectNode', this.selectNode);
        this.network.on('deselectNode', function(params) {
            self.highlightNode({}); // de-select all 
            self.buildPropTable();
            document.graphFilter.reset();
        });
        
        let doingZoom = false; // don't flood us with zoom triggers 
        this.network.on('zoom', function(params) {
            if (doingZoom) return;
            doingZoom = true;
            if (self.isBigGraph()) {
                self.fogOfWar();
            }
            setTimeout(function(){ doingZoom = false; }, 500);
        });
    };
}
/**/
