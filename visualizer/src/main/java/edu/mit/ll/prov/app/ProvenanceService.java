package edu.mit.ll.prov.app;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.*;
import javax.xml.namespace.QName;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.*;
import java.lang.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.mit.ll.prov.curator.db.*;
import edu.mit.ll.prov.curator.model.*;
import edu.mit.ll.prov.curator.serialize.*;


@Path("/")
public class ProvenanceService {
    
    private final static Logger logger = LoggerFactory.getLogger(ProvenanceService.class);
    
    public ProvenanceService() {
    }
    
    
    // Allow general sanitization of user input to the RESTful interface 
    public enum SanitizeType {
        GENERIC, UINT, INT, ALPHA, ALNUM, VERTEXID
    }
    public static String sanitizeProvInput(String input, SanitizeType inputType) {
        String returnStr = "";
        
        switch (inputType) {
            case VERTEXID:
            case GENERIC:
                returnStr = input.replaceAll("[^A-Za-z0-9:\\*_ -]", "");
                break;
            case UINT:
                returnStr = input.replaceAll("[^0-9]", "");
                break;
            case INT:
                returnStr = input.replaceAll("[^0-9-]", "");
                break;
            case ALPHA:
                returnStr = input.replaceAll("[^A-Za-z]", "");
                break;
            case ALNUM:
                returnStr = input.replaceAll("[^A-Za-z0-9]", "");
                break;
        }
        
        return returnStr;
    }
    
    
    @Path("/")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Map<String,List<String>> images() {
        try {
            return ProvenanceDatabase.getQuery().getCategories();
        } catch (QueryException e) {
            logger.error("Not found exception", e);
            throw new NotFoundException("Not found exception", e);
        }
    }
    
    
    /** 
    *   Clear out database data (preserving tables) 
    **/
    @Path("clearDatabase.json")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response clearItOut() 
    throws IOException, InterruptedException {
        try {
            ProvenanceDatabase.getQuery().clearDatabase();
        } catch (DatabaseException e) {
            logger.error("Error clearing out database!", e);
            throw new NotFoundException("Error clearing out database!", e);
        }
        
        return Response.ok("OK").build();
    }
    /* ** */ 
    
    
    // Helper function for provenance image functions
    public String provenanceImageHelper(String imageName, String imageCategory) throws IOException, InterruptedException {
        Graph graph = provenance(imageName, imageCategory);
        Serializer ser = new ProvJsonSerializer(true);
        return ser.serialize(graph);
    }
    
    /** 
    *   Return Provenance graph as SVG 
    **/
    @Path("{imagecategory}/{imagename}/provenance.svg")
    @GET
    @Produces("image/svg")
    public Response provenanceImageSvg(
                        @PathParam("imagename") String imageName,
                        @PathParam("imagecategory") String imageCategory
    ) throws IOException, InterruptedException {
    
    imageName = sanitizeProvInput(imageName, SanitizeType.GENERIC);
    imageCategory = sanitizeProvInput(imageCategory, SanitizeType.GENERIC);
    
    org.openprovenance.prov.model.ProvFactory provFactory =
        org.openprovenance.prov.interop.InteropFramework.newXMLProvFactory();
    org.openprovenance.prov.interop.InteropFramework interopFramework =
        new org.openprovenance.prov.interop.InteropFramework(provFactory);

	String json = provenanceImageHelper(imageName, imageCategory);
    org.openprovenance.prov.model.Document doc =
        interopFramework.readDocument(new java.io.ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8)),
                        org.openprovenance.prov.interop.InteropFramework.ProvFormat.JSON,
                        provFactory,
                        "");
        try {
            try (ByteArrayOutputStream stream = new ByteArrayOutputStream()) {
                interopFramework.writeDocument(stream,
					       org.openprovenance.prov.interop.InteropFramework.ProvFormat.SVG,
					       doc);
                byte[] imageData = stream.toByteArray();
                return Response.ok(imageData).build();
            }
        } catch (IOException e) {
            logger.error("Not found exception", e);
            throw new NotFoundException("Not found exception", e);
        }
    }
    /* ** */ 
    
    /** 
    *   Return Provenance graph as PNG image 
    **/
    @Path("{imagecategory}/{imagename}/provenance.png")
    @GET
    @Produces("image/png")
    public Response provenanceImagePng(
                        @PathParam("imagename") String imageName,
                        @PathParam("imagecategory") String imageCategory
    ) throws IOException, InterruptedException {
    
    imageName = sanitizeProvInput(imageName, SanitizeType.GENERIC);
    imageCategory = sanitizeProvInput(imageCategory, SanitizeType.GENERIC);
	
    org.openprovenance.prov.model.ProvFactory provFactory =
        org.openprovenance.prov.interop.InteropFramework.newXMLProvFactory();
    org.openprovenance.prov.interop.InteropFramework interopFramework =
        new org.openprovenance.prov.interop.InteropFramework(provFactory);

	String json = provenanceImageHelper(imageName, imageCategory);
    org.openprovenance.prov.model.Document doc =
        interopFramework.readDocument(new java.io.ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8)),
                        org.openprovenance.prov.interop.InteropFramework.ProvFormat.JSON,
                        provFactory,
                        "");
        try {
            try (ByteArrayOutputStream stream = new ByteArrayOutputStream()) {
                interopFramework.writeDocument(stream,
					       org.openprovenance.prov.interop.InteropFramework.ProvFormat.PNG,
					       doc);
                byte[] imageData = stream.toByteArray();
                return Response.ok(imageData).build();
            }
        } catch (IOException e) {
            logger.error("Not found exception", e);
            throw new NotFoundException("Not found exception", e);
        }
    }
    /* ** */
    
    
    /**
    *   methods below from ProvJsonSerializer 
    **/
    protected void addPrefix(QName id, Map<String, String> prefixes) {
        // Should we pull in default prefixes from ProvSerializer? 
        /*if (defaultPrefixes.containsKey(id.getNamespaceURI())) {
            return;
        }*/
        if (prefixes.containsKey(id.getNamespaceURI())) {
            return;
        }
        // ignore the prefix in the id for safety
        prefixes.put(id.getNamespaceURI(), String.format("ns%d", prefixes.size() + 1));
    }
    
    protected Map<String, String> addPrefixes(Graph graph) {
        Map<String, String> prefixes = new HashMap<>();
        
        for (Vertex vertex : graph.vertices.values()) {
            addPrefix(vertex.getId(), prefixes);
            for (QName id : vertex.attrs.keySet()) {
                addPrefix(id, prefixes);
            }
        }
        for (Edge edge : graph.edges.values()) {
            addPrefix(edge.getId(), prefixes);
            for (QName id : edge.attrs.keySet()) {
                addPrefix(id, prefixes);
            }
        }
        
        return prefixes;
    }
    
    protected String prefixedName(QName name, Map<String, String> prefixes) {
        return String.format("%s:%s", prefixes.get(name.getNamespaceURI()), name.getLocalPart());
    }
    /* ** */ 
    
    
    /** 
    *   Get GraphViz-based positional data for a Graph's nodes 
    **/
    protected String provenancePositionsJson(Graph graph) 
        throws IOException, InterruptedException {
        
        // Helper for getting graph node positions (in JSON format) 
        
        Map<String, String> prefixes = addPrefixes(graph);
        
        // convert Graph into DOT format 
        StringBuilder dotFile = new StringBuilder();
        dotFile.append("digraph G {\nrankdir=RL\nsplines=line\n");
        for (Edge edge: graph.edges.values()) {
            QName source = edge.getSource();
            QName destination = edge.getDestination();
            dotFile.append('"');
            dotFile.append(prefixedName(source, prefixes));
            dotFile.append("\" -> \"");
            dotFile.append(prefixedName(destination, prefixes));
            dotFile.append("\"\n");
        }
        dotFile.append("}\n");
        
        // Feed DOT file into GraphViz
        // Convert GraphViz plain text to positional JSON 
        StringBuilder dotOut = new StringBuilder();
        StringBuilder jsonOut = new StringBuilder();
        dotOut.append("\n\n");
        jsonOut.append("{\n");
        try {
            ProcessBuilder pb = new ProcessBuilder("dot", "-Tplain");
            pb.redirectErrorStream(true);
            Process proc = pb.start();
            
            // Send DOT file to dot 
            Writer procInput = new OutputStreamWriter(proc.getOutputStream());
            procInput.write(dotFile.toString());
            procInput.flush();
            procInput.close();
            
            // Read in dot's response 
            BufferedReader procOutput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            String line = null;
            boolean firstEntry = true;
            while((line=procOutput.readLine()) != null) {
                dotOut.append(line);
                dotOut.append('\n');
                
                // We only want to know position of nodes for JSON 
                if (line.startsWith("node ")) {
                    String[] parts = line.split(" ");
                    if (parts.length > 4) {
                        // Need comma separators after all entities (skip first) 
                        if (!firstEntry) {
                            jsonOut.append(",\n");
                        }
                        
                        jsonOut.append("    ");
                        jsonOut.append(parts[1]);
                        jsonOut.append(":{\n        \"x\":");
                        jsonOut.append(parts[2]);
                        jsonOut.append(",\n        \"y\":");
                        jsonOut.append(parts[3]);
                        jsonOut.append("\n    }");
                        
                        firstEntry = false;
                    }
                }
                
            }
            
            procOutput.close();
            int exitVal = proc.waitFor();
            if (exitVal != 0) {
                //ResponseBuilder builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
                return "GraphViz exited with error code " + exitVal + "\n";
            }
        } catch(IOException e) {
            throw e;
        } catch(InterruptedException e) {
            throw e;
        }
        dotOut.append("\n\n");
        jsonOut.append("\n}\n");
        
        //dotFile.toString() + dotOut.toString()
        return jsonOut.toString();
    }
    
    @Path("{imagecategory}/{imagename}/provenance_pos_behalf.json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response provenanceImageBehalfJson(
        @PathParam("imagename") String imageName,
        @PathParam("imagecategory") String imageCategory
    ) 
    throws IOException, InterruptedException {
        
        imageName = sanitizeProvInput(imageName, SanitizeType.GENERIC);
        imageCategory = sanitizeProvInput(imageCategory, SanitizeType.GENERIC);
        
        try {
            Graph graph = provenanceBehalf(provenance(imageName, imageCategory));
            String jsonOut = provenancePositionsJson(graph);
            return Response.ok(jsonOut).build();
        } catch (IOException e) {
            throw e;
        } catch (InterruptedException e) {
            throw e;
        }
    }
    @Path("{vertexid}/provenance_pos_behalf.json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response provenanceImageBehalfJson(
        @PathParam("vertexid") String vertexId
    ) 
    throws IOException, InterruptedException {
        
        vertexId = sanitizeProvInput(vertexId, SanitizeType.VERTEXID);
        
        QName qn = new QName("http://www.ll.mit.edu/provenance/application", vertexId);
        try {
            Graph graph = provenanceBehalf(provenance(qn));
            String jsonOut = provenancePositionsJson(graph);
            return Response.ok(jsonOut).build();
        } catch (IOException e) {
            throw e;
        } catch (InterruptedException e) {
            throw e;
        }
    }
    
    @Path("{imagecategory}/{imagename}/provenance_pos_informed.json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response provenanceImageInformedJson(
        @PathParam("imagename") String imageName,
        @PathParam("imagecategory") String imageCategory
    ) 
    throws IOException, InterruptedException {
        
        imageName = sanitizeProvInput(imageName, SanitizeType.GENERIC);
        imageCategory = sanitizeProvInput(imageCategory, SanitizeType.GENERIC);
        
        try {
            Graph graph = provenanceGrouped(provenance(imageName, imageCategory), GraphType.ACTIVITY);
            String jsonOut = provenancePositionsJson(graph);
            return Response.ok(jsonOut).build();
        } catch (IOException e) {
            throw e;
        } catch (InterruptedException e) {
            throw e;
        }
    }
    @Path("{vertexid}/provenance_pos_informed.json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response provenanceImageInformedJson(
        @PathParam("vertexid") String vertexId
    ) 
    throws IOException, InterruptedException {
        
        vertexId = sanitizeProvInput(vertexId, SanitizeType.VERTEXID);
        
        QName qn = new QName("http://www.ll.mit.edu/provenance/application", vertexId);
        try {
            Graph graph = provenanceGrouped(provenance(qn), GraphType.ACTIVITY);
            String jsonOut = provenancePositionsJson(graph);
            return Response.ok(jsonOut).build();
        } catch (IOException e) {
            throw e;
        } catch (InterruptedException e) {
            throw e;
        }
    }
    
    @Path("{imagecategory}/{imagename}/provenance_pos_derived.json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response provenanceImageDerivedJson(
        @PathParam("imagename") String imageName,
        @PathParam("imagecategory") String imageCategory
    ) 
    throws IOException, InterruptedException {
        
        imageName = sanitizeProvInput(imageName, SanitizeType.GENERIC);
        imageCategory = sanitizeProvInput(imageCategory, SanitizeType.GENERIC);
        
        try {
            Graph graph = provenanceGrouped(provenance(imageName, imageCategory), GraphType.ENTITY);
            String jsonOut = provenancePositionsJson(graph);
            return Response.ok(jsonOut).build();
        } catch (IOException e) {
            throw e;
        } catch (InterruptedException e) {
            throw e;
        }
    }
    @Path("{vertexid}/provenance_pos_derived.json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response provenanceImageDerivedJson(
        @PathParam("vertexid") String vertexId
    ) 
    throws IOException, InterruptedException {
        
        vertexId = sanitizeProvInput(vertexId, SanitizeType.VERTEXID);
        
        QName qn = new QName("http://www.ll.mit.edu/provenance/application", vertexId);
        try {
            Graph graph = provenanceGrouped(provenance(qn), GraphType.ENTITY);
            String jsonOut = provenancePositionsJson(graph);
            return Response.ok(jsonOut).build();
        } catch (IOException e) {
            throw e;
        } catch (InterruptedException e) {
            throw e;
        }
    }
    
    @Path("{imagecategory}/{imagename}/provenance_pos.json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response provenanceImageJson(
        @PathParam("imagename") String imageName,
        @PathParam("imagecategory") String imageCategory
    ) 
    throws IOException, InterruptedException {
        
        imageName = sanitizeProvInput(imageName, SanitizeType.GENERIC);
        imageCategory = sanitizeProvInput(imageCategory, SanitizeType.GENERIC);
        
        try {
            Graph graph = null;
            
            // If one of params is variable, give them entire AGENT graph (HUGE!) 
            if ("*".equals(imageName) || "*".equals(imageCategory)) {
                try {
                    Query query = ProvenanceDatabase.getQuery();
                    graph = provenanceBehalf(query.graph());
                } catch (QueryException e) {
                    logger.error("Error building full agent view", e);
                    throw new InternalServerErrorException(e);
                }
            }
            else {
                graph = provenance(imageName, imageCategory);
            }
            
            String jsonOut = provenancePositionsJson(graph);
            return Response.ok(jsonOut).build();
        } catch (IOException e) {
            throw e;
        } catch (InterruptedException e) {
            throw e;
        }
    }
    @Path("{vertexid}/provenance_pos.json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response provenanceImageJson(
        @PathParam("vertexid") String vertexId
    ) 
    throws IOException, InterruptedException {
        
        vertexId = sanitizeProvInput(vertexId, SanitizeType.VERTEXID);
        
        try {
            Graph graph = provenance(new QName("http://www.ll.mit.edu/provenance/application", vertexId));
            String jsonOut = provenancePositionsJson(graph);
            return Response.ok(jsonOut).build();
        } catch (IOException e) {
            throw e;
        } catch (InterruptedException e) {
            throw e;
        }
    }
    /* ** */ 
    
    
    /** 
    *   Allow simple sub-graph searching for attributes (as JSON array results) 
    **/
    @Path("{vertexid}/{filterType}/{filterValue}/provenance_search_s.json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response provenanceJson(
        @PathParam("vertexid") String vertexId,
        @PathParam("filterType") String filterType,
        @PathParam("filterValue") String filterValue
    ) {
        vertexId = sanitizeProvInput(vertexId, SanitizeType.VERTEXID);
        filterType = sanitizeProvInput(filterType, SanitizeType.GENERIC);
        filterValue = sanitizeProvInput(filterValue, SanitizeType.GENERIC);
        
        Graph graph = provenance(new QName("http://www.ll.mit.edu/provenance/application", vertexId));
        return provenanceJsonHelper(graph, filterType, filterValue);
    }
    @Path("{imagecategory}/{imagename}/{filterType}/{filterValue}/provenance_search_s.json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response provenanceJson(
        @PathParam("imagename") String imageName,
        @PathParam("imagecategory") String imageCategory,
        @PathParam("filterType") String filterType,
        @PathParam("filterValue") String filterValue
     ) {
        imageName = sanitizeProvInput(imageName, SanitizeType.GENERIC);
        imageCategory = sanitizeProvInput(imageCategory, SanitizeType.GENERIC);
        filterType = sanitizeProvInput(filterType, SanitizeType.GENERIC);
        filterValue = sanitizeProvInput(filterValue, SanitizeType.GENERIC);
        
        Graph graph = provenance(imageName, imageCategory);
        return provenanceJsonHelper(graph, filterType, filterValue);
     }
     
     public Response provenanceJsonHelper(Graph graph, String filterType, String filterValue) {
        Map<String, String> prefixes = addPrefixes(graph);
        
        List<String> vertIds = new ArrayList<>();
        try {
            // Build attributes filter 
            Map<QName, Object> attrs = new HashMap<>();
            String typeNamespace = "";
            String type = "";
            if (filterType.contains(":")) {
                String[] filterTypeAry = filterType.split(":", 2);
                typeNamespace = filterTypeAry[0];
                
                // translate typeNamespace prefix from shorthand to full URI 
                Set<Map.Entry<String, String>> prefixEntries = prefixes.entrySet();
                for (Map.Entry<String, String> prefix : prefixEntries) {
                    if (prefix.getValue().equals(typeNamespace)) {
                        typeNamespace = prefix.getKey();
                        break;
                    }
                }
                
                type = filterTypeAry[1];
            }
            attrs.put(new QName(typeNamespace, type), filterValue);
            
            // Find vertices that match attributes 
            List<Vertex> verts = graph.findVertices(attrs);
            for (Vertex vert : verts) {
                vertIds.add("\"" + prefixedName(vert.getId(), prefixes) + "\"");
            }
        } catch (Exception e) {
            logger.error("Not found exception", e);
            throw new NotFoundException("Not found exception", e);
        }
        String response = "[" + String.join(", ", vertIds) + "]";
        
        return Response.ok(response).build();
    }
    /* ** */ 
    
    
    /** 
    *   Return Provenance as JSON data with custom view 
    **/
    @Path("{imagecategory}/{imagename}/provenance_behalf.json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String provenanceJsonBehalf(
                    @PathParam("imagename") String imageName,
                    @PathParam("imagecategory") String imageCategory
    ) throws IOException, InterruptedException {
        imageName = sanitizeProvInput(imageName, SanitizeType.GENERIC);
        imageCategory = sanitizeProvInput(imageCategory, SanitizeType.GENERIC);
        
        Graph graph = provenanceBehalf(provenance(imageName, imageCategory));
        String posStr = provenancePositionsJson(graph);
        Serializer ser = new ProvJsonSerializer(true);
        return "{ \n\"prov\": " + ser.serialize(graph) + ",\n\"prov_pos\":" + posStr + "\n}";
    }
    @Path("{vertexid}/provenance_behalf.json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String provenanceJsonBehalf(
                    @PathParam("vertexid") String vertexId
    ) throws IOException, InterruptedException {
        vertexId = sanitizeProvInput(vertexId, SanitizeType.VERTEXID);
        
        Graph graph;
        QName qn = new QName("http://www.ll.mit.edu/provenance/application", vertexId);
        graph = provenanceBehalf(provenance(qn));
        String posStr = provenancePositionsJson(graph);
        Serializer ser = new ProvJsonSerializer(true);
        return "{ \n\"prov\": " + ser.serialize(graph) + ",\n\"prov_pos\":" + posStr + "\n}";
    }
    
    @Path("{imagecategory}/{imagename}/provenance_informed.json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String provenanceJsonInformed(
                    @PathParam("imagename") String imageName,
                    @PathParam("imagecategory") String imageCategory
    ) throws IOException, InterruptedException {
        imageName = sanitizeProvInput(imageName, SanitizeType.GENERIC);
        imageCategory = sanitizeProvInput(imageCategory, SanitizeType.GENERIC);
        
        Graph graph = provenanceGrouped(provenance(imageName, imageCategory), GraphType.ACTIVITY);
        String posStr = provenancePositionsJson(graph);
        Serializer ser = new ProvJsonSerializer(true);
        return "{ \n\"prov\": " + ser.serialize(graph) + ",\n\"prov_pos\":" + posStr + "\n}";
    }
    @Path("{vertexid}/provenance_informed.json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String provenanceJsonInformed(
                        @PathParam("vertexid") String vertexId
    ) throws IOException, InterruptedException {
        vertexId = sanitizeProvInput(vertexId, SanitizeType.VERTEXID);
        
        Graph graph;
        QName qn = new QName("http://www.ll.mit.edu/provenance/application", vertexId);
        graph = provenanceGrouped(provenance(qn), GraphType.ACTIVITY);
        String posStr = provenancePositionsJson(graph);
        Serializer ser = new ProvJsonSerializer(true);
        return "{ \n\"prov\": " + ser.serialize(graph) + ",\n\"prov_pos\":" + posStr + "\n}";
    }
    
    @Path("{imagecategory}/{imagename}/provenance_derived.json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String provenanceJsonDerived(
                    @PathParam("imagename") String imageName,
                    @PathParam("imagecategory") String imageCategory
    ) throws IOException, InterruptedException {
        imageName = sanitizeProvInput(imageName, SanitizeType.GENERIC);
        imageCategory = sanitizeProvInput(imageCategory, SanitizeType.GENERIC);
        
        Graph graph = provenanceGrouped(provenance(imageName, imageCategory), GraphType.ENTITY);
        String posStr = provenancePositionsJson(graph);
        Serializer ser = new ProvJsonSerializer(true);
        return "{ \n\"prov\": " + ser.serialize(graph) + ",\n\"prov_pos\":" + posStr + "\n}";
    }
    @Path("{vertexid}/provenance_derived.json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String provenanceJsonDerived(
                        @PathParam("vertexid") String vertexId
    ) throws IOException, InterruptedException {
        vertexId = sanitizeProvInput(vertexId, SanitizeType.VERTEXID);
        
        Graph graph;
        QName qn = new QName("http://www.ll.mit.edu/provenance/application", vertexId);
        graph = provenanceGrouped(provenance(qn), GraphType.ENTITY);
        String posStr = provenancePositionsJson(graph);
        Serializer ser = new ProvJsonSerializer(true);
        return "{ \n\"prov\": " + ser.serialize(graph) + ",\n\"prov_pos\":" + posStr + "\n}";
    }
    /* ** */ 
    
    
    /** 
    *   Return Provenance graph as JSON data (generic, with filtering options) 
    **/
    @Path("type/{nodetype}/name/{attrname}/value/{attrvalue}/off/{offset}/provenance_search.json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String provenanceJsonF5(@PathParam("nodetype") String nodeType,
				 @PathParam("attrname") String attrName,
				 @PathParam("attrvalue") String attrValue,
				 @PathParam("offset") String offset) {
        
        nodeType = sanitizeProvInput(nodeType, SanitizeType.ALPHA);
        attrName = sanitizeProvInput(attrName, SanitizeType.GENERIC);
        attrValue = sanitizeProvInput(attrValue, SanitizeType.GENERIC);
        offset = sanitizeProvInput(offset, SanitizeType.UINT);
        
        Graph graph = provenanceFiltered(nodeType, attrName, attrValue, offset);
        Serializer ser = new ProvJsonSerializer(true);
        return ser.serialize(graph);
    }
    
    @Path("name/{attrname}/value/{attrvalue}/off/{offset}/provenance_search.json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String provenanceJsonF4(@PathParam("attrname") String attrName,
				 @PathParam("attrvalue") String attrValue,
				 @PathParam("offset") String offset) {
        attrName = sanitizeProvInput(attrName, SanitizeType.GENERIC);
        attrValue = sanitizeProvInput(attrValue, SanitizeType.GENERIC);
        offset = sanitizeProvInput(offset, SanitizeType.UINT);
        
        Graph graph = provenanceFiltered(null, attrName, attrValue, offset);
        Serializer ser = new ProvJsonSerializer(true);
        return ser.serialize(graph);
    }
    
    @Path("type/{nodetype}/off/{offset}/provenance_search.json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String provenanceJsonF3(
                 @PathParam("nodetype") String nodeType,
                 @PathParam("offset") String offset) {
        nodeType = sanitizeProvInput(nodeType, SanitizeType.ALPHA);
        offset = sanitizeProvInput(offset, SanitizeType.UINT);
        
        Graph graph = provenanceFiltered(nodeType, null, null, offset);
        Serializer ser = new ProvJsonSerializer(true);
        return ser.serialize(graph);
    }
    /* ** */ 
    
    
    /** 
    *   Return Provenance graph as JSON data 
    **/
    @Path("{imagecategory}/{imagename}/provenance.json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String provenanceJson(
                    @PathParam("imagename") String imageName,
                    @PathParam("imagecategory") String imageCategory
    ) throws IOException, InterruptedException {
        imageName = sanitizeProvInput(imageName, SanitizeType.GENERIC);
        imageCategory = sanitizeProvInput(imageCategory, SanitizeType.GENERIC);
        
        Graph graph = provenance(imageName, imageCategory);
        String posStr = "{}";
        if (!"*".equals(imageName) && !"*".equals(imageCategory)) {
            posStr = provenancePositionsJson(graph);
        }
        Serializer ser = new ProvJsonSerializer(true);
        return "{ \n\"prov\": " + ser.serialize(graph) + ",\n\"prov_pos\":" + posStr + "\n}";
    }
    
    @Path("{vertexid}/provenance.json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String provenanceJson(
                    @PathParam("vertexid") String vertexId
    ) throws IOException, InterruptedException {
        vertexId = sanitizeProvInput(vertexId, SanitizeType.VERTEXID);
        
        Graph graph = provenance(new QName("http://www.ll.mit.edu/provenance/application", vertexId));
        String posStr = provenancePositionsJson(graph);
        Serializer ser = new ProvJsonSerializer(true);
        return "{ \n\"prov\": " + ser.serialize(graph) + ",\n\"prov_pos\":" + posStr + "\n}";
    }
    /* ** */ 
    
    
    /*
    @Path("{imagecategory}/{imagename}/provenance.provn")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String provenanceProvn(@PathParam("imagename") String imageName,
				  @PathParam("imagecategory") String imageCategory) {
        Document doc = provenance(imageName, imageCategory);
        OutputStream stream = new ByteArrayOutputStream();
        interopFramework.writeDocument(stream, InteropFramework.ProvFormat.PROVN, doc);
        return stream.toString();
    }
    */
    
    
    /** 
    *   Return raw Provenance Graph with custom view 
    **/
    protected boolean isEntity(Vertex v) {
        return (v instanceof Entity);
    }
    protected boolean isActivity(Vertex v) {
        return (v instanceof Activity);
    }
    protected boolean isAgent(Vertex v) {
        return (v instanceof Agent);
    }
    protected enum GraphType {
        AGENT, ACTIVITY, ENTITY
    }
    protected Graph provenanceBehalf(Graph graph) {
        HashMap<String, QName> edgeLookup = new HashMap<String, QName>();
        String xs = "http://www.ll.mit.edu/provenance/application";
        String xs2 = "http://www.ll.mit.edu/provenance/application#";
        
        // Build new graph with only agents 
        Graph g = new Graph();
        ArrayDeque<QName> queue = new ArrayDeque<QName>();
        for (Vertex startVertex : graph.vertices.values()) {
            if (startVertex == null || !isAgent(startVertex)) {
                continue;
            }
            
            // If AppRole attr defined, it must be a producer at this point 
            QName vertexAttrName = new QName(xs2, "AppRole");
            if (startVertex.containsAttribute(vertexAttrName)) {
                Object appRoleObj = startVertex.getAttributeValue(vertexAttrName);
                if (appRoleObj != null) {
                    String appRole = (String)appRoleObj;
                    if (!"producer".equals(appRole)) {
                        continue;
                    }
                }
            }
            
            // Add agent to new graph 
            g.add(startVertex);
            
            // Add all activities assoc. with this agent 
            queue.clear();
            if (startVertex.getId() != null && graph.backward.containsKey(startVertex.getId())) {
                for (QName edgeId : graph.backward.get(startVertex.getId())) {
                    if (edgeId == null) {
                        continue;
                    }
                    Edge edge = graph.edges.get(edgeId);
                    if (edge == null || edge.getSource() == null) {
                        continue;
                    }
                    Vertex src = graph.vertices.get(edge.getSource());
                    if (src == null) {
                        continue;
                    }
                    
                    // Handle on-behalf-of links between agents 
                    if (isAgent(src)) {
                        if (!startVertex.getId().equals(src.getId())) {
                            g.add(edge);
                        }
                    }
                    else {
                        //if (isActivity(src)) {
                            queue.add(src.getId());
                        //}
                    }
                }
            }
            
            // BFS upwards looking for Activities 
            while (queue.size() > 0) {
                Vertex this_v = graph.vertices.get(queue.remove());
                if ((this_v == null) || !graph.forward.containsKey(this_v.getId())) {
                    continue;
                }
                
                // Crawl up graph 
                QName this_vID = this_v.getId();
                if (this_vID == null) {
                    continue;
                }
                for (QName edgeId : graph.forward.get(this_vID)) {
                    if (edgeId == null) {
                        continue;
                    }
                    Edge edge = graph.edges.get(edgeId);
                    Vertex dest = graph.vertices.get(edge.getDestination());
                    if (dest == null) {
                        continue;
                    }
                    
                    // Follow non-Activities 
                    if (isActivity(dest)) {
                        // Stop at the next Activity 
                        QName destID = dest.getId();
                        if (destID == null || graph.forward.get(destID) == null) {
                            continue;
                        }
                        for (QName stepAheadEdgeID : graph.forward.get(destID)) {
                            if (stepAheadEdgeID == null) {
                                continue;
                            }
                            Edge stepAheadEdge = graph.edges.get(stepAheadEdgeID);
                            if (stepAheadEdge == null || stepAheadEdge.getDestination() == null) {
                                continue;
                            }
                            Vertex stepAheadDest = graph.vertices.get(stepAheadEdge.getDestination());
                            if (stepAheadDest == null) {
                                continue;
                            }
                            
                            // Assume each Activity has (at least one) Agent attached! 
                            if (!isAgent(stepAheadDest)) {
                                continue;
                            }
                            
                            // Skip if same agent as starting agent  
                            if (startVertex.getId().equals(stepAheadDest.getId())) {
                                continue;
                            }
                            
                            // Add destination (Agent) to graph 
                            g.add(stepAheadDest);
                            
                            // Create new edge between agents 
                            QName attrName = new QName(xs, "weight");
                            String lookupID = "" + startVertex.getId() + "->" + stepAheadDest.getId();
                            if (!edgeLookup.containsKey(lookupID)) {
                                QName name = new QName(xs, String.valueOf(lookupID.hashCode()));
                                Edge newEdge = new ActedOnBehalfOf(name, startVertex.getId(), stepAheadDest.getId());
                                
                                newEdge.setAttribute(attrName, Integer.valueOf(1));
                                
                                g.add(newEdge);
                                edgeLookup.put(lookupID, name);
                            }
                            else {
                                // Increment attribute weight 
                                Edge this_edge = g.edges.get(edgeLookup.get(lookupID));
                                if (this_edge != null) {
                                    Object wtObj = this_edge.getAttributeValue(attrName);
                                    if (wtObj != null) {
                                        Integer weight = ((Integer)wtObj).intValue()+1;
                                        this_edge.setAttribute(attrName, weight);
                                    }
                                    else {
                                        // Something went wrong! 
                                        QName name = new QName(xs, String.valueOf(lookupID.hashCode()));
                                        this_edge.setAttribute(name, Integer.valueOf(-1));
                                    }
                                }
                            }
                        }
                    }
                    else if (isAgent(dest)) {
                        // Skip if same agent as starting agent  
                        if (startVertex.getId().equals(dest.getId())) {
                            continue;
                        }
                        
                        // If AppRole attr defined, it must be a consumer at this point 
                        QName destAttrName = new QName(xs2, "AppRole");
                        if (dest.containsAttribute(destAttrName)) {
                            Object appRoleObj = dest.getAttributeValue(destAttrName);
                            if (appRoleObj != null) {
                                String appRole = (String)appRoleObj;
                                if (!"consumer".equals(appRole)) {
                                    continue;
                                }
                            }
                        }
                        
                        // Add destination (Agent) to graph 
                        g.add(dest);
                        
                        // Create new edge between agents 
                        QName attrName = new QName(xs, "weight");
                        String lookupID = "" + startVertex.getId() + "->" + dest.getId();
                        if (!edgeLookup.containsKey(lookupID)) {
                            QName name = new QName(xs, String.valueOf(lookupID.hashCode()));
                            Edge newEdge = new ActedOnBehalfOf(name, startVertex.getId(), dest.getId());
                            
                            newEdge.setAttribute(attrName, Integer.valueOf(1));
                            
                            g.add(newEdge);
                            edgeLookup.put(lookupID, name);
                        }
                        else {
                            // Increment attribute weight 
                            Edge this_edge = g.edges.get(edgeLookup.get(lookupID));
                            if (this_edge != null) {
                                Object wtObj = this_edge.getAttributeValue(attrName);
                                if (wtObj != null) {
                                    Integer weight = ((Integer)wtObj).intValue()+1;
                                    this_edge.setAttribute(attrName, weight);
                                }
                                else {
                                    // Something went wrong! 
                                    QName name = new QName(xs, String.valueOf(lookupID.hashCode()));
                                    this_edge.setAttribute(name, Integer.valueOf(-1));
                                }
                            }
                        }
                    }
                    else {
                        queue.add(dest.getId());
                        continue;
                    }
                    
                }
            }
        }
        
        return g;
    }
    
    protected Graph provenanceGrouped(Graph graph, GraphType type) {
        HashMap<String, QName> edgeLookup = new HashMap<String, QName>();
        String xs = "http://www.ll.mit.edu/provenance/application";
        
        // Build new graph with only GraphType type 
        Graph g = new Graph();
        ArrayDeque<QName> queue = new ArrayDeque<QName>();
        for (Vertex startVertex : graph.vertices.values()) {
            if (startVertex == null) {
                continue;
            }
            // Skip non-type GraphTypes 
            if (
                (type == GraphType.ENTITY) && !isEntity(startVertex) 
                || (type == GraphType.ACTIVITY) && !isActivity(startVertex)
            ) {
                continue;
            }
            
            // Add vertex to new graph 
            g.add(startVertex);
            
            // Add vertex to search space 
            queue.clear();
            queue.add(startVertex.getId());
            
            // BFS upwards looking for GraphType types 
            while (queue.size() > 0) {
                Vertex this_v = graph.vertices.get(queue.remove());
                if ((this_v == null) || !graph.forward.containsKey(this_v.getId())) {
                    continue;
                }
                
                // Crawl up graph 
                for (QName edgeId : graph.forward.get(this_v.getId())) {
                    if (edgeId == null) {
                        continue;
                    }
                    Edge edge = graph.edges.get(edgeId);
                    if (edge == null || edge.getDestination() == null) {
                        continue;
                    }
                    Vertex dest = graph.vertices.get(edge.getDestination());
                    if (dest == null) {
                        continue;
                    }
                    
                    // Follow non-matching types 
                    if (
                        (type == GraphType.ENTITY) && !isEntity(dest) 
                        || (type == GraphType.ACTIVITY) && !isActivity(dest)
                    ) {
                        queue.add(dest.getId());
                        continue;
                    }
                    // Stop at the next GraphType type 
                    
                    // Skip if same as starting vertex 
                    if (startVertex.getId().equals(dest.getId())) {
                        continue;
                    }
                    
                    // Add destination to graph 
                    g.add(dest);
                    
                    // Create new edge between vertices 
                    QName attrName = new QName(xs, "weight");
                    String lookupID = "" + startVertex.getId() + "->" + dest.getId();
                    if (!edgeLookup.containsKey(lookupID)) {
                        QName name = new QName(xs, String.valueOf(lookupID.hashCode()));
                        Edge newEdge = null;
                        if ((type == GraphType.ENTITY) && isEntity(dest)) {
                            newEdge = new WasDerivedFrom(name, startVertex.getId(), dest.getId());
                        }
                        else if ((type == GraphType.ACTIVITY) && isActivity(dest)) {
                            newEdge = new WasInformedBy(name, startVertex.getId(), dest.getId());
                        }
                        else {
                            // Mismatch somehow!
                            continue;
                        }
                        
                        newEdge.setAttribute(attrName, Integer.valueOf(1));
                        
                        g.add(newEdge);
                        edgeLookup.put(lookupID, name);
                    }
                    else {
                        // Increment attribute weight 
                        Edge this_edge = g.edges.get(edgeLookup.get(lookupID));
                        if (this_edge != null) {
                            Object wtObj = this_edge.getAttributeValue(attrName);
                            if (wtObj != null) {
                                Integer weight = ((Integer)wtObj).intValue()+1;
                                this_edge.setAttribute(attrName, weight);
                            }
                            else {
                                // Something went wrong! 
                                QName name = new QName(xs, String.valueOf(lookupID.hashCode()));
                                this_edge.setAttribute(name, Integer.valueOf(-1));
                            }
                        }
                    }
                }
            }
        }
        
        return g;
    }
    /* ** */ 
    
    
    /** 
    *   Return raw Provenance Graph from database (with filtering options) 
    **/
    protected Graph provenanceFiltered(String nodeType, String attrName, String attrValue, String offset) {
        Graph graph = new Graph();
        
        // Ignored search terms are empty 
        if (attrName == null || " ".equals(attrName)) {
            attrName = "";
        }
        if (attrValue == null || " ".equals(attrValue)) {
            attrValue = "";
        }
        
        // Attribute name String -> QName 
        //TODO: allow specifying namespace
        QName qnAttrName = new QName("http://www.ll.mit.edu/provenance/application", attrName);
        
        // validate name and category
        try {
            Query query = ProvenanceDatabase.getQuery();
            
            try {
                Map<QName, Object> attrs = new HashMap<>();
                // Include user-defined attribute filter (if something provided) 
                if (!("".equals(attrName) && "".equals(attrValue))) {
                    attrs.put(qnAttrName, attrValue);
                }
                
                // handle pagination 
                Integer offInt = null;
                if (!" ".equals(offset)) {
                    offInt = Integer.valueOf(offset);
                }
                
                // match name or value by default 
                List<Vertex> verts = query.findVertices(attrs, nodeType, true, offInt);
                for (Vertex vert : verts) {
                    graph.add(vert);
                }
            } catch (QueryException e) {
                logger.error("Not found exception", e);
                throw new NotFoundException("Not found exception", e);
            }
            //System.out.println(graph);
            return graph;
        } catch (QueryException e) {
            logger.error("Server error while building filtered prov graph", e);
            throw new InternalServerErrorException(e);
        }
    }
    
    protected Graph provenance(QName vertexId) {
        try {
            Query query = ProvenanceDatabase.getQuery();
            Graph graph = query.subgraph(vertexId, 4); // override hops to 4! 
            //System.out.println(graph);
            return graph;
        } catch (QueryException e) {
            logger.error("Server error while building prov graph", e);
            throw new InternalServerErrorException(e);
        }
    }
    
    protected Graph provenance(String imageName, String imageCategory) {
        String xs = "http://www.ll.mit.edu/provenance/application";
        
        // validate name and category
        try {
            Query query = ProvenanceDatabase.getQuery();
            
            // If one of params is variable, give them entire AGENT graph (HUGE!) 
            if ("*".equals(imageName) || "*".equals(imageCategory)) {
                Graph tmpGraph = query.graph();
                if (tmpGraph == null) {
                    logger.error("Failed to get graph from Query interface.");
                    return tmpGraph;
                }
                logger.debug("Loaded full graph -- total edges: {}", tmpGraph.edges.size());
                Graph graph = provenanceBehalf(tmpGraph);
                
                try {
                    // Gather avg edge weight metric 
                    int avgEdgeWtNum = 0;
                    int avgEdgeWtDenom = 0;
                    QName attrName = new QName(xs, "weight");
                    for (Edge edge: graph.edges.values()) {
                        Object wtObj = edge.getAttributeValue(attrName);
                        if (wtObj != null) {
                            avgEdgeWtNum += ((Integer)wtObj).intValue();
                            ++avgEdgeWtDenom;
                        }
                    }
                    if (avgEdgeWtDenom == 0) {
                        // Non-zero denominator 
                        avgEdgeWtDenom = 1;
                    }
                    
                    // Prepare to output metrics 
                    java.nio.file.Path tmpPath = Paths.get("/tmp");
                    
                    // Print metrics
                    java.nio.file.Path tmpLogMetricFile = Files.createTempFile(tmpPath, "cmcc_provenance_stats-",".txt");
                    List<String> logData = Arrays.asList("Vertics: "+graph.vertices.size(), "\nEdges: "+graph.edges.size(), "\nAvg. Edge. Wt: "+(avgEdgeWtNum/avgEdgeWtDenom));
                    Files.write(tmpLogMetricFile, logData);
                    
                    // Print sanitized graph
                    java.nio.file.Path tmpLogGraphFile = Files.createTempFile(tmpPath, "cmcc_provenance_graph-",".txt");
                    List<String> logGraph = Arrays.asList(graph.toString(true));
                    Files.write(tmpLogGraphFile, logGraph);
                } catch (IOException | SecurityException e) {
                    logger.error("IO-related exception", e);
                    throw new InternalServerErrorException(e);
                }
                
                return graph;
            }
            
            // Otherwise continue with filtered graph 
            List<QName> vertIds = new ArrayList<>();
            try {
                Map<QName, Object> attrs = new HashMap<>();
                attrs.put(new QName("http://www.ll.mit.edu/provenance/application", "name"), imageName);
                attrs.put(new QName("http://www.ll.mit.edu/provenance/application", "category"), imageCategory);
                
                List<Vertex> verts = query.findVertices(attrs);
                for (Vertex vert : verts) {
                    vertIds.add(vert.getId());
                }
            } catch (QueryException e) {
                logger.error("Not found exception", e);
                throw new NotFoundException("Not found exception", e);
            }
            Graph graph = query.connected(vertIds);
            //System.out.println(graph);
            return graph;
        } catch (QueryException e) {
            logger.error("Server error while building prov graph", e);
            throw new InternalServerErrorException(e);
        }
    }
    /* ** */ 
    
}
