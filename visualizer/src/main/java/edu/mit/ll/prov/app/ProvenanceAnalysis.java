package edu.mit.ll.prov.app;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.*;
import javax.xml.namespace.QName;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

import edu.mit.ll.prov.curator.db.*;
import edu.mit.ll.prov.curator.model.*;

@Path("/")
public class ProvenanceAnalysis {

    public ProvenanceAnalysis() {
    }

    @Path("{imagecategory}/{imagename}/analysis.json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String provenanceJson(@PathParam("imagename") String imageName,
				 @PathParam("imagecategory") String imageCategory) {
        //Graph graph = provenance(imageName, imageCategory);
        
        // analyze the provenance document
        String status = "Unknown";
        String explain = "Provenance analysis not implemented yet";
        
        String pattern = "{\n" + 
            "\"status\": \"%status\",\n" + 
            "\"explain\": \"%explain\"\n" + 
            "}\n";
        return pattern.replace("%status",status).replace("%explain",explain);
    }

    protected Graph provenance(String imageName, String imageCategory) {
        // validate name and category
        try {
            Query query = ProvenanceDatabase.getQuery();
            List<QName> vertIds = new ArrayList<>();
            try {
                Map<QName, Object> attrs = new HashMap<>();
                attrs.put(new QName("http://www.ll.mit.edu/provenance/application", "name"), imageName);
                attrs.put(new QName("http://www.ll.mit.edu/provenance/application", "category"), imageCategory);
                List<Vertex> verts = query.findVertices(attrs);
                for (Vertex vert : verts) {
                    vertIds.add(vert.getId());
                }
            } catch (QueryException e) {
                //e.printStackTrace();
                throw new NotFoundException("Not found exception", e);
            }
            Graph graph = query.connected(vertIds);
            //System.out.println(graph);
            return graph;
        } catch (QueryException e) {
            //e.printStackTrace();
            throw new InternalServerErrorException(e);
        }
    }

}
