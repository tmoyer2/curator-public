#!/bin/bash

DIR=$HOME/Curator/visualizer

export CATALINA_OPTS="$CATALINA_OPTS -Dcurator.config.file=$DIR/config.yml -Xms2048m -Xmx2048m"
