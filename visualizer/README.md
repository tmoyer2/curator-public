
# Provenance Visualizer WebApp #

## Required Software ##

* Tomcat 7+
* GraphViz

## Building WAR File ##

Simply use Apache Maven to install Curator, and the Visualization suite will be built automatically.

````cmd
$ cd $CURATOR_DIR
$ mvn install
````

The resulting WAR file can then be found in the $CURATOR_DIR/visualizer/target directory 

## Set up Tomcat environment ##

1. Create a symbolic link to the visualizer/setenv.sh script.  For example: 

    ````cmd
    $ ln -s $CURATOR_DIR/visualizer/setenv.sh /usr/share/tomcat7/bin/setenv.sh
    ````

    * This script sets the CATALINA_OPTS environment variable so the visualizer/config.yml file can be found by Tomcat 
        * More information can be found here: https://docs.oracle.com/cd/E40520_01/integrator.311/integrator_install/src/cli_ldi_server_config.html
    * **_Be sure to customize the DIR variable in this script to point to where you put this config file:_**
        
        ````bash
        DIR=$HOME/prov-app
        ````
        
    * Since tomcat will not be running under your user account (it has its own account), it is recommended to replace $HOME with an absolute path!
2. Update the visualizer/config.yml file to include your MySQL installation details.  Update these lines in particular: 

    ````yaml
    store:
      sql:
        driver: com.mysql.jdbc.Driver
        url: "jdbc:mysql://localhost:3306/provenance"
        username: prov
        password: 5P@d1N6
    ````
## Add WebApp files ##

1. Drop the generated WAR file into your appBase directory so that it will be deployed by Tomcat, as specified here: https://tomcat.apache.org/tomcat-7.0-doc/deployer-howto.html
    * For example, on Ubuntu this is often located at '/var/lib/tomcat7/webapps/'

    ````cmd
    $ cp $CURATOR_DIR/visualizer/target/prov-app-webservice-2.0-SNAPSHOT.war /var/lib/tomcat7/webapps/prov-app.war
    ````
2. Tomcat should automatically unpack the WAR file and make the service available. Usually this process takes a few seconds, and you will get 'HTTP Status 404' errors in the steps below until it is available. 

    *Additional help deploying Tomcat WAR files can be found here: http://www.baeldung.com/tomcat-deploy-war*

## Test your WebApp

1. Test out the Provenance WebApp by visiting, for example: http://localhost:8080/prov-app/index.html
    * This URL should match how your Tomcat config is set up and which IP/port it listens on (this is typically localhost:8080) 
    * This page allows you to search your provenance data
        * A good starting point is to set **Type** to 'entity' to see all entities, then press 'Filter'. 
        * Click on one of the results to see its local provenance graph neighborhood. 
2. Test out the *__full__* Agent view by visiting, for example: http://localhost:8080/prov-app/provenance/*/*/index.html
    * This graph will take several minutes to generate, especially if the database has over 100k nodes.  
    * The graph will likely be very large with many nodes, so scrolling (with mouse wheel) will be necessary if you want to see details. 
    * This page will also log metric and graph data to the /tmp directory (with random filenames each time) 
        * e.g., __stat metrics__ - /tmp/cmcc_provenance_stats-_271991626467694870_.txt
        * e.g., __graph data__ - /tmp/cmcc_provenance_graph-_5418047862826368816_.txt

## Debugging

1. Server-side errors can be found in tomcat's catalina log file: e.g., ````/var/log/tomcat7/catalina.out````.  
2. Client-side errors (e.g., JavaScript) can be seen by pressing Ctrl+Shift+K in Firefox. 
