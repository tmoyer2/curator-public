package edu.mit.ll.prov.curator.serialize;

import java.io.StringWriter;

import javax.json.*;
import javax.json.stream.*;
import javax.xml.namespace.QName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.mit.ll.prov.curator.model.*;

public class ProvJsonSerializer extends ProvSerializer implements Serializer {

    private boolean pretty = false;

    private final static Logger logger = LoggerFactory.getLogger(ProvJsonSerializer.class);

    public ProvJsonSerializer() {
        super();
    }

    public ProvJsonSerializer(boolean pretty) {
        super();
        this.pretty = pretty;
    }

    public ProvJsonSerializer(Map<String, String> extraPrefixes) {
        super(extraPrefixes);
    }

    public String serialize(Graph graph) {
        addPrefixes(graph);
        JsonObjectBuilder docBuilder = createDocumentBuilder();
        addVertices(graph, docBuilder);
        addEdges(graph, docBuilder);

        if (pretty) {
            JsonObject obj = docBuilder.build();
            StringWriter stringWriter = new StringWriter();
            Map<String, Object> properties = new HashMap<>();
            properties.put(JsonGenerator.PRETTY_PRINTING, true);
            JsonWriterFactory factory = Json.createWriterFactory(properties);
            JsonWriter writer = factory.createWriter(stringWriter);
            writer.write(obj);
            writer.close();
            return stringWriter.toString();
        } else {
            return docBuilder.build().toString();
        }
    }

    protected void addPrefixes(Graph graph) {
        prefixes = new HashMap<>(); // clear the prefixes of any old ones
        for (Vertex vertex : graph.vertices.values()) {
            addPrefix(vertex.getId());
            for (QName id : vertex.attrs.keySet()) {
                addPrefix(id);
            }
        }
        for (Edge edge : graph.edges.values()) {
            addPrefix(edge.getId());
            for (QName id : edge.attrs.keySet()) {
                addPrefix(id);
            }
        }
    }

    protected void addPrefix(QName id) {
        if (defaultPrefixes.containsKey(id.getNamespaceURI())) {
            return;
        }
        if (prefixes.containsKey(id.getNamespaceURI())) {
            return;
        }
        // ignore the prefix in the id for safety
        prefixes.put(id.getNamespaceURI(), String.format("ns%d", prefixes.size() + 1));
    }
    
    protected JsonObjectBuilder createDocumentBuilder() {
        JsonObjectBuilder docBuilder = Json.createObjectBuilder();

        if (prefixes.size() > 0) {
            JsonObjectBuilder nsBuilder = Json.createObjectBuilder();
            Set<Map.Entry<String, String>> prefixEntries = prefixes.entrySet();
            for (Map.Entry<String, String> prefix : prefixEntries) {
                nsBuilder.add(prefix.getValue(), prefix.getKey());
            }
            docBuilder.add("prefix", nsBuilder.build());
        }

        return docBuilder;
    }

    protected void addVertices(Graph graph, JsonObjectBuilder docBuilder) {
        List<Activity> activities = new ArrayList<>();
        List<Agent> agents = new ArrayList<>();
        List<Entity> entities = new ArrayList<>();
        for (Vertex vertex : graph.vertices.values()) {
            if (vertex instanceof Activity) {
                activities.add((Activity)vertex);
            } else if (vertex instanceof Agent) {
                agents.add((Agent)vertex);
            } else if (vertex instanceof Entity) {
                entities.add((Entity)vertex);
            } else {
                // warning
            }
        }

        if (activities.size() > 0) {
            JsonObjectBuilder builder = Json.createObjectBuilder();
            for(Activity activity : activities) {
                builder.add(prefixedName(activity.getId()), toJson(activity));
            }
            docBuilder.add("activity", builder.build());
        }
        if (agents.size() > 0) {
            JsonObjectBuilder builder = Json.createObjectBuilder();
            for(Agent agent : agents) {
                builder.add(prefixedName(agent.getId()), toJson(agent));
            }
            docBuilder.add("agent", builder.build());
        }
        if (entities.size() > 0) {
            JsonObjectBuilder builder = Json.createObjectBuilder();
            for(Entity entity : entities) {
                builder.add(prefixedName(entity.getId()), toJson(entity));
            }
            docBuilder.add("entity", builder.build());
        }
    }
        
    protected void addEdges(Graph graph, JsonObjectBuilder docBuilder) {
        Map<String, List<Edge>> edges = new HashMap<>();
        for (Edge edge : graph.edges.values()) {
            if (!edges.containsKey(edge.getType())) {
                edges.put(edge.getType(), new ArrayList<Edge>());
            }
            edges.get(edge.getType()).add(edge);
        }
        Set<Map.Entry<String, List<Edge>>> edgeSet = edges.entrySet();
        for (Map.Entry<String, List<Edge>> edgeTuple : edgeSet) {
            JsonObjectBuilder builder = Json.createObjectBuilder();
            for(Edge edge : edgeTuple.getValue()) {
                builder.add(prefixedName(edge.getId()), toJson(edge));
            }
            docBuilder.add(edgeTuple.getKey(), builder.build());
        }
    }

    protected JsonObject toJson(Vertex vertex) {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        addAttributes(vertex, builder);
        return builder.build();
    }

    protected JsonObject toJson(Edge edge) {
        if (edge instanceof ActedOnBehalfOf) {
            return toJson((ActedOnBehalfOf)edge);
        } else if (edge instanceof Used) {
            return toJson((Used)edge);
        } else if (edge instanceof WasAssociatedWith) {
            return toJson((WasAssociatedWith)edge);
        } else if (edge instanceof WasAttributedTo) {
            return toJson((WasAttributedTo)edge);
        } else if (edge instanceof WasDerivedFrom) {
            return toJson((WasDerivedFrom)edge);
        } else if (edge instanceof WasInformedBy) {
            return toJson((WasInformedBy)edge);
        } else if (edge instanceof WasGeneratedBy) {
            return toJson((WasGeneratedBy)edge);
        } else {
            logger.warn(String.format("can't handle edge of type %s", edge.getClass().getName()));
            JsonObjectBuilder builder = Json.createObjectBuilder();
            return builder.build();
        }
    }

    protected JsonObject toJson(ActedOnBehalfOf behalf) {
        JsonObjectBuilder builder = Json.createObjectBuilder()
            .add("prov:delegate", prefixedName(behalf.getSource()))
            .add("prov:responsible", prefixedName(behalf.getDestination()));
        addAttributes(behalf, builder);
        return builder.build();
    }

    protected JsonObject toJson(Used used) {
        JsonObjectBuilder builder = Json.createObjectBuilder()
            .add("prov:activity", prefixedName(used.getSource()))
            .add("prov:entity", prefixedName(used.getDestination()));
        addAttributes(used, builder);
        return builder.build();
    }

    protected JsonObject toJson(WasAssociatedWith waw) {
        JsonObjectBuilder builder = Json.createObjectBuilder()
            .add("prov:activity", prefixedName(waw.getSource()))
            .add("prov:agent", prefixedName(waw.getDestination()));
        addAttributes(waw, builder);
        return builder.build();
    }

    protected JsonObject toJson(WasAttributedTo wat) {
        JsonObjectBuilder builder = Json.createObjectBuilder()
            .add("prov:entity", prefixedName(wat.getSource()))
            .add("prov:agent", prefixedName(wat.getDestination()));
        addAttributes(wat, builder);
        return builder.build();
    }
    
    protected JsonObject toJson(WasDerivedFrom wdf) {
        JsonObjectBuilder builder = Json.createObjectBuilder()
            .add("prov:generatedEntity", prefixedName(wdf.getSource()))
            .add("prov:usedEntity", prefixedName(wdf.getDestination()));
        addAttributes(wdf, builder);
        return builder.build();
    }

    protected JsonObject toJson(WasGeneratedBy wgb) {
        JsonObjectBuilder builder = Json.createObjectBuilder()
            .add("prov:entity", prefixedName(wgb.getSource()))
            .add("prov:activity", prefixedName(wgb.getDestination()));
        addAttributes(wgb, builder);
        return builder.build();
    }

    protected JsonObject toJson(WasInformedBy wib) {
        JsonObjectBuilder builder = Json.createObjectBuilder()
            .add("prov:informant", prefixedName(wib.getSource()))
            .add("prov:informed", prefixedName(wib.getDestination()));
        addAttributes(wib, builder);
        return builder.build();
    }

    protected void addAttributes(Attributes provObj, JsonObjectBuilder docBuilder) {
        for (QName name : provObj.attrs.keySet()) {
            JsonObjectBuilder attrBuilder = Json.createObjectBuilder()
                .add("$", String.format("%s", provObj.attrs.get(name)));
            if (provObj.attrs.get(name) instanceof String) {
                docBuilder.add(prefixedName(name), attrBuilder.add("type", "xsd:string").build());
            } else if (provObj.attrs.get(name) instanceof Boolean) {
                docBuilder.add(prefixedName(name), attrBuilder.add("type", "xsd:boolean").build());
            } else if (provObj.attrs.get(name) instanceof Integer) {
                docBuilder.add(prefixedName(name), attrBuilder.add("type", "xsd:integer").build());
            } else if (provObj.attrs.get(name) instanceof Long) {
                docBuilder.add(prefixedName(name), attrBuilder.add("type", "xsd:long").build());
            } else if (provObj.attrs.get(name) instanceof Float) {
                docBuilder.add(prefixedName(name), attrBuilder.add("type", "xsd:float").build());
            } else if (provObj.attrs.get(name) instanceof Double) {
                docBuilder.add(prefixedName(name), attrBuilder.add("type", "xsd:double").build());
            } else {
                // log a warning
                docBuilder.add(prefixedName(name), attrBuilder.add("type", "xsd:string").build());
            }
        }
    }

    protected String prefixedName(QName name) {
        if (defaultPrefixes.containsKey(name.getNamespaceURI())) {
            return String.format("%s:%s", defaultPrefixes.get(name.getNamespaceURI()), name.getLocalPart());
        } else {
            return String.format("%s:%s", prefixes.get(name.getNamespaceURI()), name.getLocalPart());
        }
    }
}
