package edu.mit.ll.prov.curator.serialize;

import java.util.HashMap;
import java.util.Map;

import edu.mit.ll.prov.curator.model.*;

public abstract class ProvSerializer implements Serializer {

    protected Map<String, String> defaultPrefixes = new HashMap<>(); // namespace -> prefix
    protected Map<String, String> prefixes = new HashMap<>(); // namespace -> prefix
    
    public ProvSerializer() {
        // defined in the spec
        defaultPrefixes.put("http://www.w3.org/ns/prov#", "prov");
        defaultPrefixes.put("http://www.w3.org/2000/10/XMLSchema#", "xsd");

        //prefixes.put("http://www.ll.mit.edu/provenance", "default"); // namespace used when no prefix specified

        //prefixes.put("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "rdf");

        // ones we'll use frequently
        //prefixes.put("http://www.ll.mit.edu/provenance/application", "app");
    }
    
    public ProvSerializer(Map<String, String> extraPrefixes) {
        this();
        prefixes.putAll(extraPrefixes);
    }

    public abstract String serialize(Graph graph);
    
    public String serialize(Activity activity) {
        return serialize(new Graph().add(activity));
    }
    
    public String serialize(Agent agent) {
        return serialize(new Graph().add(agent));
    }

    public String serialize(Entity entity) {
        return serialize(new Graph().add(entity));
    }

    public String serialize(ActedOnBehalfOf behalf) {
        return serialize(new Graph().add(behalf));
    }

    public String serialize(Used used) {
        return serialize(new Graph().add(used));
    }

    public String serialize(WasAssociatedWith waw) {
        return serialize(new Graph().add(waw));
    }

    public String serialize(WasAttributedTo wat) {
        return serialize(new Graph().add(wat));
    }
    
    public String serialize(WasDerivedFrom wdf) {
        return serialize(new Graph().add(wdf));
    }
    
    public String serialize(WasInformedBy wib) {
        return serialize(new Graph().add(wib));
    }
    
    public String serialize(WasGeneratedBy wgb) {
        return serialize(new Graph().add(wgb));
    }

}
