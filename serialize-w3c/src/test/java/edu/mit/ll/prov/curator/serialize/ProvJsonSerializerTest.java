package edu.mit.ll.prov.curator.serialize;

import java.io.StringReader;

import javax.json.*;
import javax.xml.namespace.QName;

import edu.mit.ll.prov.curator.model.*;

public class ProvJsonSerializerTest {

    public void testActivity() {
        Activity activity = new Activity(new QName("http://www.ll.mit.edu/prov/test", "activity1"));
        Serializer ser = new ProvJsonSerializer();

        String str = ser.serialize(activity);
        //System.out.println(str);

        JsonReader reader = Json.createReader(new StringReader(str));
        JsonObject doc = reader.readObject();

        assert(doc.size() == 2);
        assert(doc.containsKey("prefix"));
        String prefix = getPrefix(doc, "http://www.ll.mit.edu/prov/test");
        assert(prefix != null);

        assert(doc.containsKey("activity"));
        assert(doc.getJsonObject("activity").containsKey(String.format("%s:activity1", prefix)));
    }

    protected String getPrefix(JsonObject doc, String namespaceURI) {
        for (String prefix : doc.getJsonObject("prefix").keySet()) {
            if (doc.getJsonObject("prefix").getString(prefix).equals(namespaceURI)) {
                return prefix;
            }
        }
        return null;
    }

    public void testAgent() {
        Agent agent = new Agent(new QName("http://www.ll.mit.edu/prov/test", "agent1"));
        Serializer ser = new ProvJsonSerializer();

        String str = ser.serialize(agent);
        //System.out.println(str);

        JsonReader reader = Json.createReader(new StringReader(str));
        JsonObject doc = reader.readObject();

        assert(doc.size() == 2);

        assert(doc.containsKey("prefix"));
        assert(doc.getJsonObject("prefix").size() == 1);
        String prefix = getPrefix(doc, "http://www.ll.mit.edu/prov/test");
        assert(prefix != null);

        assert(doc.containsKey("agent"));
        assert(doc.getJsonObject("agent").containsKey(String.format("%s:agent1", prefix)));
    }

    public void testEntity() {
        Entity entity = new Entity(new QName("http://www.ll.mit.edu/prov/test", "entity1"));
        entity.setAttribute(new QName("http://www.ll.mit.edu/prov/test2", "boolValue"), true);
        entity.setAttribute(new QName("http://www.ll.mit.edu/prov/test2", "intValue"), 5);
        entity.setAttribute(new QName("http://www.ll.mit.edu/prov/test2", "longValue"), 9l);
        entity.setAttribute(new QName("http://www.ll.mit.edu/prov/test2", "floatValue"), 7.3f);
        entity.setAttribute(new QName("http://www.ll.mit.edu/prov/test2", "doubleValue"), 9.175);
        Serializer ser = new ProvJsonSerializer();

        String str = ser.serialize(entity);
        //System.out.println(str);

        JsonReader reader = Json.createReader(new StringReader(str));
        JsonObject doc = reader.readObject();

        assert(doc.size() == 2);

        assert(doc.containsKey("prefix"));
        assert(doc.getJsonObject("prefix").size() == 2);
        String prefix = getPrefix(doc, "http://www.ll.mit.edu/prov/test");
        assert(prefix != null);
        String prefix2 = getPrefix(doc, "http://www.ll.mit.edu/prov/test2");
        assert(prefix2 != null);

        assert(doc.containsKey("entity"));
        assert(doc.getJsonObject("entity").containsKey(String.format("%s:entity1", prefix)));

        JsonObject attrs = doc.getJsonObject("entity").getJsonObject(String.format("%s:entity1", prefix));

        assert(attrs.containsKey(String.format("%s:boolValue", prefix2)));
        JsonObject attr = attrs.getJsonObject(String.format("%s:boolValue", prefix2));
        assert(attr.getString("type").equals("xsd:boolean"));
        assert(attr.getString("$").equals("true"));

        assert(attrs.containsKey(String.format("%s:intValue", prefix2)));
        attr = attrs.getJsonObject(String.format("%s:intValue", prefix2));
        assert(attr.getString("type").equals("xsd:integer"));
        assert(attr.getString("$").equals("5"));

        assert(attrs.containsKey(String.format("%s:longValue", prefix2)));
        attr = attrs.getJsonObject(String.format("%s:longValue", prefix2));
        assert(attr.getString("type").equals("xsd:long"));
        assert(attr.getString("$").equals("9"));

        assert(attrs.containsKey(String.format("%s:floatValue", prefix2)));
        attr = attrs.getJsonObject(String.format("%s:floatValue", prefix2));
        assert(attr.getString("type").equals("xsd:float"));
        assert(attr.getString("$").equals("7.3"));

        assert(attrs.containsKey(String.format("%s:doubleValue", prefix2)));
        attr = attrs.getJsonObject(String.format("%s:doubleValue", prefix2));
        assert(attr.getString("type").equals("xsd:double"));
        assert(attr.getString("$").equals("9.175"));
    }

    public void testBehalfOf() {
        ActedOnBehalfOf behalf = new ActedOnBehalfOf(new QName("http://www.ll.mit.edu/prov/test", "behalf1"),
                                                     new QName("http://www.ll.mit.edu/prov/test", "agent1"),
                                                     new QName("http://www.ll.mit.edu/prov/test", "agent2"));
        Serializer ser = new ProvJsonSerializer();

        String str = ser.serialize(behalf);
        //System.out.println(str);

        JsonReader reader = Json.createReader(new StringReader(str));
        JsonObject doc = reader.readObject();

        assert(doc.size() == 2);
        assert(doc.containsKey("prefix"));

        assert(doc.containsKey("actedOnBehalfOf"));
        String prefix = getPrefix(doc, "http://www.ll.mit.edu/prov/test");
        assert(prefix != null);
        assert(doc.getJsonObject("actedOnBehalfOf").containsKey(String.format("%s:behalf1", prefix)));
        JsonObject behalfObj = doc.getJsonObject("actedOnBehalfOf").getJsonObject(String.format("%s:behalf1", prefix));

        assert(behalfObj.containsKey("prov:delegate"));
        assert(behalfObj.getString("prov:delegate").equals(String.format("%s:agent1", prefix)));
        assert(behalfObj.containsKey("prov:responsible"));
        assert(behalfObj.getString("prov:responsible").equals(String.format("%s:agent2", prefix)));
    }

    public void testUsed() {
        Used used = new Used(new QName("http://www.ll.mit.edu/prov/test", "used1"),
                             new QName("http://www.ll.mit.edu/prov/test", "activity1"),
                             new QName("http://www.ll.mit.edu/prov/test", "entity1"));
        Serializer ser = new ProvJsonSerializer();

        String str = ser.serialize(used);
        //System.out.println(str);

        JsonReader reader = Json.createReader(new StringReader(str));
        JsonObject doc = reader.readObject();

        assert(doc.size() == 2);
        assert(doc.containsKey("prefix"));

        assert(doc.containsKey("used"));
        String prefix = getPrefix(doc, "http://www.ll.mit.edu/prov/test");
        assert(prefix != null);
        assert(doc.getJsonObject("used").containsKey(String.format("%s:used1", prefix)));
        JsonObject usedObj = doc.getJsonObject("used").getJsonObject(String.format("%s:used1", prefix));

        assert(usedObj.containsKey("prov:activity"));
        assert(usedObj.getString("prov:activity").equals(String.format("%s:activity1", prefix)));
        assert(usedObj.containsKey("prov:entity"));
        assert(usedObj.getString("prov:entity").equals(String.format("%s:entity1", prefix)));
    }

    public void testWasAssociatedWith() {
        WasAssociatedWith waw = new WasAssociatedWith(new QName("http://www.ll.mit.edu/prov/test", "waw1"),
                                                      new QName("http://www.ll.mit.edu/prov/test", "activity1"),
                                                      new QName("http://www.ll.mit.edu/prov/test", "agent1"));
        Serializer ser = new ProvJsonSerializer();

        String str = ser.serialize(waw);
        //System.out.println(str);

        JsonReader reader = Json.createReader(new StringReader(str));
        JsonObject doc = reader.readObject();

        assert(doc.size() == 2);
        assert(doc.containsKey("prefix"));

        assert(doc.containsKey("wasAssociatedWith"));
        String prefix = getPrefix(doc, "http://www.ll.mit.edu/prov/test");
        assert(prefix != null);
        assert(doc.getJsonObject("wasAssociatedWith").containsKey(String.format("%s:waw1", prefix)));
        JsonObject wawObj = doc.getJsonObject("wasAssociatedWith").getJsonObject(String.format("%s:waw1", prefix));

        assert(wawObj.containsKey("prov:activity"));
        assert(wawObj.getString("prov:activity").equals(String.format("%s:activity1", prefix)));
        assert(wawObj.containsKey("prov:agent"));
        assert(wawObj.getString("prov:agent").equals(String.format("%s:agent1", prefix)));
    }

    public void testWasAttributedTo() {
        WasAttributedTo wat = new WasAttributedTo(new QName("http://www.ll.mit.edu/prov/test", "wat1"),
                                                  new QName("http://www.ll.mit.edu/prov/test", "entity1"),
                                                  new QName("http://www.ll.mit.edu/prov/test", "agent1"));
        Serializer ser = new ProvJsonSerializer();

        String str = ser.serialize(wat);
        //System.out.println(str);

        JsonReader reader = Json.createReader(new StringReader(str));
        JsonObject doc = reader.readObject();

        assert(doc.size() == 2);
        assert(doc.containsKey("prefix"));

        assert(doc.containsKey("wasAttributedTo"));
        String prefix = getPrefix(doc, "http://www.ll.mit.edu/prov/test");
        assert(prefix != null);
        assert(doc.getJsonObject("wasAttributedTo").containsKey(String.format("%s:wat1", prefix)));
        JsonObject wawObj = doc.getJsonObject("wasAttributedTo").getJsonObject(String.format("%s:wat1", prefix));

        assert(wawObj.containsKey("prov:entity"));
        assert(wawObj.getString("prov:entity").equals(String.format("%s:entity1", prefix)));
        assert(wawObj.containsKey("prov:agent"));
        assert(wawObj.getString("prov:agent").equals(String.format("%s:agent1", prefix)));
    }

    public void testWasDerivedFrom() {
        WasDerivedFrom wdf = new WasDerivedFrom(new QName("http://www.ll.mit.edu/prov/test", "wdf1"),
                                                new QName("http://www.ll.mit.edu/prov/test", "entity1"),
                                                new QName("http://www.ll.mit.edu/prov/test", "entity2"));
        Serializer ser = new ProvJsonSerializer();

        String str = ser.serialize(wdf);
        //System.out.println(str);

        JsonReader reader = Json.createReader(new StringReader(str));
        JsonObject doc = reader.readObject();

        assert(doc.size() == 2);
        assert(doc.containsKey("prefix"));

        assert(doc.containsKey("wasDerivedFrom"));
        String prefix = getPrefix(doc, "http://www.ll.mit.edu/prov/test");
        assert(prefix != null);
        assert(doc.getJsonObject("wasDerivedFrom").containsKey(String.format("%s:wdf1", prefix)));
        JsonObject wdfObj = doc.getJsonObject("wasDerivedFrom").getJsonObject(String.format("%s:wdf1", prefix));

        assert(wdfObj.containsKey("prov:generatedEntity"));
        assert(wdfObj.getString("prov:generatedEntity").equals(String.format("%s:entity1", prefix)));
        assert(wdfObj.containsKey("prov:usedEntity"));
        assert(wdfObj.getString("prov:usedEntity").equals(String.format("%s:entity2", prefix)));
    }

    public void testWasInformedBy() {
        WasInformedBy wib = new WasInformedBy(new QName("http://www.ll.mit.edu/prov/test", "wib1"),
                                              new QName("http://www.ll.mit.edu/prov/test", "activity1"),
                                              new QName("http://www.ll.mit.edu/prov/test", "activity2"));
        Serializer ser = new ProvJsonSerializer();

        String str = ser.serialize(wib);
        //System.out.println(str);

        JsonReader reader = Json.createReader(new StringReader(str));
        JsonObject doc = reader.readObject();

        assert(doc.size() == 2);
        assert(doc.containsKey("prefix"));

        assert(doc.containsKey("wasInformedBy"));
        String prefix = getPrefix(doc, "http://www.ll.mit.edu/prov/test");
        assert(prefix != null);
        assert(doc.getJsonObject("wasInformedBy").containsKey(String.format("%s:wib1", prefix)));
        JsonObject wgbObj = doc.getJsonObject("wasInformedBy").getJsonObject(String.format("%s:wib1", prefix));

        assert(wgbObj.containsKey("prov:informant"));
        assert(wgbObj.getString("prov:informant").equals(String.format("%s:activity1", prefix)));
        assert(wgbObj.containsKey("prov:informed"));
        assert(wgbObj.getString("prov:informed").equals(String.format("%s:activity2", prefix)));
    }

    public void testWasGeneratedBy() {
        WasGeneratedBy wgb = new WasGeneratedBy(new QName("http://www.ll.mit.edu/prov/test", "wgb1"),
                                                new QName("http://www.ll.mit.edu/prov/test", "entity1"),
                                                new QName("http://www.ll.mit.edu/prov/test", "activity1"));
        Serializer ser = new ProvJsonSerializer();

        String str = ser.serialize(wgb);
        //System.out.println(str);

        JsonReader reader = Json.createReader(new StringReader(str));
        JsonObject doc = reader.readObject();

        assert(doc.size() == 2);
        assert(doc.containsKey("prefix"));

        assert(doc.containsKey("wasGeneratedBy"));
        String prefix = getPrefix(doc, "http://www.ll.mit.edu/prov/test");
        assert(prefix != null);
        assert(doc.getJsonObject("wasGeneratedBy").containsKey(String.format("%s:wgb1", prefix)));
        JsonObject wgbObj = doc.getJsonObject("wasGeneratedBy").getJsonObject(String.format("%s:wgb1", prefix));

        assert(wgbObj.containsKey("prov:entity"));
        assert(wgbObj.getString("prov:entity").equals(String.format("%s:entity1", prefix)));
        assert(wgbObj.containsKey("prov:activity"));
        assert(wgbObj.getString("prov:activity").equals(String.format("%s:activity1", prefix)));
    }

}
