package edu.mit.ll.prov;

import org.openprovenance.prov.generator.GraphGenerator;
import org.openprovenance.prov.interop.InteropFramework;
import org.openprovenance.prov.model.*;

// Below are needed for the commented out "static graph" below
//import java.util.Arrays;
//import java.util.Collections;

public class SynthGraph {
    private Namespace namespace;
    private ProvFactory provFactory = InteropFramework.newXMLProvFactory();

    public static void main(String args[]) throws InterruptedException {
        InteropFramework interopFramework = new InteropFramework();
        // Number of nodes to generate
        Integer noNodes = Integer.parseInt(args[0]);
        // Max. number of edges for a node
        Integer noEdges = Integer.parseInt(args[1]);
        // Should the first node be an entity or an activity
        String firstNode = GraphGenerator.FIRST_NODE_AS_ENTITY;
        // What is my namepace?
        String namespace = "llprov";
        // Needed to make PROV-DM objects...
        ProvFactory pf = InteropFramework.newXMLProvFactory();

//        SynthGraph synthGraph = new SynthGraph();
//
//        // activties
//        Activity ac0, ac1, ac2;
//
//        // entities
//        Entity e0, e1, e2, e3, e4, e5, e6, e7;
//
//        // used edges
//        Used u0, u1, u2, u3, u4, u5;
//
//        // wasGeneratedBy edges
//        WasGeneratedBy wgb0, wgb1, wgb2, wgb3;
//
//        // wasInformedBy edges
//        WasInformedBy wib0, wib1;
//
//        synthGraph.setNamespace("llprov", "http://edu.mit.ll.mit.edu/prov#");
//
//        ac0 = synthGraph.provFactory.newActivity(synthGraph.qn("AC0"));
//        ac1 = synthGraph.provFactory.newActivity(synthGraph.qn("AC1"));
//        ac2 = synthGraph.provFactory.newActivity(synthGraph.qn("AC2"));
//
//        e0 = synthGraph.provFactory.newEntity(synthGraph.qn("EN0"));
//        e1 = synthGraph.provFactory.newEntity(synthGraph.qn("EN1"));
//        e2 = synthGraph.provFactory.newEntity(synthGraph.qn("EN2"));
//        e3 = synthGraph.provFactory.newEntity(synthGraph.qn("EN3"));
//        e4 = synthGraph.provFactory.newEntity(synthGraph.qn("EN4"));
//        e5 = synthGraph.provFactory.newEntity(synthGraph.qn("EN5"));
//        e6 = synthGraph.provFactory.newEntity(synthGraph.qn("EN6"));
//        e7 = synthGraph.provFactory.newEntity(synthGraph.qn("EN7"));
//
//        u0 = synthGraph.provFactory.newUsed(synthGraph.qn("u-0"), ac0.getId(), e0.getId());
//        u1 = synthGraph.provFactory.newUsed(synthGraph.qn("u-1"), ac0.getId(), e1.getId());
//        u2 = synthGraph.provFactory.newUsed(synthGraph.qn("u-2"), ac0.getId(), e2.getId());
//        u3 = synthGraph.provFactory.newUsed(synthGraph.qn("u-3"), ac1.getId(), e3.getId());
//        u4 = synthGraph.provFactory.newUsed(synthGraph.qn("u-4"), ac1.getId(), e4.getId());
//        u5 = synthGraph.provFactory.newUsed(synthGraph.qn("u-5"), ac2.getId(), e5.getId());
//
//        wib0 = synthGraph.provFactory.newWasInformedBy(synthGraph.qn("wib-0"), ac1.getId(), ac0.getId());
//        wib1 = synthGraph.provFactory.newWasInformedBy(synthGraph.qn("wib-1"), ac2.getId(), ac1.getId());
//
//        wgb0 = synthGraph.provFactory.newWasGeneratedBy(synthGraph.qn("wgb-0"), e3.getId(), ac0.getId());
//        wgb1 = synthGraph.provFactory.newWasGeneratedBy(synthGraph.qn("wgb-1"), e5.getId(), ac1.getId());
//        wgb2 = synthGraph.provFactory.newWasGeneratedBy(synthGraph.qn("wgb-2"), e6.getId(), ac2.getId());
//        wgb3 = synthGraph.provFactory.newWasGeneratedBy(synthGraph.qn("wgb-3"), e7.getId(), ac2.getId());
//
//        Document doc = synthGraph.provFactory.newDocument(Arrays.asList(ac0, ac1, ac2), // Activities
//                Arrays.asList(e0, e1, e2, e3, e4, e5, e6, e7), // Entities
//                Collections.<Agent>emptyList(), // Agents
//                Arrays.<Statement>asList(u0, u1, u2, u3, u4, u5, wib0, wib1, wgb0, wgb1, wgb2, wgb3)); // Statements
//        doc.setNamespace(synthGraph.namespace);
//
//        interopFramework.writeDocument("fixed-example.dot", InteropFramework.ProvFormat.DOT, doc);

        System.out.println("Starting synth-graph with " + noNodes + " nodes");
        generateRandomGraph(noNodes, noEdges, firstNode, namespace, pf, args[2] + ".dot");
    }

    private static void generateRandomGraph(Integer noNodes, Integer noEdges, String firstNode, String namespace, ProvFactory pf, String dotFile) {
        GraphGenerator graphGenerator = new GraphGenerator(noNodes, noEdges, firstNode, namespace, pf, null, null);
        InteropFramework interopFramework = new InteropFramework();
        System.out.println("Starting graph generation");
        graphGenerator.generateElements();
        Document generatedDoc = graphGenerator.getDocument();
        System.out.println("Finished graph generation, starting GraphViz write");
        interopFramework.writeDocument(dotFile, InteropFramework.ProvFormat.DOT, generatedDoc);

        System.out.println("Finished GraphViz write, starting Curator proecssing");
        Curator.publishProvenance(generatedDoc);
        System.out.println("Finished Curator processing, starting Curator shutdown");
        Curator.shutdown();
        System.out.println("Finished Curator shutdown, goodbye!");
    }

    private QualifiedName qn(String name) {
        return namespace.stringToQualifiedName(name, provFactory);
    }

    public void setNamespace(String name, String url) {
        namespace = new Namespace();
        namespace.addKnownNamespaces();
        namespace.register(name, url);
        namespace.registerDefault(name);
    }
}
