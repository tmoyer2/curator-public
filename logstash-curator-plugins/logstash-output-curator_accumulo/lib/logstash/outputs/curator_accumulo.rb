# encoding: utf-8
require "logstash/outputs/base"
require "logstash/namespace"
require "java"

java_import "edu.mit.ll.prov.curator.db.Accumulo"
java_import "edu.mit.ll.prov.curator.deserialize.ProvJsonDeserializer"

# An curator-sql output that does nothing.
class LogStash::Outputs::CuratorAccumulo < LogStash::Outputs::Base
  config_name "curator_accumulo"

  # Access these via @<name>, e.g. @url, @driver, etc.
  config :instance, :validate => :string, :required => true
  config :zooserver, :validate => :string, :required => true
  config :username, :validate => :string, :required => true
  config :password, :validate => :string, :required => true
  
  public
  def register
    # Instantiate an edu.mit.ll.prov.curator.db.Accumulo object
    @store = Accumulo.new(@instance, @zooserver, @username, @password)
    # Instantiate an edu.mit.ll.prov.curator.deserialize.ProvJsonDeserializer object
    @deser = ProvJsonDeserializer.new()

  end # def register

  public
  def multi_receive(events)
    events.each do |event|
        @store.write(@deser.deserialize(event.to_hash["message"]))
    end
    return
  end # def event
end # class LogStash::Outputs::CuratorSql
