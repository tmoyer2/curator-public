Gem::Specification.new do |s|
  s.name          = 'logstash-output-curator_accumulo'
  s.version       = '0.1.0'
  s.licenses      = ['Apache License (2.0)']
  s.summary       = 'Output Curator data to Accumulo'
  s.description   = 'Process provenance data sent to logstash and store the provenance graph in Accumulo.'
  s.homepage      = 'https://llcad-github.llan.ll.mit.edu/Cyber-Provenance/'
  s.authors       = ['Thomas Moyer']
  s.email         = 'thomas.moyer@ll.mit.edu'
  s.require_paths = ['lib']

  # Files
  s.files = Dir['lib/**/*','spec/**/*','vendor/**/*','*.gemspec','*.md','CONTRIBUTORS','Gemfile','LICENSE','NOTICE.TXT']
   # Tests
  s.test_files = s.files.grep(%r{^(test|spec|features)/})

  # Special flag to let us know this is actually a logstash plugin
  s.metadata = { "logstash_plugin" => "true", "logstash_group" => "output" }

  # Gem dependencies
  s.add_runtime_dependency "logstash-core-plugin-api", "~> 2.0"
  s.add_runtime_dependency "logstash-codec-plain"
  s.add_development_dependency "logstash-devutils"
  s.requirements << "jar 'edu.mit.ll.prov.curator.db.Accumulo', '2.0-SNAPSHOT'"
  s.requirements << "jar 'edu.mit.ll.prov.curator.deserialize.ProvJsonDeserializer', '2.0-SNAPSHOT'"
  s.add_runtime_dependency 'jar-dependencies'
end
