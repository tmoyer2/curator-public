Gem::Specification.new do |s|
  s.name          = 'logstash-filter-activemq_prov'
  s.version       = '0.2.2'
  s.licenses      = ['Apache License (2.0)']
  s.summary       = 'Convert ActiveMQ log messages into provenance graphs.'
  s.homepage      = 'http://llcad-github.llan.ll.mit.edu/Cyber-Provenance/logstash-curator-plugins'
  s.authors       = ['Thomas Moyer']
  s.email         = 'thomas.moyer@ll.mit.edu'
  s.require_paths = ['lib']

  # Files
  s.files = Dir['lib/**/*','spec/**/*','vendor/**/*','*.gemspec','*.md','CONTRIBUTORS','Gemfile','LICENSE','NOTICE.TXT']
   # Tests
  s.test_files = s.files.grep(%r{^(test|spec|features)/})

  # Special flag to let us know this is actually a logstash plugin
  s.metadata = { "logstash_plugin" => "true", "logstash_group" => "filter" }

  # Gem dependencies
  s.add_runtime_dependency "logstash-core-plugin-api", "~> 2.0"
  s.add_development_dependency 'logstash-devutils'
  s.requirements << "jar 'edu.mit.ll.prov.curator.model.Agent', '2.0-SNAPSHOT'"
  s.requirements << "jar 'edu.mit.ll.prov.curator.model.Activity', '2.0-SNAPSHOT'"
  s.requirements << "jar 'edu.mit.ll.prov.curator.model.Entity', '2.0-SNAPSHOT'"
  s.requirements << "jar 'edu.mit.ll.prov.curator.model.Used', '2.0-SNAPSHOT'"
  s.requirements << "jar 'edu.mit.ll.prov.curator.model.WasAttributedTo', '2.0-SNAPSHOT'"
  s.requirements << "jar 'edu.mit.ll.prov.curator.model.WasGeneratedBy', '2.0-SNAPSHOT'"
  s.requirements << "jar 'edu.mit.ll.prov.curator.model.WasAssociatedWith', '2.0-SNAPSHOT'"
  s.requirements << "jar 'edu.mit.ll.prov.curator.model.Graph', '2.0-SNAPSHOT'"
  s.requirements << "jar 'edu.mit.ll.prov.curator.serialize.ProvJsonSerializer', '2.0-SNAPSHOT'"
  s.add_runtime_dependency 'jar-dependencies'
end
