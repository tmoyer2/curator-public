#!/bin/bash

mysql --user=root --password=5P@d1N6 -e "create database provenance"
mysql --user=root --password=5P@d1N6 -e "create user 'prov'@'localhost' identified by '5P@d1N6'"
mysql --user=root --password=5P@d1N6 -e "grant all privileges on provenance.* to 'prov'@'localhost'"
mysql --user=root --password=5P@d1N6 -e "create user 'prov'@'%' identified by '5P@d1N6'"
mysql --user=root --password=5P@d1N6 -e "grant all privileges on provenance.* to 'prov'@'%'"

mysql --user=root --password=5P@d1N6 -e "create database provapp"
mysql --user=root --password=5P@d1N6 -e "create user 'app'@'localhost' identified by 'a991nG'"
mysql --user=root --password=5P@d1N6 -e "grant all privileges on provapp.* to 'app'@'localhost'"
mysql --user=root --password=5P@d1N6 -e "create user 'app'@'%' identified by 'a991nG'"
mysql --user=root --password=5P@d1N6 -e "grant all privileges on provapp.* to 'app'@'%'"
