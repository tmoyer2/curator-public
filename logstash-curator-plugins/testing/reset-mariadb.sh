#!/bin/bash

mysqlsh --user=root --password=5P@d1N6 --sql --database=provenance -e "TRUNCATE TABLE vertex"
mysqlsh --user=root --password=5P@d1N6 --sql --database=provenance -e "TRUNCATE TABLE vertex_attrs"
mysqlsh --user=root --password=5P@d1N6 --sql --database=provenance -e "TRUNCATE TABLE vertex_type"
mysqlsh --user=root --password=5P@d1N6 --sql --database=provenance -e "TRUNCATE TABLE edge"
mysqlsh --user=root --password=5P@d1N6 --sql --database=provenance -e "TRUNCATE TABLE edge_attrs"
mysqlsh --user=root --password=5P@d1N6 --sql --database=provenance -e "TRUNCATE TABLE edge_type"
mysqlsh --user=root --password=5P@d1N6 --sql --database=provenance -e "TRUNCATE TABLE namespace"
