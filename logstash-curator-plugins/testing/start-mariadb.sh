#!/bin/bash

docker kill logstash-testing

docker-cleanup

docker run --name logstash-testing -v $(pwd -P)/mariadb-setup:/docker-entrypoint-initdb.d -e MYSQL_ROOT_PASSWORD=5P@d1N6 -p 127.0.0.1:3306:3306 -d mariadb:10.1

docker logs -f logstash-testing
