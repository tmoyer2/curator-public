#!/bin/bash

CLASSPATH=/Users/th23141/Scratch/jars/log4j-1.2.17.jar
CLASSPATH=$CLASSPATH:/Users/th23141/Scratch/jars/core-2.0-SNAPSHOT.jar
CLASSPATH=$CLASSPATH:/Users/th23141/Scratch/jars/logging-log4j-2.0-SNAPSHOT.jar
CLASSPATH=$CLASSPATH:/Users/th23141/Scratch/jars/db-sql-2.0-SNAPSHOT.jar
CLASSPATH=$CLASSPATH:/Users/th23141/Scratch/jars/deserialize-w3c-2.0-SNAPSHOT.jar
CLASSPATH=$CLASSPATH:/Users/th23141/Scratch/jars/slf4j-api-1.7.22.jar
CLASSPATH=$CLASSPATH:/Users/th23141/Scratch/jars/serialize-w3c-2.0-SNAPSHOT.jar
CLASSPATH=$CLASSPATH:/Users/th23141/Scratch/jars/javax.json-api-1.0.jar
CLASSPATH=$CLASSPATH:/Users/th23141/Scratch/jars/javax.json-1.0.4.jar
CLASSPATH=$CLASSPATH:/Users/th23141/Scratch/jars/mysql-connector-java-5.1.38.jar
CLASSPATH=$CLASSPATH:/Users/th23141/Scratch/jars/accumulo-core-1.8.0.jar
CLASSPATH=$CLASSPATH:/Users/th23141/Scratch/jars/db-accumulo-2.0-SNAPSHOT.jar
CLASSPATH=$CLASSPATH:/Users/th23141/Scratch/jars/hadoop-common-2.6.4.jar
CLASSPATH=$CLASSPATH:/Users/th23141/Scratch/jars/commons-configuration-1.6.jar
CLASSPATH=$CLASSPATH:/Users/th23141/Scratch/jars/commons-logging-1.1.1.jar
CLASSPATH=$CLASSPATH:/Users/th23141/Scratch/jars/commons-lang-2.4.jar
CLASSPATH=$CLASSPATH:/Users/th23141/Scratch/jars/commons-collections-3.2.2.jar
CLASSPATH=$CLASSPATH:/Users/th23141/Scratch/jars/guava-14.0.1.jar
CLASSPATH=$CLASSPATH:/Users/th23141/Scratch/jars/accumulo-fate-1.8.0.jar
CLASSPATH=$CLASSPATH:/Users/th23141/Scratch/jars/zookeeper-3.4.6.jar
CLASSPATH=$CLASSPATH:/Users/th23141/Scratch/jars/libthrift-0.9.3.jar
CLASSPATH=$CLASSPATH:/Users/th23141/Scratch/jars/htrace-core-3.1.0-incubating.jar

export CLASSPATH

~/Scratch/logstash-5.4.2/bin/logstash -f ~/Scratch/logstash.conf

