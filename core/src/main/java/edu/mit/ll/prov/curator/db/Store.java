package edu.mit.ll.prov.curator.db;

import java.util.Map;

import edu.mit.ll.prov.curator.model.Edge;
import edu.mit.ll.prov.curator.model.Graph;
import edu.mit.ll.prov.curator.model.Vertex;

public interface Store {
    public void write(Graph graph) throws StoreException;
    public void write(Vertex vertex) throws StoreException;
    public void write(Edge edge) throws StoreException;
}
