package edu.mit.ll.prov.curator.model;

import java.util.Map;
import javax.xml.namespace.QName;

public class Entity extends Vertex {
    public Entity(QName id) {
        super(id);
    }

    public Entity(QName id, Map<QName, Object> attrs) {
        super(id, attrs);
    }

    public String getType() {
        return "entity";
    }
}
