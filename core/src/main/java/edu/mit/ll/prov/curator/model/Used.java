package edu.mit.ll.prov.curator.model;

import java.util.Map;

import javax.xml.namespace.QName;

public class Used extends Edge {

    public Used(QName id, Activity source, Entity destination) {
        super(id, source, destination);
    }

    public Used(QName id, Activity source, Entity destination, Map<QName, Object> attrs) {
        super(id, source, destination, attrs);
    }

    public Used(QName id, QName source, QName destination) {
        super(id, source, destination);
    }

    public Used(QName id, QName source, QName destination, Map<QName, Object> attrs) {
        super(id, source, destination, attrs);
    }

    public String getType() {
        return "used";
    }
}
