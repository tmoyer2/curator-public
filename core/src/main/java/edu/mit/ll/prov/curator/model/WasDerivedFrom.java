package edu.mit.ll.prov.curator.model;

import java.util.Map;

import javax.xml.namespace.QName;

public class WasDerivedFrom extends Edge {

    public WasDerivedFrom(QName id, Entity source, Entity destination) {
        super(id, source, destination);
    }

    public WasDerivedFrom(QName id, Entity source, Entity destination, Map<QName, Object> attrs) {
        super(id, source, destination, attrs);
    }

    public WasDerivedFrom(QName id, QName source, QName destination) {
        super(id, source, destination);
    }

    public WasDerivedFrom(QName id, QName source, QName destination, Map<QName, Object> attrs) {
        super(id, source, destination, attrs);
    }

    public String getType() {
        return "wasDerivedFrom";
    }
}
