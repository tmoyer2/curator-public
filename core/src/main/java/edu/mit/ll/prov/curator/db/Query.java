package edu.mit.ll.prov.curator.db;

import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import edu.mit.ll.prov.curator.model.Edge;
import edu.mit.ll.prov.curator.model.Graph;
import edu.mit.ll.prov.curator.model.Vertex;

public interface Query {
    public static final int DEFAULT_HOPS = 15;
    public static final int MAX_CHILDREN_FOLLOW = 50;
    
    public Map<String,List<String>> getCategories() throws QueryException;
    
    public void clearDatabase() throws DatabaseException;
    
    // should these two return List<QName>?
    public List<Vertex> findVertices(QName attrName, Object attrValue) throws QueryException;
    public List<Vertex> findVertices(QName attrName, Object attrValue, String vertexType, boolean either, Integer paginate) throws QueryException;
    public List<Vertex> findVertices(Map<QName, Object> attrs) throws QueryException;
    public List<Vertex> findVertices(Map<QName, Object> attrs, String vertexType, boolean either, Integer paginate) throws QueryException;

    public Vertex getVertex(QName vertexId) throws QueryException;
    public Edge getEdge(QName edgeId) throws QueryException;

    public Graph ancestors(QName vertexId) throws QueryException;
    public Graph descendants(QName vertexId) throws QueryException;
    public Graph connected(QName vertexId) throws QueryException;
    public Graph connected(List<QName> vertexId) throws QueryException;
    public Graph graph() throws QueryException;
    public Graph subgraph(QName vertexId) throws QueryException;
    public Graph subgraph(QName vertexId, int hops) throws QueryException;
    public Graph subgraph(List<QName> vertexId) throws QueryException;
    public Graph subgraph(List<QName> vertexId, int hops) throws QueryException;
}
