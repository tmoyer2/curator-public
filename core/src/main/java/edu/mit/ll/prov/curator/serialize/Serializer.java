package edu.mit.ll.prov.curator.serialize;

import edu.mit.ll.prov.curator.model.*;

public interface Serializer {
    public String serialize(Graph graph);
    public String serialize(Activity activity);
    public String serialize(Agent agent);
    public String serialize(Entity entity);
    public String serialize(ActedOnBehalfOf behalf);
    public String serialize(Used used);
    public String serialize(WasAssociatedWith waw);
    public String serialize(WasAttributedTo wat);
    public String serialize(WasDerivedFrom wdf);
    public String serialize(WasInformedBy wib);
    public String serialize(WasGeneratedBy wgb);
}
