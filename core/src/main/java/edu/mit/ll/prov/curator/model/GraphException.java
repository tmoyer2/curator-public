package edu.mit.ll.prov.curator.model;

public class GraphException extends Exception {
    public GraphException(String message) {
        super(message);
    }

    public GraphException(Throwable cause) {
        super(cause);
    }

    public GraphException(String message, Throwable cause) {
        super(message, cause);
    }
}
