package edu.mit.ll.prov.curator.model;

import java.util.Map;
import javax.xml.namespace.QName;

public class Activity extends Vertex {
    public Activity(QName id) {
        super(id);
    }

    public Activity(QName id, Map<QName, Object> attrs) {
        super(id, attrs);
    }

    public String getType() {
        return "activity";
    }
}
