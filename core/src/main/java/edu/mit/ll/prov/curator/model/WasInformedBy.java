package edu.mit.ll.prov.curator.model;

import java.util.Map;

import javax.xml.namespace.QName;

public class WasInformedBy extends Edge {

    public WasInformedBy(QName id, Activity source, Activity destination) {
        super(id, source, destination);
    }

    public WasInformedBy(QName id, Activity source, Activity destination, Map<QName, Object> attrs) {
        super(id, source, destination, attrs);
    }

    public WasInformedBy(QName id, QName source, QName destination) {
        super(id, source, destination);
    }

    public WasInformedBy(QName id, QName source, QName destination, Map<QName, Object> attrs) {
        super(id, source, destination, attrs);
    }

    public String getType() {
        return "wasInformedBy";
    }
}
