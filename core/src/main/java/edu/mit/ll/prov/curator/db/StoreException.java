package edu.mit.ll.prov.curator.db;

public class StoreException extends DatabaseException {
    public StoreException(String message) {
        super(message);
    }

    public StoreException(Throwable cause) {
        super(cause);
    }

    public StoreException(String message, Throwable cause) {
        super(message, cause);
    }
}
