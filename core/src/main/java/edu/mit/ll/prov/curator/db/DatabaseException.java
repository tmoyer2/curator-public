package edu.mit.ll.prov.curator.db;

import edu.mit.ll.prov.curator.CuratorException;

public class DatabaseException extends CuratorException {
    public DatabaseException(String message) {
        super(message);
    }

    public DatabaseException(Throwable cause) {
        super(cause);
    }

    public DatabaseException(String message, Throwable cause) {
        super(message, cause);
    }
}
