package edu.mit.ll.prov.curator.model;

import java.util.Map;

import javax.xml.namespace.QName;

public class WasAttributedTo extends Edge {

    public WasAttributedTo(QName id, Entity source, Agent destination) {
        super(id, source, destination);
    }

    public WasAttributedTo(QName id, Entity source, Agent destination, Map<QName, Object> attrs) {
        super(id, source, destination, attrs);
    }

    public WasAttributedTo(QName id, QName source, QName destination) {
        super(id, source, destination);
    }

    public WasAttributedTo(QName id, QName source, QName destination, Map<QName, Object> attrs) {
        super(id, source, destination, attrs);
    }

    public String getType() {
        return "wasAttributedTo";
    }
}
