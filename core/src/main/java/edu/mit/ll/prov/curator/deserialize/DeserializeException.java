package edu.mit.ll.prov.curator.deserialize;

import edu.mit.ll.prov.curator.CuratorException;

public class DeserializeException extends CuratorException {

    public DeserializeException() {
        super();
    }

    public DeserializeException(String message) {
        super(message);
    }

    public DeserializeException(String message, Throwable cause) {
        super(message, cause);
    }

    public DeserializeException(Throwable cause) {
        super(cause);
    }
    
}
