package edu.mit.ll.prov.curator.model;

import java.util.Map;
import javax.xml.namespace.QName;

public class Agent extends Vertex {
    public Agent(QName id) {
        super(id);
    }

    public Agent(QName id, Map<QName, Object> attrs) {
        super(id, attrs);
    }

    public String getType() {
        return "agent";
    }
}
