package edu.mit.ll.prov.curator.model;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Comparator;
import java.util.Set;

import javax.xml.namespace.QName;

public class Graph {
    public QNameComparator qNameComp = new QNameComparator();
    public Map<QName, Vertex> vertices = new TreeMap<>(qNameComp);
    public Map<QName, Edge> edges = new TreeMap<>(qNameComp);

    public Map<QName, List<QName>> forward = new HashMap<>();  // vertexId -> [edgeId]
    public Map<QName, List<QName>> backward = new HashMap<>(); // vertexId -> [edgeId]
    
    public static final int DEFAULT_HOPS = 15;
    public static final int MAX_CHILDREN_FOLLOW = 10;
    
    
    static class QNameComparator implements Comparator<QName> {
        @Override 
        public int compare(QName a, QName b) {
            return a.toString().compareTo(b.toString());
        }
    }
    
    public String toString() {
        return toString(false);
    }
    
    public String toString(boolean sanitized) {
        StringBuilder str = new StringBuilder();
        str.append("---- Graph ----\n");
        for (Vertex vertex: vertices.values()) {
            str.append(vertex.toString(sanitized));
        }
        for (Edge edge: edges.values()) {
            str.append(edge.toString(sanitized));
        }
        return str.toString();
    }

    public Graph add(Vertex vertex) {
        // just overwrite if already exists - don't want to clutter logging code with exception handling
        vertices.put(vertex.id, vertex);
        return this;
    }
    
    public Graph add(Edge edge) {
        // just overwrite if already exists - don't want to clutter logging code with exception handling
        if (edges.containsKey(edge.id)) {
            removeEdge(edge.id);
        }
        edges.put(edge.id, edge);

        // don't check if source and destination exist - it's ok to log a graph with dangling edges
        if (!forward.containsKey(edge.source)) {
            forward.put(edge.source, new ArrayList<QName>());
        }
        forward.get(edge.source).add(edge.id);

        if (!backward.containsKey(edge.destination)) {
            backward.put(edge.destination, new ArrayList<QName>());
        }
        backward.get(edge.destination).add(edge.id);
        return this;
    }

    public Vertex removeVertex(QName vertexId) {
        if (forward.containsKey(vertexId)) {
            for (QName edgeId: forward.get(vertexId)) {
                edges.remove(edgeId);
            }
            forward.remove(vertexId);
        }
        if (backward.containsKey(vertexId)) {
            for (QName edgeId: backward.get(vertexId)) {
                edges.remove(edgeId);
            }
            backward.remove(vertexId);
        }
        return vertices.remove(vertexId);
    }

    public List<Vertex> findVertices(QName attrName, Object attrValue) throws GraphException {
        return findVertices(Collections.singletonMap(attrName, attrValue), false);
    }
    public List<Vertex> findVertices(QName attrName, Object attrValue, boolean either) throws GraphException {
        return findVertices(Collections.singletonMap(attrName, attrValue), either);
    }

    public List<Vertex> findVertices(Map<QName, Object> attrs) throws GraphException {
        return findVertices(attrs, false);
    }
    public List<Vertex> findVertices(Map<QName, Object> attrs, boolean either) throws GraphException {
        List<Vertex> verts = new ArrayList<Vertex>();
        for (Vertex vertex: vertices.values()) {
            if (vertexHasAttributes(vertex, attrs)) {
                verts.add(vertex);
            }
        }
        return verts;
    }

    protected boolean vertexHasAttributes(Vertex vertex, Map<QName, Object> attrs) {
        Set<Map.Entry<QName, Object>> attrSet = attrs.entrySet();
        for (Map.Entry<QName, Object> attr : attrSet) {
            if (!vertex.attrs.containsKey(attr.getKey())) {
                return false;
            }
            if (!vertex.attrs.get(attr.getKey()).equals(attr.getValue())) {
                return false;
            }
        }
        return true;
    }

    public Edge removeEdge(QName edgeId) {
        forward.get(edges.get(edgeId).source).remove(edgeId);
        backward.get(edges.get(edgeId).destination).remove(edgeId);
        return edges.remove(edgeId);
    }

    public Vertex getVertex(QName vertexId) throws GraphException {
        if (!vertices.containsKey(vertexId)) {
            throw new GraphException(String.format("unknown vertex: %s in %s",
                                                   vertexId.getLocalPart(), vertexId.getNamespaceURI()));
        }
        return vertices.get(vertexId);
    }

    public Edge getEdge(QName edgeId) throws GraphException {
        if (!edges.containsKey(edgeId)) {
            throw new GraphException(String.format("unknown edge: %s in %s",
                                                   edgeId.getLocalPart(), edgeId.getNamespaceURI()));
        }
        return edges.get(edgeId);
    }
    
    public Graph ancestors(QName vertexId) throws GraphException {
        if (!vertices.containsKey(vertexId)) {
            throw new GraphException(String.format("unknown vertex: %s in %s",
                                                   vertexId.getLocalPart(), vertexId.getNamespaceURI()));
        }

        Graph g = new Graph();

        ArrayDeque<QName> queue = new ArrayDeque<QName>();
        queue.add(vertexId);

        while (queue.size() > 0) {
            Vertex vertex = vertices.get(queue.remove());
            if (vertex != null) {
                if (forward.containsKey(vertex.id)) {
                    for (QName edgeId : forward.get(vertex.id)) {
                        Edge edge = edges.get(edgeId);
                        queue.add(edge.destination);
                        g.add(edge);
                    }
                }
                g.add(vertex);
            }
        }

        return g;
    }
    
    public Graph descendants(QName vertexId) throws GraphException {
        if (!vertices.containsKey(vertexId)) {
            throw new GraphException(String.format("unknown vertex: %s in %s",
                                                   vertexId.getLocalPart(), vertexId.getNamespaceURI()));
        }

        Graph g = new Graph();

        ArrayDeque<QName> queue = new ArrayDeque<QName>();
        queue.add(vertexId);

        while (queue.size() > 0) {
            Vertex vertex = vertices.get(queue.remove());
            if (vertex != null) {
                if (backward.containsKey(vertex.id)) {
                    for (QName edgeId : backward.get(vertex.id)) {
                        Edge edge = edges.get(edgeId);
                        queue.add(edge.source);
                        g.add(edge);
                    }
                }
                g.add(vertex);
            }
        }

        return g;
    }

    public Graph connected(QName vertexId) throws GraphException {
        return ancestors(vertexId).union(descendants(vertexId));
    }

    public Graph connected(List<QName> vertexIds) throws GraphException {
        Graph graph = new Graph();
        for (QName vertexId : vertexIds) {
            graph = graph.union(connected(vertexId));
        }
        return graph;
    }

    public Graph subgraph(QName vertexId) throws GraphException {
        return subgraph(Collections.singletonList(vertexId));
    }
    public Graph subgraph(QName vertexId, int hops) throws GraphException {
        return subgraph(Collections.singletonList(vertexId), hops);
    }

    public Graph subgraph(List<QName> vertexIds) throws GraphException {
        return subgraph(vertexIds, DEFAULT_HOPS);
    }
    public Graph subgraph(List<QName> vertexIds, int hops) throws GraphException {
        for (QName vertexId : vertexIds) {
            if (!vertices.containsKey(vertexId)) {
                throw new GraphException(String.format("unknown vertex: %s in %s",
                                                       vertexId.getLocalPart(), vertexId.getNamespaceURI()));
            }
        }

        Graph g = new Graph();

        ArrayDeque<QName> queue = new ArrayDeque<QName>();
        for (QName vertexId : vertexIds) {
            queue.add(vertexId);
        }

        while (queue.size() > 0) {
            Vertex vertex = vertices.get(queue.remove());
            if (vertex == null) {
                continue;
            }
            if (g.vertices.containsKey(vertex.id)) {
                continue;
            }
            if (backward.containsKey(vertex.id)) {
                for (QName edgeId : backward.get(vertex.id)) {
                    Edge edge = edges.get(edgeId);
                    queue.add(edge.source);
                    g.add(edge);
                }
            }
            if (forward.containsKey(vertex.id)) {
                for (QName edgeId : forward.get(vertex.id)) {
                    Edge edge = edges.get(edgeId);
                    queue.add(edge.destination);
                    g.add(edge);
                }
            }
            g.add(vertex);
        }

        return g;
    }

    public Graph union(Graph g) {
        Graph graph = new Graph();
        for (Vertex vertex : vertices.values()) {
            graph.add(vertex);
        }
        for (Edge edge : edges.values()) {
            graph.add(edge);
        }
        for (Vertex vertex : g.vertices.values()) {
            graph.add(vertex);
        }
        for (Edge edge : g.edges.values()) {
            graph.add(edge);
        }
        return graph;
    }
}
