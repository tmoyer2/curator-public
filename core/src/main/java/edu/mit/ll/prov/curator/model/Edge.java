package edu.mit.ll.prov.curator.model;

import java.util.Map;

import javax.xml.namespace.QName;

public abstract class Edge extends Attributes {

    protected final QName id;
    protected final QName source;
    protected final QName destination;

    public Edge(QName id, Vertex source, Vertex destination) {
        this(id, source.getId(), destination.getId());
    }

    public Edge(QName id, Vertex source, Vertex destination, Map<QName, Object> attrs) {
        this(id, source.getId(), destination.getId(), attrs);
    }

    public Edge(QName id, QName source, QName destination) {
        this.id = id;
        this.source = source;
        this.destination = destination;
    }

    public Edge(QName id, QName source, QName destination, Map<QName, Object> attrs) {
        this.id = id;
        this.source = source;
        this.destination = destination;
        this.attrs = attrs;
    }

    public QName getId() {
        return id;
    }

    public abstract String getType();

    public QName getSource() {
        return source;
    }

    public QName getDestination() {
        return destination;
    }

    public String toString() {
        return toString(false);
    }
    
    public String toString(boolean sanitized) {
        String str = String.format("%s %s in %s%n", getType(), id.getLocalPart(), id.getNamespaceURI());
        str += String.format("  source:      %s in %s%n", source.getLocalPart(), source.getNamespaceURI());
        str += String.format("  destination: %s in %s%n", destination.getLocalPart(), destination.getNamespaceURI());
        str += super.toString();
        return str;
    }
}
