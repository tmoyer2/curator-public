package edu.mit.ll.prov.curator.db;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import javax.xml.namespace.QName;

import edu.mit.ll.prov.curator.model.*;

public class GraphDb implements Database {

    protected Graph graph = new Graph();

    
    public Map<String,List<String>> getCategories() throws QueryException {
        return new HashMap<String,List<String>>();
    }
    
    public void clearDatabase() throws DatabaseException {
        //TODO: implement 
    }
    
    public void write(Graph graph) throws StoreException {
        for (Vertex vertex : graph.vertices.values()) {
            this.graph.add(vertex);
        }
        for (Edge edge : graph.edges.values()) {
            this.graph.add(edge);
        }
    }

    public void write(Vertex vertex) throws StoreException {
        this.graph.add(vertex);
    }

    public void write(Edge edge) throws StoreException {
        this.graph.add(edge);
    }


    public List<Vertex> findVertices(QName attrName, Object attrValue) throws QueryException {
        return findVertices(attrName, attrValue, null, false, null);
    }
    public List<Vertex> findVertices(QName attrName, Object attrValue, String vertexType, boolean either, Integer paginate) throws QueryException {
        try {
            return graph.findVertices(attrName, attrValue, either);
        } catch (GraphException e) {
            throw new QueryException("find failed", e);
        }
    }

    public List<Vertex> findVertices(Map<QName, Object> attrs) throws QueryException {
        return findVertices(attrs, null, false, null);
    }
    public List<Vertex> findVertices(Map<QName, Object> attrs, String vertexType, boolean either, Integer paginate) throws QueryException {
        try {
            return graph.findVertices(attrs, either);
        } catch (GraphException e) {
            throw new QueryException("find failed", e);
        }
    }

    public Vertex getVertex(QName vertexId) throws QueryException {
        try {
            return graph.getVertex(vertexId);
        } catch (GraphException e) {
            throw new QueryException("get failed", e);
        }
    }

    public Edge getEdge(QName edgeId) throws QueryException {
        try {
            return graph.getEdge(edgeId);
        } catch (GraphException e) {
            throw new QueryException("get failed", e);
        }
    }

    public Graph ancestors(QName vertexId) throws QueryException {
        try {
            return graph.ancestors(vertexId);
        } catch (GraphException e) {
            throw new QueryException("ancestors failed", e);
        }
    }

    public Graph descendants(QName vertexId) throws QueryException {
        try {
            return graph.descendants(vertexId);
        } catch (GraphException e) {
            throw new QueryException("descendants failed", e);
        }
    }

    public Graph connected(QName vertexId) throws QueryException {
        try {
            return graph.connected(vertexId);
        } catch (GraphException e) {
            throw new QueryException("connected failed", e);
        }
    }

    public Graph connected(List<QName> vertexId) throws QueryException {
        try {
            return graph.connected(vertexId);
        } catch (GraphException e) {
            throw new QueryException("connected failed", e);
        }
    }

    public Graph graph() throws QueryException {
        //TODO: implement
        return null;
    }
    
    public Graph subgraph(QName vertexId) throws QueryException {
        return subgraph(vertexId, DEFAULT_HOPS);
    }
    public Graph subgraph(QName vertexId, int hops) throws QueryException {
        try {
            return graph.subgraph(vertexId, hops);
        } catch (GraphException e) {
            throw new QueryException("subgraph failed", e);
        }
    }

    public Graph subgraph(List<QName> vertexId) throws QueryException {
        return subgraph(vertexId, DEFAULT_HOPS);
    }
    public Graph subgraph(List<QName> vertexId, int hops) throws QueryException {
        try {
            return graph.subgraph(vertexId, hops);
        } catch (GraphException e) {
            throw new QueryException("subgraph failed", e);
        }
    }

}
