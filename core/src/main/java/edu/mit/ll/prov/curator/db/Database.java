package edu.mit.ll.prov.curator.db;

public interface Database extends Store, Query {
}
