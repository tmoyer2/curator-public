package edu.mit.ll.prov.curator.model;

import javax.xml.namespace.QName;

public class AttributesTest {

    public void testSetStringAttribute() {
        Attributes attrs = new Attributes();

        QName name = new QName("http://www.ll.mit.edu/prov/test", "name");
        QName type = new QName("http://www.w3.org/2000/10/XMLSchema#", "string");
        attrs.setAttribute(name, "string_value", type);

        assert("string_value".equals(attrs.getAttributeValue(name)));
    }

    public void testSetBooleanAttribute() {
        Attributes attrs = new Attributes();

        QName name = new QName("http://www.ll.mit.edu/prov/test", "name");
        QName type = new QName("http://www.w3.org/2000/10/XMLSchema#", "boolean");
        attrs.setAttribute(name, "true", type);

        assert(((Boolean)attrs.getAttributeValue(name)).booleanValue() == true);
    }

    public void testSetIntAttribute() {
        Attributes attrs = new Attributes();

        QName name = new QName("http://www.ll.mit.edu/prov/test", "name");
        QName type = new QName("http://www.w3.org/2000/10/XMLSchema#", "int");
        attrs.setAttribute(name, "43", type);

        assert(((Integer)attrs.getAttributeValue(name)).intValue() == 43);
    }

    public void testSetLongAttribute() {
        Attributes attrs = new Attributes();

        QName name = new QName("http://www.ll.mit.edu/prov/test", "name");
        QName type = new QName("http://www.w3.org/2000/10/XMLSchema#", "long");
        attrs.setAttribute(name, "43", type);

        assert(((Long)attrs.getAttributeValue(name)).longValue() == 43l);
    }

    public void testSetFloatAttribute() {
        Attributes attrs = new Attributes();

        QName name = new QName("http://www.ll.mit.edu/prov/test", "name");
        QName type = new QName("http://www.w3.org/2000/10/XMLSchema#", "float");
        attrs.setAttribute(name, "43.43", type);

        assert(Math.abs(((Float)attrs.getAttributeValue(name)).floatValue() - 43.43) < 0.001);
    }

    public void testSetDoubleAttribute() {
        Attributes attrs = new Attributes();

        QName name = new QName("http://www.ll.mit.edu/prov/test", "name");
        QName type = new QName("http://www.w3.org/2000/10/XMLSchema#", "double");
        attrs.setAttribute(name, "43.43", type);

        assert(Math.abs(((Double)attrs.getAttributeValue(name)).doubleValue() - 43.43) < 0.001);
    }

}
