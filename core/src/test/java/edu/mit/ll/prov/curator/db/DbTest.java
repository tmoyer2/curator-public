package edu.mit.ll.prov.curator.db;

import java.util.*;

import javax.xml.namespace.QName;

import edu.mit.ll.prov.curator.model.*;

public class DbTest {

    Database db;
    Random random;

    // return non-void to work around a bug in the maven surefire support for pojo tests
    public int setUp() {
        //System.out.println("**** setUp ****");
        db = new GraphDb();
        random = new Random(1390408);
        return 1;
    }

    // return non-void to work around a bug in the maven surefire support for pojo tests
    public int tearDown() {
        //System.out.println("**** tearDown ****");
        db = null;
        return 1;
    }

    public void testActivity() {
        Vertex v = activity();
        try {
            db.write(v);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }

        Vertex v2 = null;
        try {
            v2 = db.getVertex(v.getId());
        } catch (QueryException e) {
            e.printStackTrace();
            assert(false);
        }
        assert(v2 instanceof Activity);
        assert(v.getId().equals(v2.getId()));
        equalAttributes(v, v2);
    }

    public void testAttributes() {
        Vertex v = entity(50);
        try {
            db.write(v);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }

        Vertex v2 = null;
        try {
            v2 = db.getVertex(v.getId());
        } catch (QueryException e) {
            e.printStackTrace();
            assert(false);
        }
        assert(v2 instanceof Entity);
        assert(v.getId().equals(v2.getId()));
        equalAttributes(v, v2);
    }

    public void testAgent() {
        Vertex v = agent(5);
        try {
            db.write(v);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }

        Vertex v2 = null;
        try {
            v2 = db.getVertex(v.getId());
        } catch (QueryException e) {
            e.printStackTrace();
            assert(false);
        }
        assert(v2 instanceof Agent);
        assert(v.getId().equals(v2.getId()));
        equalAttributes(v, v2);
    }

    public void testEntity() {
        Vertex v = entity(10);
        try {
            db.write(v);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }

        Vertex v2 = null;
        try {
            v2 = db.getVertex(v.getId());
        } catch (QueryException e) {
            assert(false);
        }
        assert(v2 instanceof Entity);
        assert(v.getId().equals(v2.getId()));
        equalAttributes(v, v2);
    }


    public void testActedOnBehalfOf() {
        Agent source = agent();
        try {
            db.write(source);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }
        Agent destination = agent();
        try {
            db.write(destination);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }

        Edge edge = new ActedOnBehalfOf(id(), source, destination);
        try {
            db.write(edge);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }

        Edge edge2 = null;
        try {
            edge2 = db.getEdge(edge.getId());
        } catch (QueryException e) {
            e.printStackTrace();
            assert(false);
        }
        assert(edge2 instanceof ActedOnBehalfOf);
        assert(edge.getId().equals(edge2.getId()));
        assert(edge.getSource().equals(edge2.getSource()));
        assert(edge.getDestination().equals(edge2.getDestination()));
        equalAttributes(edge, edge2);
    }

    public void testUsed() {
        Activity source = activity();
        try {
            db.write(source);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }
        Entity destination = entity();
        try {
            db.write(destination);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }

        Edge edge = new Used(id(), source, destination);
        try {
            db.write(edge);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }

        Edge edge2 = null;
        try {
            edge2 = db.getEdge(edge.getId());
        } catch (QueryException e) {
            e.printStackTrace();
            assert(false);
        }
        assert(edge2 instanceof Used);
        assert(edge.getId().equals(edge2.getId()));
        assert(edge.getSource().equals(edge2.getSource()));
        assert(edge.getDestination().equals(edge2.getDestination()));
        equalAttributes(edge, edge2);
    }

    public void testWasAssociatedWith() {
        Activity source = activity();
        try {
            db.write(source);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }
        Agent destination = agent();
        try {
            db.write(destination);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }

        Edge edge = new WasAssociatedWith(id(), source, destination);
        try {
            db.write(edge);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }

        Edge edge2 = null;
        try {
            edge2 = db.getEdge(edge.getId());
        } catch (QueryException e) {
            e.printStackTrace();
            assert(false);
        }
        assert(edge2 instanceof WasAssociatedWith);
        assert(edge.getId().equals(edge2.getId()));
        assert(edge.getSource().equals(edge2.getSource()));
        assert(edge.getDestination().equals(edge2.getDestination()));
        equalAttributes(edge, edge2);
    }

    public void testWasAttributedTo() {
        Entity source = entity();
        try {
            db.write(source);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }
        Agent destination = agent();
        try {
            db.write(destination);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }

        Edge edge = new WasAttributedTo(id(), source, destination);
        try {
            db.write(edge);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }

        Edge edge2 = null;
        try {
            edge2 = db.getEdge(edge.getId());
        } catch (QueryException e) {
            e.printStackTrace();
            assert(false);
        }
        assert(edge2 instanceof WasAttributedTo);
        assert(edge.getId().equals(edge2.getId()));
        assert(edge.getSource().equals(edge2.getSource()));
        assert(edge.getDestination().equals(edge2.getDestination()));
        equalAttributes(edge, edge2);
    }

    public void testWasDerivedFrom() {
        Entity source = entity();
        try {
            db.write(source);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }
        Entity destination = entity();
        try {
            db.write(destination);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }

        Edge edge = new WasDerivedFrom(id(), source, destination);
        try {
            db.write(edge);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }

        Edge edge2 = null;
        try {
            edge2 = db.getEdge(edge.getId());
        } catch (QueryException e) {
            e.printStackTrace();
            assert(false);
        }
        assert(edge2 instanceof WasDerivedFrom);
        assert(edge.getId().equals(edge2.getId()));
        assert(edge.getSource().equals(edge2.getSource()));
        assert(edge.getDestination().equals(edge2.getDestination()));
        equalAttributes(edge, edge2);
    }

    public void testWasGeneratedBy() {
        Entity source = entity();
        try {
            db.write(source);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }
        Activity destination = activity();
        try {
            db.write(destination);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }

        Edge edge = new WasGeneratedBy(id(), source, destination);
        try {
            db.write(edge);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }

        Edge edge2 = null;
        try {
            edge2 = db.getEdge(edge.getId());
        } catch (QueryException e) {
            e.printStackTrace();
            assert(false);
        }
        assert(edge2 instanceof WasGeneratedBy);
        assert(edge.getId().equals(edge2.getId()));
        assert(edge.getSource().equals(edge2.getSource()));
        assert(edge.getDestination().equals(edge2.getDestination()));
        equalAttributes(edge, edge2);
    }

    public void testWasInformedBy() {
        Activity source = activity();
        try {
            db.write(source);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }
        Activity destination = activity();
        try {
            db.write(destination);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }

        Edge edge = new WasInformedBy(id(), source, destination);
        try {
            db.write(edge);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }

        Edge edge2 = null;
        try {
            edge2 = db.getEdge(edge.getId());
        } catch (QueryException e) {
            e.printStackTrace();
            assert(false);
        }
        assert(edge2 instanceof WasInformedBy);
        assert(edge.getId().equals(edge2.getId()));
        assert(edge.getSource().equals(edge2.getSource()));
        assert(edge.getDestination().equals(edge2.getDestination()));
        equalAttributes(edge, edge2);
    }

    public void testFind() {
        Map<QName, Vertex> vertsWithAttr = new HashMap<>();
        for(int i=0;i<5;i++) {
            try {
                Map<QName, Object> attrs = new HashMap<>();
                attrs.put(id("name1"), "find_value1");
                Entity entity = new Entity(id(), attrs);
                vertsWithAttr.put(entity.getId(), entity);
                db.write(entity);
            } catch (StoreException e) {
                e.printStackTrace();
                assert(false);
            }
        }
        for(int i=0;i<500;i++) {
            try {
                Map<QName, Object> attrs = new HashMap<>();
                attrs.put(id("name1"), String.format("%d", random.nextInt()));
                attrs.put(id("name2"), "find_value1");
                Entity entity = new Entity(id(), attrs);
                db.write(entity);
            } catch (StoreException e) {
                e.printStackTrace();
                assert(false);
            }
        }

        List<Vertex> found = null;
        try {
            found = db.findVertices(id("name1"), "find_value1");
        } catch (QueryException e) {
            e.printStackTrace();
            assert(false);
        }

        assert(found.size() == vertsWithAttr.size());
        for (Vertex v : found) {
            assert(vertsWithAttr.containsKey(v.getId()));
        }
    }

    public void testFindMultiple() {
        Map<QName, Vertex> vertsWithAttr = new HashMap<>();
        for(int i=0;i<5;i++) {
            try {
                Map<QName, Object> attrs = new HashMap<>();
                attrs.put(id("name1"), "multifind_value1");
                attrs.put(id("name2"), new Integer(42));
                Entity entity = new Entity(id(), attrs);
                vertsWithAttr.put(entity.getId(), entity);
                db.write(entity);
            } catch (StoreException e) {
                e.printStackTrace();
                assert(false);
            }
        }
        for(int i=0;i<100;i++) {
            try {
                Map<QName, Object> attrs = new HashMap<>();
                attrs.put(id("name1"), "multifind_value1");
                attrs.put(id("name2"), "multifind_value1");
                Entity entity = new Entity(id(), attrs);
                db.write(entity);
            } catch (StoreException e) {
                e.printStackTrace();
                assert(false);
            }
        }
        for(int i=0;i<100;i++) {
            try {
                Map<QName, Object> attrs = new HashMap<>();
                attrs.put(id("name1"), String.format("%d", random.nextInt()));
                attrs.put(id("name2"), new Integer(42));
                Entity entity = new Entity(id(), attrs);
                db.write(entity);
            } catch (StoreException e) {
                e.printStackTrace();
                assert(false);
            }
        }

        List<Vertex> found = null;
        try {
            Map<QName, Object> attrs = new HashMap<>();
            attrs.put(id("name1"), "multifind_value1");
            attrs.put(id("name2"), new Integer(42));
            found = db.findVertices(attrs);
        } catch (QueryException e) {
            e.printStackTrace();
            assert(false);
        }

        assert(found.size() == vertsWithAttr.size());
        for (Vertex v : found) {
            assert(vertsWithAttr.containsKey(v.getId()));
        }
    }

    public void testGraph() {
        Graph graph = new Graph();

        List<Entity> inputs = new ArrayList<>();
        for(int i=0;i<8;i++) {
            Entity entity = new Entity(id());
            inputs.add(entity);
        }
        for(Entity entity : inputs) {
            graph.add(entity);
        }

        List<Entity> outputs = new ArrayList<>();
        for(int i=0;i<12;i++) {
            Entity entity = new Entity(id());
            outputs.add(entity);
        }
        for(Entity entity : outputs) {
            graph.add(entity);
        }

        List<Agent> agents = new ArrayList<>();
        for(int i=0;i<2;i++) {
            Agent agent = new Agent(id());
            agents.add(agent);
        }
        for(Agent agent : agents) {
            graph.add(agent);
        }

        List<Activity> activities = new ArrayList<>();
        for(int i=0;i<4;i++) {
            Activity activity = new Activity(id());
            activities.add(activity);
            graph.add(activity);
            graph.add(new WasAssociatedWith(id(), activity, agents.get(i/2)));
            graph.add(new Used(id(), activity, inputs.get(2*i)));
            graph.add(new Used(id(), activity, inputs.get(2*i+1)));
            graph.add(new WasGeneratedBy(id(), outputs.get(3*i), activity));
            graph.add(new WasGeneratedBy(id(), outputs.get(3*i+1), activity));
            graph.add(new WasGeneratedBy(id(), outputs.get(3*i+2), activity));
        }

        try {
            db.write(graph);
        } catch (StoreException e) {
            e.printStackTrace();
            assert(false);
        }

        /*
          (I)\     / (O)        (I)\     / (O)
              (Act)- (O)            (Act)- (O)
          (I)/  |  \ (O)        (I)/  |  \ (O)
             (Agent)               (Agent)
          (I)\  |  / (O)        (I)\  |  / (O)
              (Act)- (O)            (Act)- (O)
          (I)/     \ (O)        (I)/     \ (O)

         */

        for (int i=0;i<12;i++) {
            try {
                Graph g = db.ancestors(outputs.get(i).getId());
                assert(g.vertices.size() == 5);
                assert(g.edges.size() == 4);
                assert(g.vertices.containsKey(outputs.get(i).getId()));
                assert(g.vertices.containsKey(activities.get(i/3).getId()));
                assert(g.vertices.containsKey(inputs.get(i/3*2).getId()));
                assert(g.vertices.containsKey(inputs.get(i/3*2+1).getId()));
                assert(g.vertices.containsKey(agents.get(i/6).getId()));
            } catch (QueryException e) {
                e.printStackTrace();
                assert(false);
            }
        }

        for (int i=0;i<8;i++) {
            try {
                Graph g = db.descendants(inputs.get(i).getId());
                assert(g.vertices.size() == 5);
                assert(g.edges.size() == 4);
                assert(g.vertices.containsKey(inputs.get(i).getId()));
                assert(g.vertices.containsKey(activities.get(i/2).getId()));
                assert(g.vertices.containsKey(outputs.get(i/2*3).getId()));
                assert(g.vertices.containsKey(outputs.get(i/2*3+1).getId()));
                assert(g.vertices.containsKey(outputs.get(i/2*3+2).getId()));
            } catch (QueryException e) {
                e.printStackTrace();
                assert(false);
            }
        }

        for(int i=0;i<4;i++) {
            try {
                Graph g = db.connected(activities.get(i).getId());
                assert(g.vertices.size() == 7);
            } catch (QueryException e) {
                e.printStackTrace();
                assert(false);
            }
        }

        for(int i=0;i<2;i++) {
            try {
                Graph g = db.subgraph(inputs.get(i*6).getId());
                assert(g.vertices.size() == 13);
            } catch (QueryException e) {
                e.printStackTrace();
                assert(false);
            }
        }
        for(int i=0;i<2;i++) {
            try {
                Graph g = db.subgraph(outputs.get(i*6+3).getId());
                assert(g.vertices.size() == 13);
            } catch (QueryException e) {
                e.printStackTrace();
                assert(false);
            }
        }
        for(int i=0;i<2;i++) {
            try {
                Graph g = db.subgraph(activities.get(i*2).getId());
                assert(g.vertices.size() == 13);
            } catch (QueryException e) {
                e.printStackTrace();
                assert(false);
            }
        }
        for(int i=0;i<2;i++) {
            try {
                Graph g = db.subgraph(agents.get(i).getId());
                assert(g.vertices.size() == 13);
            } catch (QueryException e) {
                e.printStackTrace();
                assert(false);
            }
        }

    }

    protected Activity activity() {
        return new Activity(id());
    }

    protected Activity activity(int numAttrs) {
        return new Activity(id(), attributes(numAttrs));
    }

    protected Agent agent() {
        return new Agent(id());
    }

    protected Agent agent(int numAttrs) {
        return new Agent(id(), attributes(numAttrs));
    }

    protected Entity entity() {
        return new Entity(id());
    }

    protected Entity entity(int numAttrs) {
        return new Entity(id(), attributes(numAttrs));
    }
    
    protected QName id() {
        return new QName("http://www.ll.mit.edu/provenance/test", UUID.randomUUID().toString());
    }
    
    protected QName id(String name) {
        return new QName("http://www.ll.mit.edu/provenance/test", name);
    }

    protected Map<QName, Object> attributes(int numAttrs) {
        Map<QName, Object> attrs = new HashMap<>();
        for(int i=1;i<=numAttrs;i++) {
            attrs.put(id(String.format("attr%d", i)), randomValue());
        }
        return attrs;
    }

    protected Object randomValue() {
        switch (random.nextInt(6)) {
        case 0: return random.nextBoolean();
        case 1: return random.nextDouble();
        case 2: return random.nextFloat();
        case 3: return random.nextInt();
        case 4: return random.nextLong();
        default: return String.format("str-%d", random.nextInt());
        }
    }

    protected void equalAttributes(Attributes a1, Attributes a2) {
        assert(a1.getAttributeNames().size() == a2.getAttributeNames().size());
        for(QName name : a1.getAttributeNames()) {
            assert(a2.getAttributeValue(name) != null);
            assert(a1.getAttributeValue(name).equals(a2.getAttributeValue(name)));
        }
    }

}
